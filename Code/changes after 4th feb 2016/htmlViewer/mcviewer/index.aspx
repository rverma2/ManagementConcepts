﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="ViewerJS_index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv='cache-control' content='no-cache'/>
	<meta http-equiv='expires' content='0'/>
	<meta http-equiv='pragma' content='no-cache'/>
    <title>MC Viewer</title>   
    <%--<script src="../jquery.texthighlighter-0.2.1/jquery.texthighlighter-0.2.1/src/jquery.textHighlighter.js"></script>--%>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="viewer.css" media="screen" />
    <%--<script src="viewer.js" type="text/javascript" charset="utf-8"></script>
    <script src="PluginLoader.js" type="text/javascript" charset="utf-8"></script>--%>
   <%-- <script src="../Scripts/script1/jquery.xml2json.js"></script>
    <script src="../Scripts/script1/jquery.xml2json.pack.js"></script>
    <script src="../Scripts/script1/jquery-1.2.6.js"></script>   
    <script src="../Scripts/jquery-1.10.2.min.js"></script>    
    <script src="../Scripts/jquery-1.10.2.js"></script>--%>
    <script src="../Scripts/jquery-1.10.2.min.js"></script>
    <script src="../Scripts/WebForms/includeJS.js"></script>
   <%-- <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.4.0/fabric.min.js"></script>--%>
    <script src="../Scripts/fabricjs_viewport.js"></script>
    <%--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="../style/maxcdnBootstrap.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"/>
    <script src="../Scripts/paper.js"></script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"/>
    <%--<script src="http://htmlviewer/svg-edit-2.7/embedapi.js"></script>
    <script src="../Scripts/jquery-1.3.2.min.js"></script>--%>
    <link href="../style/foo.css" rel="stylesheet" />
    <link href="../style/footer.css" rel="stylesheet" />
    <link href="../Management_Concepts/css/styleViewer.css" rel="stylesheet" />
    <script type='text/javascript'>

        var socialLink =
        {
            isAnimation: true,

            init: function () {

                $('#dividerBtn').on('click', this.navigationMenu)
            },
            navigationMenu: function () {

                if (socialLink.isAnimation) {
                    $('#rightItem').show("", function () {
                        $("#dividerBtn").removeClass("dividerCSS");
                        $("#dividerBtn").addClass("dividerCSSRight");
                    });
                    socialLink.isAnimation = false;
                }
                else {
                    $('#rightItem').hide(500, function () {
                        $("#dividerBtn").removeClass("dividerCSSRight");
                        $("#dividerBtn").addClass("dividerCSS");
                    });
                    socialLink.isAnimation = true;
                }
            }
        }

        var NoteController =
        {
            init: function () {

                $('#notesTab').on('click', NoteController.openNote)
                $('#footnotesTab').on('click', NoteController.openFootNote)
            },
            openNote: function () {

                if ($('#noteSlide').attr('visibility') == 'hidden') {
                    $('#userContent').show()
                    $('#noteSlide').attr('visibility', 'visible');
                    $('#noteWrapper').width('280px');
                    $('#notesTab').css('margin-left', '260px');
                    $('#notesTab').animate({ marginLeft: '10px' }, 500)
                    $('#noteSlide').animate({ marginLeft: '-=100%' }, 500);
                    if ($('#footNoteSlide').attr('visibility') != 'hidden') {
                        $('#footNoteSlide').attr('visibility', 'hidden');
                        $('#footnotesTab').animate({ marginLeft: '260px' }, 500, function () {
                            $('#footNoteWrapper').width('30px');
                            $('#footnotesTab').css('margin-left', '10px');
                        });
                        $('#footNoteSlide').animate({ marginLeft: '280px' }, 500);


                    }
                }
                else {
                    $('#noteSlide').attr('visibility', 'hidden');
                    $('#notesTab').animate({ marginLeft: '260px' }, 500, function () {
                        $('#noteWrapper').width('30px');
                        $('#notesTab').css('margin-left', '10px');
                    });
                    $('#noteSlide').animate({ marginLeft: '280px' }, 500);
                }

            },
            openFootNote: function () {
                if ($('#footNoteSlide').attr('visibility') == 'hidden') {
                    $('#footNoteSlide').attr('visibility', 'visible');
                    $('#footNoteWrapper').width('280px');
                    $('#footnotesTab').css('margin-left', '260px');
                    $('#footnotesTab').animate({ marginLeft: '10' }, 500);
                    $('#footNoteSlide').animate({ marginLeft: '-=100%' }, 500);
                }
                else {
                    $('#footNoteSlide').attr('visibility', 'hidden');
                    $('#footnotesTab').animate({ marginLeft: '260px' }, 500, function () {
                        $('#footNoteWrapper').width('30px');
                        $('#footnotesTab').css('margin-left', '10px');
                    });
                    $('#footNoteSlide').animate({ marginLeft: '280px' }, 500);

                }


            }
        }

        $(document).ready(function () {
            NoteController.init()
            socialLink.init()

        })




    </script>
 
   <%-- <script type="text/paperscript" canvas="canvas-line">
            document.getElementById('canvas-line').style.display = 'none';
            document.getElementById('button-line').style.display = 'none';

              var myPath;

            function onMouseDown(event) {alert('test');
              // The mouse was clicked, so let's put a newly created Path into
              // myPath, give it the color black and add the location as the
              // path's first segment.
              myPath = new Path();
              myPath.strokeColor = 'red';
              myPath.add(event.point);
            }

            function onMouseUp(event) {
              // The mouse was released, so we add the new location as the end
              // segment of the line.
              myPath.add(event.point);
            }

            function onClick(event){
                if(clearCanvas && project.activeLayer.hasChildren()){
                    project.activeLayer.removeChildren();
                    clearCanvas = true;
                }
            }

             // clears graphics
                  document.getElementById('clear-line').addEventListener('click', function() {
                    project.clear()
                  }, true);

    </script>--%>
    <!--end of js for line-->
    <!--js for button toggle line-->
    <%--<script>
        $(document).ready(function () {

            $("button.toggler-line").click(function () {
                $("#clicktotoggle-line").toggleClass("maximize");
                $(this).html($(this).html() == "Line Tool On" ? "Line Tool Off" : "Line Tool On");
            });
        });
    </script>--%>
    <!--end of js for button toggle line-->
    <!--js for div toggle line-->
   <%-- <script>
        $(document).ready(function () {
            $("#clicktotoggle-line").click(function () {
                $(".toggle-line").toggle();
            });
        });
    </script>--%>
    <!--end of js for div toggle line-->
    <!--start of entire js section for line-->
    <style>
         .disableCourseIcon
        {
            background-position: -3px -104px !important;
            cursor:default !important;
        }
        .input_page > div {
            display: inline-block;
        }

      /*  #printNote {
            background: rgba(0, 0, 0, 0) url("../Management_Concepts/images/print.png") no-repeat scroll 0 0;
            cursor: pointer;
            float: right;
            margin-top: -14px;
            padding-right: 41px;
        }*/
        
         #printNote {
            background: rgba(0, 0, 0, 0) url("../Management_Concepts/images/print.png") no-repeat scroll 0 0;
            cursor: pointer;
            float: right;
            margin-top: -14px;
            position:relative;
            right:29px;
            width:17px;
        }

        #PageHeader .ContentWrapper {
            height: 64px !important;
        }

        .showSVG {
            background: transparent;
            position: absolute;
            z-index: 6;
        }

        .hideSVG {
            background: transparent;
            position: absolute;
            z-index: 4;
        }
    </style>

    <style>
        html > body {
            overflow: unset !important;
        }

        .wPaint-theme-classic.wPaint-theme-standard {
            background-color: transparent !important;
            left: -26px;
            top: 32px;
            width: 690px !important;
            z-index: 999;
            height: 835px !important;
        }

        .wPaint-menu.ui-draggable.wPaint-menu-alignment-horizontal {
            left: 254px !important;
            width: 581px !important;
        }

        .wPaint-canvas-bg {
            background: none repeat scroll 0 0;
            height: 100%;
            width: 691px;
        }

        #canvasContainer {
            overflow: auto !important;
            z-index: 5 !important;
            left: 42px;
            top: 20px;
           /* min-height: 1000px;
            max-height: 2000px;
            height: 1800px;
            */
        }
       
        canvas#simple_sketch {
            border: 1px solid red;
            border: 1px solid red;
            bottom: auto;            
            left: 15%;
            overflow: auto;
            position: absolute;
            right: auto;
            top: 80px;
            width: 78%;
            z-index: 999;
        }

        .canvas-container {
            height: 834px !important;
            margin-left: 20% !important;
            position: absolute !important;
            top: 75px !important;
            width: 80% !important;
        }

        .showCanvas {
            z-index: 6;
        }

        #objViewer {
            z-index: 5 !important;
            position: relative;
            background: transparent;
        }

        .hideCanvas {
            z-index: 4 !important;
        }

        /*start of entire css for shapes*/
        /*start of entire css for circle*/
        .toggler-circle {
            height: 35px;
            width: 100px;
            /*margin: 10px 0 10px 90px;*/
        }

        #canvas-circle {
            width: 800px;
            height: 600px;
            border: 1px solid #ccc;
            position: absolute;
            top: 300px;
            left: 100px;
            z-index: 200;
        }

        #button-circle {
            position: absolute;
            top: 300px;
            left: 110px;
            z-index: 600;
        }

            #button-circle > input {
                padding: 10px;
                display: block;
                margin-top: 5px;
            }
        /*start of entire css for circle*/
        /*start of entire CSS for line*/
        .toggler-line {
            height: 35px;
            width: 100px;
        }

        #canvas-line {
            width: 800px;
            height: 600px;
            border: 1px solid #ccc;
            position: absolute;
            top: 300px;
            left: 100px;
            z-index: 100;
        }

        #button-line {
            position: absolute;
            top: 300px;
            left: 110px;
            z-index: 600;
        }

            #button-line > input {
                padding: 10px;
                display: block;
                margin-top: 5px;
            }

        .welcome_text {
            padding-right: 11px;
        }

        #form1 > div {
            margin-left: 5%;
            margin-right: 5%;
        }
        /*end of entire CSS for line*/
        /*end of entire CSS for shapes*/
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
		$('#searchBox').keypress(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });
            $('#txtJumpToPage').keypress(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });
        });

        $(document).ready(function () {            
            
            $("#btnUseMe").click(function (e) {
                ShowUseMeDialog();
                e.preventDefault();
            });
            $("#btnUseMeDialogClose").click(function (e) {
                HideUseMeDialog();
                e.preventDefault();
            });
            $("#btnCloseChngPwd").click(function (e) {
                HideDialog();
                e.preventDefault();
            });
            $("#brands").attr("checked", false);
            $("#btnAccount").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });
            $("#btnHelps").click(function (e) {
                ShowGenericDialog1(true);
                e.preventDefault();
            });
            $("#btnClose").click(function (e) {
                HideDialog();
                e.preventDefault();
            });
            $("#btnDialogHelpClose").click(function (e) {
                HideGenericDialog1();
                e.preventDefault();
            });
            
            $("#btnChngPwd1").click(function (e) {
                if ($('#newPwdTxt').val() == '') {
                    alert('Enter the New password')
                    ShowDialog(true);
                    e.preventDefault();

                }
                if ($('#confirmPwdTxt').val() == '') {
                    alert('Enter the Confirm password')
                    ShowDialog(true);
                    e.preventDefault();

                }
                if ($('#newPwdTxt').val() != $('#confirmPwdTxt').val()) {
                    alert('Passwords are not matched.')
                    ShowDialog(true);
                    e.preventDefault();

                }

            });
        });
        function ShowDialog(modal) {
            $("#overlay").show();
            $("#dialog").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }

        function HideDialog() {
            $("#overlay").hide();
            $("#dialog").fadeOut(300);
        }
        function ShowGenericDialog1(modal) {
            $("#overlay").show();
            $("#genericDialogHelps").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideGenericDialog1() {
            $("#overlay").hide();
            $("#genericDialogHelps").fadeOut(300);
        }

        function ShowUseMeDialog(modal) {
            $("#overlay").show();
            $("#useMeDialog").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }

        function HideUseMeDialog() {
            $("#overlay").hide();
            $("#useMeDialog").fadeOut(300);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:HiddenField ID="hdnObjectUrl" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnBookMarkUrl" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnPageNumber" ClientIDMode="Static" runat="server" />
        <div>

            <!-- jQuery UI -->
            <script type="text/javascript" src="http://htmlviewer/Scripts/lib/jquery.1.10.2.min.js"></script>
            <script type="text/javascript" src="http://htmlviewer/Scripts/lib/jquery.ui.core.1.10.3.min.js"></script>
            <script type="text/javascript" src="http://htmlviewer/Scripts/lib/jquery.ui.widget.1.10.3.min.js"></script>
            <script type="text/javascript" src="http://htmlviewer/Scripts/lib/jquery.ui.mouse.1.10.3.min.js"></script>
            <script type="text/javascript" src="http://htmlviewer/Scripts/lib/jquery.ui.draggable.1.10.3.min.js"></script>
            <!--Top implementation start here-->
            <div id="overlay" class="web_dialog_overlay"></div>
            <div class="zoomCovePageDialog" id="useMeDialog" style="margin-top: -220px; display: none;">
            <div class="zoom_title">
                <h5 id="titleOfVideoDialog">How to</h5>
                <div id="btnUseMeDialogClose" class="zoom_close">
                    x
			
                    <!--<img src="assets/images/helpPopupCloseBtn.png"> -->
                </div>
            </div>
            <div class="inner_space" style="height: 300px;">
                <video controls="" id="popupVideoPlayer"  src="http://coursematerials.managementconcepts.com:8088/video/eBooksVid_160204.mp4">
                 
                </video>
            </div>
        </div>
            <div visibility="hidden" class="zoomCovePageDialog" id="genericDialogHelps" style="display: none; margin-top: -220px;">
            <div class="zoom_title">
                <h5 id="titleOfDialog">Help</h5>
                <div id="btnDialogHelpClose" class="zoom_close">
                    x
			
                <!-- <img src="assets/images/helpPopupCloseBtn.png"> -->
                </div>
            </div>
            <div class="inner_space" id="dialogMainContainer" style="text-align: left; height: 300px;">
                <!--?xml version="1.0" encoding="UTF-8"?-->



                <title>Help</title>
                <meta content="width=device-width, initial-scale=1.0" name="viewport">

                <style>
                    p.extract {
                        font-size: 1.1em;
                        font-family: arial;
                        text-align: left;
                        text-indent: 0em;
                        margin-bottom: 0em;
                        margin-top: 1em;
                    }

                    span.gray {
                        color: #444444;
                        font-family: arial;
                    }

                    p.hanging {
                        font-size: 1.1em;
                        text-indent: -1.8em;
                        margin-left: 3em;
                        margin-top: 0.3em;
                        margin-bottom: 0em;
                        text-align: left;
                    }

                    p.hanging1 {
                        font-size: 1.1em;
                        text-indent: -1.5em;
                        margin-left: 6em;
                        margin-top: 0.4em;
                        margin-bottom: 0em;
                        text-align: left;
                    }

                    span.big {
                        font-size: 2.2em;
                    }

                    p.center {
                        margin-top: 1em;
                        text-align: center;
                        margin-bottom: 1em;
                    }
                </style>




                <p class="extract">
                    <strong>What are electronic course materials?</strong><br>
                    <br>
                    Electronic course materials are digitally-enhanced versions of traditional print course materials that can be downloaded on a computer, desktop, laptop, or tablet.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Do I need to have a technical background to take a course with electronic course materials?</strong><br>
                    <br>
                    No. Your materials will be accessed through an electronic bookshelf that’s designed to be intuitive and easy to operate. Additionally, you have access to a video demonstration that walks you through the basic functionsaswell as the Help section, both located at  <a href="http://coursematerials.managementconcepts.com" target="_blank">http://coursematerials.managementconcepts.com</a>.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Do I still have to come to the class?</strong><br>
                    <br>
                    Yes. The bookshelf provides access to electronic course materials used in class for instructional purposes. It is not a replacement for attendance in the course itself.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>When and how will I gain access to the electronic course materials?</strong><br>
                    <br/>
                    You will receive a welcome emailfrom studentsupport@managementconcepts.comwith login credentials one week prior to the class start date. (Please check your Spam folder if you do not see the email at this time). The electronic course materials will be waiting on your bookshelf when you log infor the first time.
                    <br />
                    To log in, follow the instructions below:<br />
                    <ul>
                    <li>•	Access the course materials on your bookshelf at <a href="http://coursematerials.managementconcepts.com" target="_blank">http://coursematerials.managementconcepts.com</a> or by clicking the “Log On” link in thewelcome email (sent one week prior to class from <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a>. </li>
                    <li>•	Enter your email address into the User Name field and use the password provided in the welcome email</li>
                    <li>•	Agree to the Terms of Use</li>
                    <li>•	Create a new password</li>
                    </ul>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What should I do if no books are available on my bookshelf?</strong><br>
                    <br>
                    If there are no books on your bookshelf please contact Student Support toll-free at <b>844.876.7476</b> or email Student Support at <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a> for assistance.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What device do I bring to class to access my electronic course materials?</strong><br>
                    <br>
                    Management Concepts will supply you with a laptop to use during your class, so you will not need to bring a device to access your electronic course materials. 
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What do I need to do to prepare for my class?</strong><br>
                    <br>
                    If you will be using a laptop or desktop at home to access your electronic course materials outside of class, you will need to verify your browser by clicking <a href="http://www.managementconcepts.com/Portals/0/Files/BrowserCheck.pdf" target="_blank">here</a>. You also need to test your operating system as well as the access to your bookshelf by following the instructions below. 
                    <br />
                    For optimum performance of electronic course materials, please use the most current version of either of these browsers on your laptop: 
                    <br />
                    <ul>
                    <li>o	Windows: Internet Explorer 9, Google Chrome 26.0+, and Mozilla Firefox 20.0+</li>
                    <li>o	Mac: Safari 5.1+</li>
                    </ul>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What features are currently available on the electronic course materials bookshelf?</strong><br>
                    <br>
                    On your bookshelf you can search by title, author, or topic. You can view the bookshelf like a traditional bookshelf, or switch to a list view. Within the materials, you can annotate, take notes, bookmark pages for quick access, highlight, and search by keyword. Notes and bookmarks can be accessed from any device with an internet connection and a browser.
                </p>
                <br>
                 <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What should I do if no books are available on my bookshelf?</strong><br>
                    <br>
                    If there are no books on your bookshelf please contact Student Support toll-free at <b>844.876.7476</b> or email Student Support at <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a> for assistance.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What if I open my bookshelf and the wrong book is there?</strong><br>
                    <br>
                    Please contact Student Support toll-free at <b>844.876.7476</b> or email Student Support at student <a href="mailto:support@managementconcepts.com" target="_blank">support@managementconcepts.com</a> for help with your bookshelf titles.                    
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>I’m in the middle of reading a book for a class, how do I get back to my bookshelf quickly?</strong><br>
                    <br>
                    Hit the home button at the top of your toolbar.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>I’m reading a book and clicked on a hyperlink and now my book is no longer on the screen.</strong><br>
                    <br>
                    Any hyperlinks clicked while reading your book will open in a different tab of your browser. To navigate back to your book, close the tab that the hyperlink opened in.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>I clicked on the table of contents but now I can only see half of my book on the screen. What should I do?</strong><br>
                    <br>
                    In order to collapse the Table on Contents, click the white back arrow at the top of the Table of Contents pane once you navigate to the section you would like to read.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>The text on my screen is too small or too large, how can I make adjustments?</strong><br>
                    <br>
                    Zoom Control Instructions for Google Chrome (two options):<br />
                    To adjust the size of everything on the webpages you visit, including text, images, and videos:

                </p>
                <br>

                <!--
<p class="extract"><strong>When I open my book I choose not to go to my last bookmarked page and now I can not remember what chapter I was on, can I easily navigate back there?</strong></p>
<p class="extract"><strong>** we will have to ask Aptara what the best answer for this one is, what do they suggest?</strong></p>
-->
                  <p class="hanging">
                    1.&nbsp;&nbsp;&nbsp;&nbsp;Click the Chrome menu
                <img width="30px" height="30px" alt="" src="assets/images/help/menu.jpg">
                    on the browser toolbar
                </p>
                <p class="hanging">2.&nbsp;&nbsp;&nbsp;&nbsp;Select <strong>Settings</strong>.</p>
                <p class="hanging">3.&nbsp;&nbsp;&nbsp;&nbsp;Click <strong>Show advanced settings</strong>.</p>
                <p class="hanging">4.&nbsp;&nbsp;&nbsp;&nbsp;In the “Web Content” section, use the "Page zoom" drop-down menu to adjust the zoom.</p>
                <br>
                <p class="extract"><strong>To adjust the zoom on your current page: </strong></p>
                <p class="extract">
                    Use the zoom options in the Chrome menu to make everything on a webpage larger or smaller.<br>
                    <br>
                </p>

                <p class="hanging">
                    1.&nbsp;&nbsp;&nbsp;&nbsp;Click the Chrome menu
                <img width="30px" height="30px" alt="" src="assets/images/help/menu.jpg">
                    on the browser toolbar.
                </p>
                <p class="hanging">2.&nbsp;&nbsp;&nbsp;&nbsp;Find the “Zoom” section in the menu and choose one of the following options:</p>
                <p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Click
                <img width="30px" height="30px" alt="" src="assets/images/help/p.jpg">
                    to make everything on the page larger. You can also use the keyboard shortcuts <strong>Ctrl</strong> and <strong>+</strong> (Windows, Linux, and Chrome OS) and
                <img width="30px" height="30px" alt="" src="assets/images/help/q.jpg">
                    and <strong>+</strong> (Mac).
                </p>
                <p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Click
                <img width="30px" height="30px" alt="" src="assets/images/help/minus.jpg">
                    to make everything smaller. You can also use the keyboard shortcuts <strong>Ctrl</strong> and - (Windows, Linux, and Chrome OS) and
                <img width="30px" height="30px" alt="" src="assets/images/help/q.jpg">
                    and - (Mac).
                </p>
                <p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;To go into full-screen mode, click 
                <img width="30px" height="30px" alt="" src="assets/images/help/minus.jpg">
                    You can also use the keyboard shortcuts <strong>F11</strong> (Windows and Linux) and
                <img width="30px" height="30px" alt="" src="assets/images/help/fullscreen.png">
                    Shift-F (Mac). If you’re using Chrome OS, you can also press 
                    <img width="30px" height="30px" alt="" src="assets/images/help/OS.png">
                    at the top of your keyboard.
                </p>
                <%--<p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;To go into full-screen mode, click
                <img width="30px" height="30px" alt="" src="assets/images/help/1.jpg">. You can also use the keyboard shortcuts <strong>F11</strong> (Windows and Linux) and<img alt="" src="assets/images/help/q.jpg" class="inline"><strong>-Shift-F</strong> (Mac). If you’re using Chrome OS, you can also press
                <img width="50px" height="80px" alt="" src="assets/images/help/2.jpg">
                    at the top of your keyboard.
                </p>--%>

                <hr width="100%" height="1%">

                <p class="hanging">Zoom Control Instructions for Firefox:</p>
                <br>
                

                
                <p class="hanging1"><span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Click the menu button on the right. The customization menu will open and you will see the zoom controls at the top.</p>
                <p class="center">
                    <img alt="" src="assets/images/help/3.jpg">
                </p>
                <p class="hanging"><span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Use the + button to zoom in, and the - button to zoom out. The number in the middle is the current zoom level - click it to reset the zoom to 100%.</p>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>If I don't have internet access, can I still access electronic course materials?</strong><br>
                    <br>
                    An internet connection is only required when downloading the materials for the first time. Each browser has different capabilities; however, if it has already been downloaded, you will be able to accesselectronic materials for at least one course without a connection.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I read electronic course materials on my bookshelf from home?</strong><br>
                    <br>
                    You can read your electronic course materials from home or anywhere else with an internet connection. If it has already been downloaded, you will be able access electronic course materials for at least one course without an internet connection from home.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What internet browsers can I use to access and read electronic course materials?</strong><br>
                    <br>
                    <strong>Recommended:</strong><br />
                    o	Windows: Internet Explorer 9, Google Chrome 26.0+, Mozilla Firefox 20.0+<br />
                    o	Mac: Safari 5.1+<br />
                    <strong>Not recommended:</strong><br />
                    •	Windows: Internet Explorer V9 and below<br />
                    •	Mac: Safari 5.1 and below
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I access and read electronic course materials at home and pick up where I left off in class?</strong><br>
                    <br>
                    Yes and your notes, bookmarks, highlights, and favorites are available as well. When you login and open your course materials from home, you will be asked if you would like to return to your last page.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>How do I bookmark a page so I can follow along in class?</strong><br>
                    <br>
                    Use the "Favorites" button in the top navigation bar to bookmark a specific page and easily access it at a later date. If you are unsure which icon represents "Favorites," click the "Question Mark" for an icon key.
                </p>
                <br>
                 <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I cut and paste from the text to take notes and send them to my personal email?</strong><br>
                    <br>
                    Yes, you can copy and paste text into any email of your choosing.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What do I get to take home after the class?</strong><br>
                    <br>
                    You will have unlimited, continuous access to the current version of the electronic course materials for three years. Management Concepts-provided laptops are for classroom use only and must be returned at the end of class.
                </p>
                <br>
                 <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I access and read electronic course materials on my Kindle?</strong><br>
                    <br>
                    Electronic course materials are not supported on Kindle devices at this time. 
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I access and read electronic course materials on my Nook?</strong><br>
                    <br>
                    Electronic course materials are not supported on Nook devices at this time.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>How do I register for a course?</strong><br>
                    <br>
                    The registration process for a class using electronic course materials is the same as any other Management Concepts class – either online at <a href="http://www.ManagementConcepts.com" target="_blank">http://www.ManagementConcepts.com</a> on the selected course page or via phone at <strong>888.545.8571</strong>. For a complete list of courses, please visit <a href="http://www.ManagementConcepts.com/ElectronicCourseMaterials" target="_blank">http://www.ManagementConcepts.com/ElectronicCourseMaterials</a>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>How much does it cost to register for a class using electronic course materials?</strong><br>
                    <br>
                    There is no difference in the price of a class that uses electronic course materials.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Is technical assistance available throughout for the class?</strong><br>
                    <br>
                    Yes. If you need help getting into the portal, or resolving other technical issues, contact our technical support team, toll-free, at <strong>844.876.7476</strong> or send an email to <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What if I forget my password?</strong><br>
                    <br>
                    If you forgot your password, go to the login screen at <a href="http://coursematerials.managementconcepts.com" target="_blank">http://coursematerials.managementconcepts.com</a> and click “Forgot Password”. Enter your email address and a new password will be sent to you.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I print the course content?</strong><br>
                    <br>
                    Yes, electronic course materials can be printed but not until the class has been completed.
                </p>
                <br>
            </div>
        </div>
            <div visibility="hidden" class="zoomCovePageDialog web_dialog" id="dialog" style="margin-top: -220px; display: none;">
            <div class="zoom_title">
                <h5>Change Password</h5>
            </div>
            <div class="inner_space" id="changePwdContainer" style="height: 300px;">



                <div class="password_box">

                    <div class="form-horizontal">
                        <div class="form-group1">
                            <label class="col-sm-3 control-label" for="inputEmail3">
                                Old
							Password</label>
                            <div class="col-sm-9">
                                <input type="password" autofocus="" placeholder="Old Password" id="oldpwd" name="oldpwd" class="form-control" runat="server" />
                            </div>
                        </div>

                        <div class="form-group1">
                            <label class="col-sm-3 control-label" for="inputPassword3">
                                New
							Password</label>
                            <div class="col-sm-9">
                                <input type="password" placeholder="New Password" id="newpwd" name="newpwd" class="form-control" runat="server" />
                            </div>
                        </div>

                        <div class="form-group1">
                            <label class="col-sm-3 control-label" for="inputPassword3">
                                Confirm
							Password</label>
                            <div class="col-sm-9">
                                <input type="password" placeholder="Confirm Password" id="confirmPwd" name="confirmPwd" class="form-control" runat="server" />
                            </div>
                        </div>
                        <%--<div class="form-group col-sm-12 forgotPassword">
                            <asp:Literal ID="lblMessage" runat="server"></asp:Literal>
                            </div>--%>
                        <div class="form-group1 col-sm-12 forgotPassword">
                            <button class="btn btn-default pass_cancel pull-right" id="btnCloseChngPwd" type="button" style="display: block;" runat="server" onclick="btnCloseChngPwd_Click">Close</button>
                            <%--<button class="btn btn-default pull-right pass_submit" runat="server" id="btnChngPwd" type="button" onclick="btnChngPwd_Click">Submit</button>--%>
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnChngPwd1" EventName="Click" />
                                            </Triggers>
                                            <ContentTemplate>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                            <asp:Button ID="btnChngPwd1" runat="server" Text="Submit" CssClass="btn btn-default pass_cancel pull-right" OnClick="btnChngPwd1_Click" />
                            

                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
            <!--Top implementation end here-->
            <div class="buttons" style="display: none;">

                <a href="#" class="btn btn-primary btn-danger showhideCanvas"><i class="fa fa-eraser"></i>Disable Annotation</a>
            </div>
            <div id="viewer">
                <div role="navigation" class="header navbar navbar-default" id="mainHeader" style="width:auto; padding-right:10px;">
                    <div class="navbar-header">
                        <div class="white_patch" id="white_patch" style="display: none;"></div>
                        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </button>
                        <a dontchange="true" href="#" class="navbar-brand">
                            <img src="images/logo.jpg" id="logo" style="top: 18px;"></a>
                    </div>

                    <div class="nav_menu navbar-collapse collapse" id="menuBar">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="welcome_text"><span><a id="welcomeUserTxt" target="_blank">
                                <asp:Label ID="lblUserName" runat="server"></asp:Label></a></span>
                                <%--<span><a class="onlineIndicator online" id="onlineIndicator" target="_blank"></a></span>--%>

                            </li>
                            <li><a id="btnAccount" target="_blank">Account</a></li>
                            <li><a id="btnLogout" href="http://coursematerials.managementconcepts.com/login.aspx" target="_parent">Logout</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->


                    <div class="navSub_menu navbar-collapse collapse" id="subMenuBar" style="clear:both">
                        <ul class="nav navbar-nav navbar-right">
                           <%-- <li><a target="_blank" href="http://www.managementconcepts.com/Contact-Us/Customer-Service-Center">Feedback</a></li>
                            <li><a id="btnUseMe" target="_blank">How to</a></li>
                            <li><a id="btnHelps" target="_blank">Help</a></li>--%>

                            <li> <a style="text-decoration:none;" href="mailto:studentsupport@managementconcepts.com?subject=Electronic%20Materials%20Support">Contact Us</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
                <script charset="utf-8" type="text/javascript">
                    function addCssDynamically(cssPath) {

                        $("link").each(function () {
                            if ($(this).attr("changeTheme") === "true") {
                                $(this).attr('href', cssPath);
                            }
                        });

                    }
	</script>
                <div class="heading">

                    <h4 id="bookHeading" style="width:62%;">
                        <asp:Label ID="lblCourseName" runat="server"></asp:Label>
                    </h4>

                    <div class="social_space">
                        <div class="social_link">
                            <div id="containerBlock" class="holderCSS">
                                <div id="dividerBtn" class="dividerCSS"></div>
                                <div style="display: none;" id="rightItem" class="social_set">
                                    <div class="social_border">
                                        <%--<a style="display: none;" id="btnGlobalAction" title="Global Action Plan" class="global_plan">&nbsp;</a>--%>
                                        <asp:LinkButton ID="globalNotesBtn2" runat="server" ToolTip="Global Notes" CssClass="global_notes" Style="display: block;" OnClick="globalNotesBtn_Click"></asp:LinkButton>
                                        <%--<asp:LinkButton ID="globalNotesBtn" runat="server" CssClass="global_notes" Style="display: block;" ToolTip="Global Notes" OnClick="globalNotesBtn_Click"></asp:LinkButton>--%>
                                    </div>
                                    <div class="social_border">
                                        <a style="display: none;" id="btnAppendix" title="Appendix" class="appendix">&nbsp;</a>
                                        <a style="display: block;" href="#" title="Search" id="search" class="search">&nbsp;</a>

                                        <a style="display: block;" href="~/pdfViewer.aspx" title="Print" id="btnPrinters" class="print" runat="server" target="_blank">&nbsp;</a>
                                        <%--<a style="display: block;" href="~/Oracle_9i_PL_SQL.pdf" title="Print" download="" id="btnPrint" class="print" runat="server" target="_blank">&nbsp;</a>--%>
                                        <%--<a href="path_to_file" download="proposed_file_name">Download</a>--%>
                                        <%--<a style="display: block;" href="#" id="btnAddRemoveFav" title="Add to Favorites" class="add_favorites favorites_remove">&nbsp;</a>--%>
                                        <asp:UpdatePanel ID="uPanelAddFav" runat="server">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnAddRemoveFav" EventName="Click" />
                                            </Triggers>
                                            <ContentTemplate>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:LinkButton ID="btnAddRemoveFav" runat="server" ToolTip="Add to Favorites" CssClass="add_favorites favorites_remove" OnClick="btnAddRemoveFav_Click"></asp:LinkButton>
                                        <%--<a style="display: block;" href="#" id="btnViewFav" title="View Favorites" class="view_favorites">&nbsp;</a>--%>
                                        <asp:LinkButton ID="btnViewFav2" runat="server" ToolTip="View Favorites" CssClass="view_favorites" Style="display: block;" OnClick="btnViewFav_Click"></asp:LinkButton>
                                        <a style="display: block;" id="sync" href="#" title="Sync" class="sync">&nbsp;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="social_set">
                                <a style="display: block;" href="#" title="Help" id="help" class="help">&nbsp;</a>
                                <a style="display: block;" id="courseExercise" title="Download Resource" class="full_screen" href="#" runat="server">&nbsp;</a>
                                <%--<a style="display: block;" id="maximize" title="Full Screen" class="full_screen">&nbsp;</a>--%>
                                <a style="display: block;" id="btnHome" href="http://coursematerials.managementconcepts.com/dashboard.aspx" title="Home" class="home">&nbsp;</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix" id="clearID"></div>
                    <div id="specialstuff" style="display: none;">
                        </p><p>Status: <span id="fsstatus" class="fullScreenSupported">Back to normal</span></p>
                    </div>
                </div>
                <div visibility="hidden" class="zoomCovePageDialog NotePopup" id="genericDialog" style="display: none; margin-top: -220px;">
                    <div class="zoom_title">
                        <h5 id="titleOfDialog">Global Notes</h5>

                        <a style="display: block;" title="Print" id="printNote" class="print" runat="server" target="_blank">&nbsp;</a>

                        <div id="btnDialogClose" class="zoom_close">
                            x
			
                            <!-- <img src="assets/images/helpPopupCloseBtn.png"> -->
                        </div>
                    </div>
                    <div class="inner_space" id="dialogMainContainer1" style="text-align: left; height: 300px;" runat="server">
                        <asp:Label ID="lblCourseDescription" runat="server" Text=""></asp:Label>
                        <hr />
                        <asp:UpdatePanel ID="uPanelRepDetails" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="globalNotesBtn2" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:Repeater ID="RepDetails" runat="server">
                                    <ItemTemplate>
                                        <div id="2" class="globalNoteWrapper">
                                            <div class="rowContent1">
                                                <div class="titleTxt" id="notePageNumber">Page:
                                                    <asp:Label ID="lblpageno" runat="server" Font-Bold="true" Text='<%#Eval("pageno") %>' />
                                                </div>
                                                <div class="infoTxt" id="noteContent">
                                                    <asp:Label ID="LabelBookMarkContents" runat="server" Font-Bold="true" Text='<%#Eval("BookMarkContents") %>' />
                                                    <%--<a onclick="stopRedirectBookMark(this);" href='<%#Eval("BookMarkContents") %>'></a>--%>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div visibility="hidden" class="zoomCovePageDialog helpDialog" id="Div1" style="display: none; margin-top: -220px;">
                    <div class="zoom_title">
                        <h5 id="H1">Help</h5>
                        <div id="Div2" class="zoom_close close_help">
                            x
			
                            <!-- <img src="assets/images/helpPopupCloseBtn.png"> -->
                        </div>
                    </div>
                   <div class="inner_space" id="Div3" style="text-align: left; height: 300px;">
                        <div class="helpMain">
                            <div class="helpRow" id="helpListData1">
                                <div class="rowContent">
                                    <div class="titleTxt">TOC (Table of Contents)</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/menu_help.png" alt="TOC (Table of Contents)">
                                    </div>
                                    <div class="infoTxt">Click to see your current location within the book or jump to another section.</div>
                                </div>
                            </div>
                           
                           
                            <div class="helpRow" id="helpListData4">
                                <div class="rowContent">
                                    <div class="titleTxt">Global Notes</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/global_notes_help.png" alt="Global Notes">
                                    </div>
                                    <div class="infoTxt">Click to see the notes you have taken throughout the book (delineated by page).</div>
                                </div>
                            </div>
                          <%--  <div class="helpRow" id="helpListData5">
                                <div class="rowContent">
                                    <div class="titleTxt">Appendix</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/appendix_help.png" alt="Appendix">
                                    </div>
                                    <div class="infoTxt">Lets you access the Appendix for this title.</div>
                                </div>
                            </div>--%>
                            <div class="helpRow" id="helpListData6">
                               
                                 <div class="rowContent">
                                    <div class="titleTxt">Search</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/search_help.png" alt="Search">
                                    </div>
                                    <div class="infoTxt">Click to enter search terms and find their location throughout the book.</div>
                                </div>
                            </div>
                            <div class="helpRow" id="helpListData7">  
                                  <div class="rowContent">
                                    <div class="titleTxt">Print</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/print_help.png" alt="Print">
                                    </div>
                                    <div class="infoTxt">Click to print the current page.</div>
                                </div>
                            </div>
                            <div class="helpRow" id="helpListData8">
                               <div class="rowContent">
                                    <div class="titleTxt">Add to Favorites</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/add_favourite_help.png" alt="Add to Favorites">
                                    </div>
                                    <div class="infoTxt">Click to add the current page to your favorites.</div>
                                </div>
                            </div>
                            <div class="helpRow" id="helpListData9">
                                <div class="rowContent">
                                    <div class="titleTxt">View Favorites</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/view_favourite_help.png" alt="View Favorites">
                                    </div>
                                    <div class="infoTxt">Click to view a list of your favorites in the book.</div>
                                </div>
                            </div>
                            <div class="helpRow" id="helpListData10">
                                 <div class="rowContent">
                                    <div class="titleTxt">Sync</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/sync_help.png" alt="Sync">
                                    </div>
                                    <div class="infoTxt">Click on the Sync button for the options to Sync <b>from</b> the Web or Sync <b>to</b> the Web.
If you select Sync from the Web, you will receive a message that the Sync from the Web is complete.
If you select Sync to the Web, you will receive a message that the Sync to the Web is complete
.</div>
                                </div>
                            </div>
                            <div class="helpRow" id="helpListData11">
                               <div class="rowContent">
                                    <div class="titleTxt">Help</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/help_help.png" alt="Help">
                                    </div>
                                    <div class="infoTxt">Click to access Help</div>
                                </div>
                            </div>
                            <div class="helpRow" id="helpListData12">
                              
                                   <div class="rowContent">
                                    <div class="titleTxt">Download Resource</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/download.png" alt="Full Screen">
                                    </div>
                                    <div class="infoTxt">Click to download supplement material, if any.</div>
                                </div>
                                  <div class="rowContent">
                                    <div class="titleTxt">Home</div>
                                    <div class="iconSec icoHelp">
                                        <img src="assets/images/help/home_help.png" alt="Home">
                                    </div>
                                    <div class="infoTxt">Click to return to the home screen.</div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                
                <div visibility="hidden" class="zoomCovePageDialog SearchGoDialog" id="genericDialog" style="display: none;">
                    <div class="zoom_title">
                        <h5 id="titleOfDialog1">Search</h5>
                        <div id="close_SearchGo" class="zoom_close">
                            x
			
                            <!-- <img src="assets/images/helpPopupCloseBtn.png"> -->
                        </div>
                    </div>
                    <div class="inner_space" id="dialogMainContainer2" style="overflow-y: hidden;">

                        <div class="search_pop">
                            <%--<input type="text" id="searchBox" placeholder="Search" class="search">--%>
                            <asp:TextBox ID="searchBox" placeholder="Search" class="search" runat="server"></asp:TextBox>
                            <%--<span id="searchBtn" class="search_btn">&nbsp;</span>--%>
                            <asp:Button ID="searchBtn" OnClientClick="BlankSearch()" class="search_btn" OnClick="searchBtn_Click" runat="server" />

                            <span class="searchReset" onclick="clearSearch()" id="searchReset">Reset</span>
                            <div class="input_page">
                                <span class="cssBtnJumpToPage">Jump To: </span>
                                <asp:TextBox ID="txtJumpToPage" ClientIDMode="Static" placeholder="Page No" class="cssJumpToPage" runat="server"></asp:TextBox>
                                <%--<input type="text" placeholder="Page No" id="txtJumpToPage" class="cssJumpToPage" runat="server" />--%>
                                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnJumpToPage" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>--%>
                                <%--<asp:LinkButton ID="btnJumpToPage" ClientIDMode="Static" OnClientClick="GoSpecificPage()" class="cssBtnJumpGo" runat="server">Go</asp:LinkButton>--%>
                                <%--</ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                <input id="btnJumpToPage" onclick="GoSpecificPage()" class="cssBtnJumpGo" type="button" value="Go" runat="server" />
                                <%--<a id="btnJumpToPage" href="javascript:void(0)" onclick="GoSpecificPage()" class="cssBtnJumpGo">Go</a>--%>
                                <span id="errmsg" class="cssErrMsg"></span>
                            </div>
                            <div id="loading"></div>
                            <div class="inner_space" id="dialogMainContainerSearch" style="text-align: left; height: 238px;">
                                <asp:UpdatePanel ID="upSearcg" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="searchBtn" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptSearch" runat="server" OnItemDataBound="rptSearch_ItemDataBound">
                                            
                                            <ItemTemplate>
                                                <div id="3" class="globalNoteWrapper">
                                                    <div class="rowContent">
                                                        <%--<li class="favListData" id="favListData11345">--%>
                                                        <div class="titleTxt" id="notePageNumber">Page:
                                                            <asp:Literal ID="pageNum" Text='<%# DataBinder.Eval(Container, "DataItem.pageno") %>' runat="server"></asp:Literal>
                                                            </div>
                                                        <div class="infoTxt" id="noteContent">
                                                            <!--<a href='<%# DataBinder.Eval(Container, "DataItem.url") %>' runat="server"><%# DataBinder.Eval(Container, "DataItem.title") %></a>-->
                                                            <a onclick="stopRedirectSearch(this);" href='#<%# DataBinder.Eval(Container, "DataItem.url") %>'><%# DataBinder.Eval(Container, "DataItem.title") %></a>
                                                        <%--</li>--%>
                                                            </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>                                               
                                                        <asp:Label ID="lblEmptyData"
                                                            Text="No data found to display" runat="server" Visible="false">
                                                        </asp:Label>                                                        
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress style="padding-left: 44%;" ID="upProgress" AssociatedUpdatePanelID="upSearcg" runat="server">
                                    <ProgressTemplate>
                                        <img src="http://preloaders.net/preloaders/287/Filling%20broken%20ring.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                        <div class="searchResult" id="searchResult" style="display: block;">
                            <ul></ul>
                        </div>



                    </div>
                </div>
                <div visibility="hidden" class="zoomCovePageDialog favoriteDialog" id="genericDialogFavourite" style="display: none; margin-top: -220px;">
                    <div class="zoom_title">
                        <h5 id="titleOfDialogFav">Favorites</h5>
                        <div id="btnDialogCloseFav" class="zoom_close close_favorite">
                            x
                        </div>
                    </div>
                                       

                    <div class="inner_space" id="dialogMainContainerFavourite" style="text-align: left; height: 300px;">
                        <asp:Label ID="lblBookName" runat="server" Text=""></asp:Label>
                        <asp:UpdatePanel ID="uPanelFavourite" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnViewFav2" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:Repeater ID="Repeaterfav" runat="server">
                                    <ItemTemplate>
                                        <div id="3" class="globalNoteWrapper">
                                            <div class="rowContent">
                                                <%--<li class="favListData" id="favListData11345">--%>
                                                    <div class="titleTxt" id="notePageNumber">Page:
                                                    <asp:Literal ID="pageNum" Text='<%# DataBinder.Eval(Container, "DataItem.pagename") %>' runat="server">.</asp:Literal>
                                                    </div>
                                                        <!--<a href='<%# DataBinder.Eval(Container, "DataItem.PageURL") %>' runat="server"><%# DataBinder.Eval(Container, "DataItem.PageTitle") %></a>-->
                                                    <div class="infoTxt" id="noteContent">
                                                    <a onclick="stopRedirect(this);" href='#<%# DataBinder.Eval(Container, "DataItem.PageURL") %>'><%# DataBinder.Eval(Container, "DataItem.PageTitle") %></a>
                                                </div>
                                               <%--</li>--%>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div class="bookMarkPopup hide show" id="bookMarkPopup">
                    <div class="bmTitle">
                        <h5 id="bmPopupTitle">Bookmark Message</h5>
                    </div>
                    <div class="bmPopupMainTextPop" id="bmPopupMainText">Do you want to go to your last bookmark page?</div>
                    <div class="bmPopControls" id="bmPopControls">
                        <button class="btn btn-default" id="bmYesBtn" type="button">Yes</button>
                        <button class="btn btn-default" id="bmNoBtn" type="button">No</button>
                    </div>
                </div>
                <%-- <div id="maximizePopupTop" class="bookMarkPopup hide">
                    <div class="bmTitle">
                    </div>
                    <div id="mPopupMainText" class="bmPopupMainTextPop">Press Alt + F4 to close Full Screen Mode</div>                    
                </div>--%>
                <div id="maximizePopup" class="bookMarkPopup hide">
                    <div class="bmTitle">
                        <%--<h5 id="mPopupTitle">Bookmark Message</h5>--%>
                    </div>
                    <div id="mPopupMainText" class="bmPopupMainTextPop">
                        Do you want to allow Full Screen Mode?<br />
                        <br />
                        Press Alt + F4 or ESC key to close Full Screen Mode at any time
                    </div>
                    <div id="mPopControls" class="bmPopControls">
                        <button type="button" id="mYesBtn" class="btn btn-default">Yes</button>
                        <button type="button" id="mNoBtn" class="btn btn-default">No</button>
                    </div>
                </div>


                <div id="canvasContainer">
                    <div id="canvas" style="height: 100%;">
                       
                        <object id="objViewer" type="text/html" data="" width="100%" height="700px"></object>
                            
                      <%--  <iframe id="objViewer" src="" height="100%" width="100%"></iframe>--%>
                       <%-- <script>
                            /*globals $, EmbeddedSVGEdit*/
                            var initEmbed;
                            $(function () {
                                'use strict';

                                var svgCanvas = null;

                                initEmbed = function () {
                                    var doc, mainButton,
                                        frame = document.getElementById('svgedit');
                                    svgCanvas = new EmbeddedSVGEdit(frame);
                                    // Hide main button, as we will be controlling new, load, save, etc. from the host document
                                    doc = frame.contentDocument || frame.contentWindow.document;
                                    mainButton = doc.getElementById('main_button');
                                    mainButton.style.display = 'none';
                                };

                                function handleSvgData(data, error) {
                                    if (error) {
                                        alert('error ' + error);
                                    } else {
                                        alert('Congratulations. Your SVG string is back in the host page, do with it what you will\n\n' + data);
                                    }
                                }

                                function loadSvg() {
                                    var svgexample = '<svg width="640" height="480" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><g><title>Layer 1<\/title><rect stroke-width="5" stroke="#000000" fill="#FF0000" id="svg_1" height="35" width="51" y="35" x="32"/><ellipse ry="15" rx="24" stroke-width="5" stroke="#000000" fill="#0000ff" id="svg_2" cy="60" cx="66"/><\/g><\/svg>';
                                    svgCanvas.setSvgString(svgexample);
                                }

                                function saveSvg() {
                                    svgCanvas.getSvgString()(handleSvgData);

                                }

                                // Add event handlers
                                $('#load').click(loadSvg);
                                $('#save').click(saveSvg);
                                
                            });
                        </script>--%>

                    </div>
                </div>
                <div style="height: 859px; display: none;" class="popupOverlay" id="popupOverlay">
                </div>
                <div class="clearafter">

                    <div class="footer_container" id="footer_container">

                        <div id="footerpane" class="footerpane_style" style="display: none;">
                            <div class="col-xs-12 col-sm-4">
                                <h2>CONTACT US</h2>
                                <span class="contact_us">Management Concepts is the
							nation's premier provider of training and professinal development
							solutions serving the public and private sectors.</span> <span class="contact_us">8230 LeesburgPike, Tysons Corner, VA
							22182</span> <span class="contact_us">888.545.8571</span> <a href="mailto:info@ManagementConcepts.com" target="_blank"><span>Info@ManagementConcepts.com</span></a>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <h2>TRAINING BY TOPIC</h2>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Acquisition%20-%20Contracting" target="_blank">
                                        <span>FederalAcquisition &amp; Contracting</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Financial%20Management" target="_blank">
                                        <span>Federal Financial Management</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Leadership%20-%20Management" target="_blank">
                                        <span>Leadership &amp; Management</span>
                                    </a>/
					
                                </p>
                                <p>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Grants%20-%20Assistance" target="_blank">
                                        <span>Federal Grants &amp; Assistance</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Project%20-%20Program%20Management" target="_blank">
                                        <span>Federal Project &amp; Program Management</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Human%20Capital%20and%20Human%20Resources" target="_blank">
                                        <span>Federal Human Capital &amp; Human Resources</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/CertificatePrograms" target="_blank">
                                        <span>Certificate Programs</span>
                                    </a>
                                </p>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                <h2>POPULAR LINKS</h2>
                                <p>
                                    <a href="http://www.managementconcepts.com/FormsCollection/RequestaCatalog" target="_blank">
                                        <span>Request a Catalog</span>
                                    </a>/
					
                                </p>
                                <p>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/How-to-Buy-from-Us" target="_blank">
                                        <span>How to Buy from Us</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Forms-Collection/Grants-ENewsletter-Signup" target="_blank">
                                        <span>Grants E-Newsletter</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Resources/Download-Free-White-Papers" target="_blank">
                                        <span>Request a Whitepaper</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://ecampus.managementconcepts.com" target="_blank"><span>eCampus</span></a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/About-Us/Careers" target="_blank"><span>Careers</span></a>/
					
                                </p>
                                <p>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Contact-Us/Classroom-Rental-Facilities" target="_blank">
                                        <span>Classroom Rental</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Contact-Us/Frequently-Asked-Questions" target="_blank">
                                        <span>FAQ</span>
                                    </a>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="footerTop_border">
                            <div class="footer_left copyright_bar">
                                <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="Footer">Copyright
							&copy; 2016 Management Concepts</span> | <a href="http://www.managementconcepts.com/Contact-Us/Terms-and-Conditions" class="Footer" target="_blank">Terms
							and Conditions </a>| <a href="http://www.managementconcepts.com/Privacy-Policy" class="Footer" target="_blank">Privacy
							Policy</a>
                            </div>


                            <div class="socialpane_style">
                                <div class="Social-Links">
                                    <a class="iconFacebook" title="Facebook" href="http://www.facebook.com/managementconcepts" target="_blank"></a><a class="iconTwitter" title="Twitter" href="http://twitter.com/#!/mgmt_concepts" target="_blank"></a><a class="iconLinkedIn" title="LinkedIn" href="http://www.linkedin.com/company/management-concepts" target="_blank"></a><a class="iconYouTube" title="YouTube" href="http://www.youtube.com/mgmtconcepts" target="_blank"></a>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function showFavorites() {
                    ShowFavoriteDialog();
                    event.preventDefault();
                }
                function showGlobalNotes() {
                    ShowGenericDialog();
                    event.preventDefault();
                }
                function ShowGenericDialog(modal) {
                    $("#popupOverlay").show();
                    $("#genericDialog").fadeIn(300);

                    if (modal) {
                        $("#popupOverlay").unbind("click");
                    }
                    else {
                        $("#popupOverlay").click(function (e) {
                            HideDialog();
                        });
                    }
                }
                function ShowFavoriteDialog(modal) {
                    $("#popupOverlay").show();
                    $(".favoriteDialog").fadeIn(300);

                    if (modal) {
                        $("#popupOverlay").unbind("click");
                    }
                    else {
                        $("#popupOverlay").click(function (e) {
                            HideDialog();
                        });
                    }
                }

                var objectDataChrome = '';
                var userid = '';
                var courseid = '';
                var juliandate = '';
                var pageGoUrl = '';
                function stopRedirect(link) {
                    objectData = '';
                    var href = $(link).attr('href');

                    var href1 = href.substring(1, href.length);

                    objectData = href1;
                    
                    $(".favoriteDialog").css('display', 'none')
                    $("#popupOverlay").css('display', 'none')
                    $("#objViewer").attr("data", objectData);
                    //event.preventDefault();
                    //dialogMainContainerSearch
                    var isChrome = window.chrome;
                    //if (isChrome) {
                        var strChrome = objectData.split("?");
                        var objectDataChrome = strChrome[0];
                        $("#objViewer").attr("data", objectDataChrome);


                        setTimeout(function () {
                            $('body').trigger('click');
                            $("#objViewer").attr("data", objectData);
                        }, 1200);
                    //}
                        $.ajax({
                            type: "POST",
                            url: "index.aspx/Bookmark",
                            data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + objectData + "', juliandate: '" + juliandate + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            cache: false,
                            success: function (data) {
                            }
,
                            failure: function (response) {
                                alert('F');

                            }
                        });
                    return false;
                }
                function clearSearch(link) {
                    $('#searchBox').val('');
                    $("#dialogMainContainerSearch").css("display", "none");
                }
                function BlankSearch(link) {
                    var searchData = $('#searchBox').val();

                    if (searchData == '') {
                        $("#dialogMainContainerSearch").css("display", "none");
                    }
                    else {
                        $("#dialogMainContainerSearch").css("display", "block");
                        $('#dialogMainContainerSearch').animate({ scrollTop: 0 }, '500');
                    }

                }
			   // function stopRedirectBookMark(link) {alert(link)
               //     objectData = '';
               //     var href = $(link).attr('href');

               //     var href1 = href.substring(1, href.length);

               //     objectData = href1;
               //     var str = objectData.split("#");

               //     objectData = str[0] + "?" + "uid=" + userid + "&jid=" + juliandate + "&cid=" + courseid + "#" + str[1];

               //     $.ajax({
               //         type: "POST",
               //         url: "index.aspx/Bookmark",
               //         data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + objectData + "', juliandate: '" + juliandate + "'}",
               //         contentType: "application/json; charset=utf-8",
               //         dataType: "json",
               //         cache: false,
               //         success: function (data) {
               //         }
               //,
               //         failure: function (response) {
               //             alert('F');

               //         }
               //     });
               // }
                function stopRedirectSearch(link) {
                    objectData = '';
                    var href = $(link).attr('href');

                    var href1 = href.substring(1, href.length);

                    objectData = href1;
                    var str = objectData.split("#");

                    objectData = str[0] + "?" + "uid=" + userid + "&jid=" + juliandate + "&cid=" + courseid + "#" + str[1];
                    
                    $.ajax({
                        type: "POST",
                        url: "index.aspx/Bookmark",
                        data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + window.location.href + "', juliandate: '" + juliandate + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                        }
              ,
                        failure: function (response) {
                            alert('F');

                        }
                    });

                    $(".SearchGoDialog").css('display', 'none')
                    $("#popupOverlay").css('display', 'none')
                    $("#objViewer").attr("data", objectData);

                    var isChrome = window.chrome;
                    var ie_version = getIEVersion();
                    var is_ie10 = ie_version.major == 10;

                    var win;
                    var isAtLeastIE11 = !!(navigator.userAgent.match(/Trident/) && !navigator.userAgent.match(/MSIE/));

                    //if (ie_version.major ==8 ||ie_version.major ==9 || ie_version.major ==10 || isAtLeastIE11==true) {
                    if (isChrome || ie_version.major == 8 || ie_version.major == 9 || ie_version.major == 10 || ie_version.major == 11 || isAtLeastIE11 == true) {
                        $("#objViewer").attr("height", "800px");
                        var strChrome = objectData.split("?");
                        var objectDataChrome = strChrome[0];
                        $("#objViewer").attr("data", objectDataChrome);

                        setTimeout(function () {
                            $('body').trigger('click');
                            $("#objViewer").attr("data", objectData);
                        }, 1200);
                    }
					$.ajax({
                        type: "POST",
                        url: "index.aspx/Bookmark",
                        data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + objectData + "', juliandate: '" + juliandate + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                        }
               ,
                        failure: function (response) {
                            alert('F');

                        }
                    });
                    return false;
                }
                function getIEVersion() {
                    var agent = navigator.userAgent;
                    var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
                    var matches = agent.match(reg);
                    if (matches != null) {
                        return { major: matches[1], minor: matches[2] };
                    }
                    return { major: "-1", minor: "-1" };
                }
                function GlobalButtonClick() {
                    ShowGenericDialog();
                    e.preventDefault();
                }

                function GoSpecificPage() {
                    $.ajaxSetup({ cache: false });
                    //event.stopPropagation();
                    var pageNo = $('#txtJumpToPage').val();
                    $(".SearchGoDialog").css('display', 'none')
                    $("#popupOverlay").css('display', 'none')
                    $("#bookMarkPopup").removeClass("bookMarkPopup hide show").addClass("bookMarkPopup hide")
                    $("#popupOverlay").css('display', 'none')
                    
                    $.ajax({
                        type: "POST",
                        url: "index.aspx/GotoSpecificpage",
                        data: "{cid: '" + courseid + "',jid: '" + juliandate + "',pageNo: '" + pageNo + "',uid: '" + userid + "' }",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var objdata = JSON.stringify(data.d);

                            if (data.d != null && data.d != "") {
                                objectData = $("#objViewer").attr("data");

                                var str = objectData.split("#");
                                objectData = str[0] + "#" + data.d;


                                $(".SearchGoDialog").css('display', 'none')
                                $("#popupOverlay").css('display', 'none')
                                $("#objViewer").attr("data", objectData);
                                //var isIE9 = document.addEventListener

                                var isChrome = window.chrome;
                                //var isIE = $.browser.msie;
                                //if (isChrome) {
                                    var strChrome = objectData.split("?");
                                    var objectDataChrome = strChrome[0];
                                    $("#objViewer").attr("data", objectDataChrome);
                                    setTimeout(function () {
                                        $('body').trigger('click');
                                        $("#objViewer").attr("data", objectData);
                                    }, 1500);
                                //}
                            }
                            else {
                                alert('Entered page number is out of range.Please try again!');
                            }
                        },
                        failure: function (response) {
                            alert('fail');

                        }
                    });
                    
                }

                window.onload = function () {


                    var isChrome = window.chrome;
                    var objHeightInChrome = $("#objViewer").attr("height")
                    if (isChrome && objHeightInChrome == "700px") {
                        $("#objViewer").attr("height", "800px");
                    }
                    //if (isChrome && objHeightInChrome == "1800px") {
                    //    $("#objViewer").attr("height", "700px");
                    //}

                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_initializeRequest(InitializeRequest);
                    prm.add_endRequest(EndRequest);
                    var postBackElement;
                    function InitializeRequest(sender, args) {
                        if (prm.get_isInAsyncPostBack())
                            args.set_cancel(true);
                        postBackElement = args.get_postBackElement();

                        if (postBackElement.id == 'searchBtn')
                            $get('upProgress').style.display = 'block';
                    }
                    function EndRequest(sender, args) {
                        if (postBackElement.id == 'searchBtn')
                            $get('upProgress').style.display = 'none';
                    }


                    $('#printNote').click(function () {

                        var divContents = $("#dialogMainContainer1").html();
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write('<html><head><title>DIV Contents</title>');
                        printWindow.document.write('</head><body >');
                        printWindow.document.write(divContents);
                        printWindow.document.write('</body></html>');
                        printWindow.document.close();
                        printWindow.print();

                    });
                    //function getIEVersion() {
                    //    var agent = navigator.userAgent;
                    //    var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
                    //    var matches = agent.match(reg);
                    //    if (matches != null) {
                    //        return { major: matches[1], minor: matches[2] };
                    //    }
                    //    return { major: "-1", minor: "-1" };
                    //}

                    //var ie_version = getIEVersion();
                    //var is_ie10 = ie_version.major == 10;

                    //var win;
                    //var isAtLeastIE11 = !!(navigator.userAgent.match(/Trident/) && !navigator.userAgent.match(/MSIE/));

                    //if (ie_version.major ==8 ||ie_version.major ==9 || ie_version.major ==10 || isAtLeastIE11==true) {
                    //    $('#maximize').click(function (e) {
                    //        localStorage['visited'] = $("#objViewer").attr("data");
                    //		alert('To enter into fullscreen mode, press F11. To escape full screen mode, press F11.')
                    //        //win = window.open(window.location.href + "&fs=1", '', 'fullscreen=yes, scrollbars=auto');
                    //    });
                    //}
                    $("#mYesBtn").click(function () {
                        mYesBtn = $(this).text();
                        if (mYesBtn == 'Yes') {
                            $("#maximizePopup").removeClass("bookMarkPopup hide show").addClass("bookMarkPopup hide")

                        }
                    });

                    $("#mNoBtn").click(function () {

                        window.close();

                    });
                    //(function () {
                    //    var
                    //        fullScreenApi = {
                    //            supportsFullScreen: false,
                    //            isFullScreen: function () { return false; },
                    //            requestFullScreen: function () { },
                    //            cancelFullScreen: function () { },
                    //            fullScreenEventName: '',
                    //            prefix: ''
                    //        },
                    //        browserPrefixes = 'webkit moz o ms khtml'.split(' ');

                    //    // check for native support
                    //    if (typeof document.cancelFullScreen != 'undefined') {
                    //        fullScreenApi.supportsFullScreen = true;
                    //    } else {
                    //        // check for fullscreen support by vendor prefix
                    //        for (var i = 0, il = browserPrefixes.length; i < il; i++) {
                    //            fullScreenApi.prefix = browserPrefixes[i];

                    //            if (typeof document[fullScreenApi.prefix + 'CancelFullScreen'] != 'undefined') {
                    //                fullScreenApi.supportsFullScreen = true;

                    //                break;
                    //            }
                    //        }
                    //    }

                    //    // update methods to do something useful
                    //    if (fullScreenApi.supportsFullScreen) {
                    //        fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';

                    //        fullScreenApi.isFullScreen = function () {
                    //            switch (this.prefix) {
                    //                case '':
                    //                    return document.fullScreen;
                    //                case 'webkit':
                    //                    return document.webkitIsFullScreen;
                    //                default:
                    //                    return document[this.prefix + 'FullScreen'];
                    //            }
                    //        }
                    //        fullScreenApi.requestFullScreen = function (el) {
                    //            return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
                    //        }
                    //        fullScreenApi.cancelFullScreen = function (el) {
                    //            return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
                    //        }
                    //    }

                    //    // jQuery plugin
                    //    if (typeof jQuery != 'undefined') {
                    //        jQuery.fn.requestFullScreen = function () {

                    //            return this.each(function () {
                    //                var el = jQuery(this);
                    //                if (fullScreenApi.supportsFullScreen) {
                    //                    fullScreenApi.requestFullScreen(el);
                    //                }
                    //            });
                    //        };
                    //    }

                    //    // export api
                    //    window.fullScreenApi = fullScreenApi;
                    //})();


                    // do something interesting with fullscreen support
                    //var fsButton = document.getElementById('maximize'),
                    //fsElement = document.getElementById('specialstuff'),
                    //fsStatus = document.getElementById('fsstatus');


                    //if (window.fullScreenApi.supportsFullScreen) {
                    //    fsStatus.innerHTML = 'YES: Your browser supports FullScreen';
                    //    fsStatus.className = 'fullScreenSupported';

                    //    // handle button click
                    //    fsButton.addEventListener('click', function () {
                    //        window.fullScreenApi.requestFullScreen(fsElement);
                    //    }, true);

                    //    fsElement.addEventListener(fullScreenApi.fullScreenEventName, function () {
                    //        if (fullScreenApi.isFullScreen()) {
                    //            fsStatus.innerHTML = 'Whoa, you went fullscreen';
                    //        } else {
                    //            fsStatus.innerHTML = 'Back to normal';
                    //        }
                    //    }, true);

                    //} else {
                    //    fsStatus.innerHTML = 'SORRY: Your browser does not support FullScreen';
                    //}

                    $('#courseExercise').hover(function () {

                        if ($(this).attr('href') == '#') {
                            $(this).addClass('disableCourseIcon')
                            //alert('No resouce exist for course!');
                        }
                    });


                    $("#search").click(function (e) {
                        ShowSearchGoDialog();
                        event.preventDefault();
                    });
                    $("#close_SearchGo").click(function (e) {
                        HideSearchGoDialog();
                        e.preventDefault();
                    });
                    //$("#btnViewFav2").click(function (e) {
                    //    ShowFavoriteDialog();
                    //    event.preventDefault();
                    //});
                    $(".close_favorite").click(function (e) {
                        HideFavoriteDialog();
                        e.preventDefault();
                    });


                    $("#help").click(function (e) {
                        ShowHelpDialog();
                        e.preventDefault();
                    });

                    //$("#globalNotesBtn2").click(function (e) {
                    //    ShowGenericDialog();
                    //    event.preventDefault();
                    //});
                    $(".close_help").click(function (e) {
                        HideHelpDialog();
                        e.preventDefault();
                    });
                    $("#btnDialogClose").click(function (e) {
                        HideGenericDialog();
                        e.preventDefault();
                    });
                    function ShowGenericDialog(modal) {
                        $("#popupOverlay").show();
                        $("#genericDialog").fadeIn(300);

                        if (modal) {
                            $("#popupOverlay").unbind("click");
                        }
                        else {
                            $("#popupOverlay").click(function (e) {
                                HideDialog();
                            });
                        }
                    }
                    function ShowFavoriteDialog(modal) {
                        $("#popupOverlay").show();
                        $(".favoriteDialog").fadeIn(300);

                        if (modal) {
                            $("#popupOverlay").unbind("click");
                        }
                        else {
                            $("#popupOverlay").click(function (e) {
                                HideDialog();
                            });
                        }
                    }
                    function HideFavoriteDialog() {
                        $("#popupOverlay").hide();
                        $(".favoriteDialog").fadeOut(300);
                    }
                    function HideGenericDialog() {
                        $("#popupOverlay").hide();
                        $("#genericDialog").fadeOut(300);
                    }
                    function ShowHelpDialog(modal) {
                        $("#popupOverlay").show();
                        $(".helpDialog").fadeIn(300);

                        if (modal) {
                            $("#popupOverlay").unbind("click");
                        }
                        else {
                            $("#popupOverlay").click(function (e) {
                                HideDialog();
                            });
                        }
                    }
                    function HideHelpDialog() {
                        $("#popupOverlay").hide();
                        $(".helpDialog").fadeOut(300);
                    }
                    function ShowSearchGoDialog(modal) {
                        $("#popupOverlay").show();
                        $(".SearchGoDialog").fadeIn(300);

                        if (modal) {
                            $("#popupOverlay").unbind("click");
                        }
                        else {
                            $("#popupOverlay").click(function (e) {
                                HideDialog();
                            });
                        }
                    }
                    function HideSearchGoDialog() {
                        $("#popupOverlay").hide();
                        $(".SearchGoDialog").fadeOut(300);
                    }
                    var a = document.getElementById("objViewer");
                    objectData = $("#hdnObjectUrl").val();
                    var bookMarkUrl = $("#hdnBookMarkUrl").val();
                    var abc = window.location.href;
                    userid = GetParameterValues('uid');
                    courseid = GetParameterValues('cid');
                    juliandate = GetParameterValues('jid');



                    function GetParameterValues(param) {
                        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

                        for (var i = 0; i < url.length; i++) {
                            var urlparam = url[i].split('=');
                            if (urlparam[0] == param) {
                                return urlparam[1];
                            }
                        }
                    }
                    var bmYesBtn;
                    var fscreenUrl = window.location.href;
                    if (fscreenUrl.indexOf("fs") > 0) {
                        var str = bookMarkUrl.split("#");
                        objectData = str[0] + "&uid=" + userid + "&jid=" + juliandate + "&cid=" + courseid + "#" + str[1];
                        //objectData=localStorage['visited'];
                        $("#objViewer").attr("data", objectData);
                        $("#bookMarkPopup").removeClass("bookMarkPopup hide show").addClass("bookMarkPopup hide")
                        $("#popupOverlay").css('display', 'none')
                        $("#maximizePopup").removeClass("bookMarkPopup hide").addClass("bookMarkPopup hide show")
                        $("#objViewer").attr("data", objectData);

                        document.onkeydown = function (evt) {
                            evt = evt || window.event;
                            if (evt.keyCode == 27) {
                                // alert('Escape')
                                window.close();
                            }
                        };
                    }


                    if (bookMarkUrl != "") {
                        $("#bmYesBtn").click(function () {
                            var isChrome = window.chrome;
                            if (isChrome) {
                                $("#objViewer").attr("height", "700px");
                            }

                            bmYesBtn = $(this).text();

                            if (bmYesBtn == 'Yes') {
                                if (bookMarkUrl != "") {
                                    var str = bookMarkUrl.split("#");
                                    objectData = str[0] + "#" + str[1];
                                    $("#bookMarkPopup").removeClass("bookMarkPopup hide show").addClass("bookMarkPopup hide")
                                    $("#popupOverlay").css('display', 'none')
                                }
                            }

                            if (bookMarkUrl != "") {
                                $("#objViewer").attr("data", objectData);
                            }
                            else {
                                if (objectData != "") {
                                    $("#objViewer").attr("data", objectData);
                                }
                                else {
                                    $("#objViewer").attr("data", 'http://htmlviewer/404Error/index.html');
                                }
                            }
                        });
                        $("#bmNoBtn").click(function () {
                            var isChrome = window.chrome;
                            if (isChrome) {
                                $("#objViewer").attr("height", "700px");
                            }
                            $("#bookMarkPopup").removeClass("bookMarkPopup hide show").addClass("bookMarkPopup hide")
                            $("#popupOverlay").css('display', 'none')
                            if (bookMarkUrl != "") {
                                $("#objViewer").attr("data", objectData + "?uid=" + userid + "&jid=" + juliandate + "&cid=" + courseid);
                            }
                            else {
                                if (objectData != "") {
                                    $("#objViewer").attr("data", objectData + "?uid=" + userid + "&jid=" + juliandate + "&cid=" + courseid);

                                }
                                else {
                                    $("#objViewer").attr("data", 'http://htmlviewer/404Error/index.html');
                                }
                            }

                        });
                    }
                    else {
                        $("#bookMarkPopup").removeClass("bookMarkPopup hide show").addClass("bookMarkPopup hide")
                        $("#popupOverlay").css('display', 'none')
                        if (objectData != "") {
                            $("#objViewer").attr("data", objectData + "?uid=" + userid + "&jid=" + juliandate + "&cid=" + courseid);

                        }
                        else {
                            $("#objViewer").attr("data", 'http://htmlviewer/404Error/index.html');
                        }
                    }

                    //$('#bookMarkPopup').removeClass('bookMarkPopup hide show').addClass('bookMarkPopup hide');
                    //$('#bookMarkPopup').removeClass('bookMarkPopup hide').addClass('bookMarkPopup hide show');
                    //alert($('.bookMarkPopup').length)

                };




            </script>

           <%-- <script>

                $(document).ready(function () {

                    $('#startAnnotation').click(function () {
                        $.i18n.load(i18n_dict);
                        // Customise the default plugin options with the third argument.
                        var annotator = $('body').annotator().annotator().data('annotator');
                        var propietary = 'demoUser';
                        annotator.addPlugin('Permissions', {
                            user: propietary,
                            permissions: {
                                'read': [propietary],
                                'update': [propietary],
                                'delete': [propietary],
                                'admin': [propietary]
                            },
                            showViewPermissionsCheckbox: true,
                            showEditPermissionsCheckbox: false
                        });
                        $('body').annotator().annotator('addPlugin', 'RichEditor');

                        $('body').annotator().annotator('addPlugin', 'Categories', {
                            errata: 'annotator-hl-errata',
                            destacat: 'annotator-hl-destacat',
                            subratllat: 'annotator-hl-subratllat'
                        }
                           );
                        $('body').annotator().annotator('addPlugin', 'AnnotatorViewer');
                        $('body').annotator().annotator("addPlugin", "Touch");
                        //Annotation scroll
                        $('#anotacions-uoc-panel').slimscroll({ height: '100%' });
                    });
                });
                jQuery(function ($) {

                });
            </script>--%>


        </div>
    </form>
</body>
</html>
