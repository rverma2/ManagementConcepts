﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Security.Cryptography;
using System.Text;

public partial class ViewerJS_index : System.Web.UI.Page
{
    string courseExerciseName = ConfigurationManager.AppSettings["courseExerciseName"] != string.Empty ? ConfigurationManager.AppSettings["courseExerciseName"] : string.Empty;
    string bookpdfName = ConfigurationManager.AppSettings["bookpdfName"]!=string.Empty?ConfigurationManager.AppSettings["bookpdfName"]:string.Empty;
    string bookiconName = ConfigurationManager.AppSettings["bookiconName"]!=string.Empty?ConfigurationManager.AppSettings["bookiconName"]:string.Empty;
    string backupUrl = string.Empty;
    string manifestUrl = string.Empty;
    List<string> lstUrl = new List<string>();
    DataTable dtUrlTitle = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["sessionout"] = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty; 

        lblCourseDescription.Text = Request.QueryString["cname"] != null ? Request.QueryString["cname"].ToString() : string.Empty;
        if (!Page.IsPostBack)
        {
            
            BindRepeaterData();
            BindFavRepeaterData();
        }
        lblCourseName.Text = Request.QueryString["cname"] != null ? Request.QueryString["cname"].ToString() : string.Empty;
        //lblBookName.Text = Request.QueryString["cname"] != null ? Request.QueryString["cname"].ToString() : string.Empty;
        string contentUrlPath = string.Empty;
        string userName = string.Empty;
        string uid = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty;
        string cid = !string.IsNullOrEmpty(Request.QueryString["cid"]) ? Request.QueryString["cid"].ToString() : string.Empty; 
        string jid = !string.IsNullOrEmpty(Request.QueryString["jid"]) ? Request.QueryString["jid"].ToString() : string.Empty; 
        hdnBookMarkUrl.Value = GetBookmark(uid, cid);
        if(!string.IsNullOrEmpty(Request.QueryString["uid"])&!string.IsNullOrEmpty(Request.QueryString["cid"])&&!string.IsNullOrEmpty(Request.QueryString["jid"]))
        {           

            string connString = ConfigurationManager.AppSettings["viewerConnection"].ToString();
            SqlConnection sqlConn = new SqlConnection(connString);
            sqlConn.Open();
            SqlCommand sqlcmd = new SqlCommand("GetContentUrl", sqlConn);
            sqlcmd.CommandType = CommandType.StoredProcedure;

            SqlParameter prmUID = new SqlParameter();
            prmUID.ParameterName = "@uid";
            prmUID.SqlDbType = System.Data.SqlDbType.NVarChar;
            prmUID.Size = 200;
            prmUID.Value = uid;            

            SqlParameter prmCID = new SqlParameter();
            prmCID.ParameterName = "@cid";
            prmCID.SqlDbType = System.Data.SqlDbType.NVarChar;
            prmCID.Size = 200;            
            prmCID.Value = cid;

            SqlParameter prmJID = new SqlParameter();
            prmJID.ParameterName = "@jid";
            prmJID.SqlDbType = System.Data.SqlDbType.NVarChar;
            prmJID.Size = 100;
            prmJID.Value = jid;

            SqlParameter prmURLPATH = new SqlParameter();
            prmURLPATH.ParameterName = "@urlpath";
            prmURLPATH.SqlDbType = System.Data.SqlDbType.NVarChar;
            prmURLPATH.Size = 500;

            SqlParameter prmUserName = new SqlParameter();
            prmUserName.ParameterName = "@username";
            prmUserName.SqlDbType = System.Data.SqlDbType.NVarChar;
            prmUserName.Size = 100;

            prmUID.Direction = System.Data.ParameterDirection.Input;
            prmCID.Direction = System.Data.ParameterDirection.Input;
            prmJID.Direction = System.Data.ParameterDirection.Input;
            prmURLPATH.Direction = System.Data.ParameterDirection.Output;
            prmUserName.Direction = System.Data.ParameterDirection.Output;

            sqlcmd.Parameters.Add(prmUID);
            sqlcmd.Parameters.Add(prmCID);
            sqlcmd.Parameters.Add(prmJID);
            sqlcmd.Parameters.Add(prmURLPATH);
            sqlcmd.Parameters.Add(prmUserName);


            sqlcmd.ExecuteNonQuery();
            contentUrlPath = sqlcmd.Parameters["@urlpath"].Value.ToString();
            userName = sqlcmd.Parameters["@username"].Value.ToString();
            hdnObjectUrl.Value = contentUrlPath == string.Empty ? string.Empty : contentUrlPath;
            lblUserName.Text = userName == string.Empty ? "Welcome, Guest" : "Welcome,  "+userName;

            string originalUrl = hdnObjectUrl.Value;
            originalUrl = originalUrl.Substring(0, originalUrl.LastIndexOf("/") + 1);
            backupUrl = originalUrl +bookpdfName+".pdf";
            Session["pdfUrl"] = backupUrl!=string.Empty?backupUrl:string.Empty;

            try
            {
                WebRequest webreq = WebRequest.Create(originalUrl + courseExerciseName);
                WebResponse webres = webreq.GetResponse();
                courseExercise.HRef = originalUrl + courseExerciseName;
                courseExercise.Title = "Download Resource";
            }
            catch (Exception)
            {

                courseExercise.Title = "This course does not contain any supplemental material.";
            }
            sqlConn.Close();
        }


    }

    public string GetBookmark(string userid, string classid)
    {
        SqlDataAdapter da = new SqlDataAdapter();
        String daresult = null;
        string constr = ConfigurationManager.AppSettings["viewerConnection"].ToString();
       // string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("GetBookmarkdetails", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", Convert.ToString(userid));
                cmd.Parameters.AddWithValue("@classid", Convert.ToString(classid));
                using (da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();
                    da.Fill(dt);
                    ds.Tables.Add(dt);
                    if (dt.Rows.Count > 0)
                        daresult = dt.Rows[0][0].ToString();
                    return daresult;
                }
            }
            con.Close();
        }

    }
    protected void BindRepeaterData()
    {


        SqlDataAdapter da = new SqlDataAdapter();
        string constr = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("MasterGlobalNotes", con))
            {
                string uid = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty;
                string cid = !string.IsNullOrEmpty(Request.QueryString["cid"]) ? Request.QueryString["cid"].ToString() : string.Empty;
                string jid = !string.IsNullOrEmpty(Request.QueryString["jid"]) ? Request.QueryString["jid"].ToString() : string.Empty;
                //cid = "2080-COTS-1515";
                //uid = "3dd768ac-5c24-41af-9f04-8e000b8d3e4d";
                //jid = "2014-12-16";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", Convert.ToString(uid));
                cmd.Parameters.AddWithValue("@classid", Convert.ToString(cid));
                cmd.Parameters.AddWithValue("@juliandate", Convert.ToString(jid));


                using (da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();


                    da.Fill(dt);
                    ds.Tables.Add(dt);
                    if (dt.Rows.Count > 0)
                    {
                        RepDetails.DataSource = dt;
                        RepDetails.DataBind();
                       
                        con.Close();
                    }
                    else
                    {
                        RepDetails.DataSource = null;
                        RepDetails.DataBind();

                        con.Close();
                    }
                }
            }
        }

    }


    //protected void globalNotesBtn2_Click(object sender, EventArgs e)
    //{
    //    BindRepeaterData();
    //}
    protected void btnAddRemoveFav_Click(object sender, EventArgs e)
    {
        string connString = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        SqlConnection con = new SqlConnection(connString);
        string uid = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty;
        string cid = !string.IsNullOrEmpty(Request.QueryString["cid"]) ? Request.QueryString["cid"].ToString() : string.Empty; ;
        string jid = !string.IsNullOrEmpty(Request.QueryString["jid"]) ? Request.QueryString["jid"].ToString() : string.Empty; ;
        try
        {
            if (uid != null || cid != null || cid != null)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "SaveAddtoFavourites";
                cmd.Parameters.AddWithValue("@userid", Convert.ToString(uid));
                
                cmd.Parameters.AddWithValue("@classid", Convert.ToString(cid));
                cmd.Parameters.AddWithValue("@juliandate", jid);
                
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Favorite added!')", true);
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void BindFavRepeaterData()
    {

        string connString = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        SqlConnection con = new SqlConnection(connString);
        string uid = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty;
        string cid = !string.IsNullOrEmpty(Request.QueryString["cid"]) ? Request.QueryString["cid"].ToString() : string.Empty; ;
        string jid = !string.IsNullOrEmpty(Request.QueryString["jid"]) ? Request.QueryString["jid"].ToString() : string.Empty; ;
        try
        {
            if (uid != null || cid != null || cid != null)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "GetCourseFavouritePages";
                cmd.Parameters.AddWithValue("@userid", Convert.ToString(uid));
                
                cmd.Parameters.AddWithValue("@classid", Convert.ToString(cid));
                cmd.Parameters.AddWithValue("@juliandate", jid);
                
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();



                da.Fill(dt);
                Repeaterfav.DataSource = dt;
                Repeaterfav.DataBind();               
                con.Close();

            }
        }
        catch (Exception ex)
        {
        }
    }
    public List<string> SearchText(string searchText, string backupUrl)
    {
        try
        {

            string stcheck = string.Empty;
            string url = backupUrl;
            WebClient wc = new WebClient();
            string st = wc.DownloadString(url);

            //Read the file              

            //string st = stream.ReadToEnd().Trim();
            // st = st.Substring((st.IndexOf("{") + 3), (st.IndexOf("}")) - 7);
            st = st.Substring((st.IndexOf("define({") + 8), (st.Length - 11));
            //hdnString.Value = st.ToString();.html":
            st = Regex.Replace(st, "<[^>]*:>", string.Empty);


            int x = 0;

            while (x == 0)
            {
                int icheckbegin1 = st.IndexOf("<script>");
                int icheckend1 = st.IndexOf(@"<\/script>") + 10;

                if (icheckbegin1 <= 0 || icheckend1 <= 0)

                    x = 1;

                else
                {


                    string S = st.Substring(icheckbegin1, icheckend1 - icheckbegin1);
                    st = st.Replace(S, "");
                }

            }


            string[] words = st.Split('\n');
            searchText = WebUtility.HtmlEncode(searchText);
            for (int i = 0; i < words.Length; i++)
            {  //loop to remove the title tag with text in between .

                string strempty = string.Empty;

                int intDocTitles = words[i].IndexOf("PageDocumentTitle");


                if (intDocTitles > 0)
                {

                    try
                    {
                        strempty = words[i].Substring(intDocTitles - 13, words[i].IndexOf(@"<\/div>") - (intDocTitles - 20));
                        words[i] = words[i].Replace(strempty, "");
                    }

                    catch (Exception ex)
                    {

                    }
                }


                if (words[i].IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > 0)
                {
                    var htmlurl = words[i].Split(':');
                    string urls = htmlurl[0];
                    for (int j = 1; j < htmlurl.Length; j++)
                    {
                        // Extra Code
                        stcheck = htmlurl[j].ToString();

                        string streplace = string.Empty;



                        if (stcheck.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > 0)
                        {
                            string str = HttpContext.Current.Server.HtmlEncode(urls);
                            urls = str.Substring(str.IndexOf("&quot;") + 6, str.LastIndexOf("&quot;") - 10);
                            urls = urls.Substring(0, urls.IndexOf("."));
                            if (!lstUrl.Contains(urls) && urls != string.Empty)
                                lstUrl.Add(urls);
                            break;
                        }

                        //}

                    }
                }

            }

        }
        catch (Exception)
        {


        }
        return lstUrl;
    }

    //public List<string> SearchText(string searchText, string backupUrl)
    //{
    //    try
    //    {
    //        //var stream = File.OpenText(backupUrl);
    //       // StreamReader streamReader = new StreamReader(backupUrl);
    //        //string st = streamReader.ReadToEnd().Trim();

    //        string url = backupUrl;
    //        WebClient wc = new WebClient();
    //        string st = wc.DownloadString(url);

    //        //Read the file              
    //        //string searchText = "documentation";
    //        //string st = stream.ReadToEnd().Trim();
    //        st = st.Substring((st.IndexOf("{") + 1), (st.IndexOf("}")) - 7);
    //        //hdnString.Value = st.ToString();.html":
    //        st=Regex.Replace(st, "<[^>]*:>", string.Empty);
    //        string[] words = st.Split('\n');
    //        searchText=WebUtility.HtmlEncode(searchText);
    //        for (int i = 0; i < words.Length; i++)
    //        {
    //            //if (words[i].Contains(searchText))
    //            //Regex.IsMatch(words[i], @"\bsearchText\b", RegexOptions.IgnoreCase)
    //            if (words[i].IndexOf(searchText,StringComparison.InvariantCultureIgnoreCase)>0)
    //            {                    
    //                var htmlurl = words[i].Split(':');
    //                string urls = htmlurl[0];
    //                for (int j = 1; j < htmlurl.Length; j++)
    //                {
    //                    if (htmlurl[j].IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > 0)
    //                    {
    //                        string str = HttpContext.Current.Server.HtmlEncode(urls);
    //                        urls = str.Substring(str.IndexOf("&quot;") + 6, str.LastIndexOf("&quot;") - 10);
    //                        urls = urls.Substring(0, urls.IndexOf("."));
    //                        if (!lstUrl.Contains(urls) && urls != string.Empty)
    //                            lstUrl.Add(urls);
    //                        break;
    //                    }
    //                }
    //            }
    //            //string search = @"\bsearchText\b".Replace("searchText", searchText);
    //            //if (Regex.IsMatch(words[i], search, RegexOptions.IgnoreCase))
    //            //{
    //            //    var htmlurl = words[i].Split(':');
    //            //    string urls = htmlurl[0];
    //            //    string str = HttpContext.Current.Server.HtmlEncode(urls);
    //            //    urls = str.Substring(str.IndexOf("&quot;") + 6, str.LastIndexOf("&quot;") - 10);
    //            //    urls = urls.Substring(0, urls.IndexOf("."));
    //            //    if (!lstUrl.Contains(urls) && urls != string.Empty)
    //            //        lstUrl.Add(urls);
    //            //}
    //            //if (searchText.Contains('$') && words[i].IndexOf(searchText) > 0)
    //            //{
    //            //    var htmlurl = words[i].Split(':');
    //            //    string urls = htmlurl[0];
    //            //    string str = HttpContext.Current.Server.HtmlEncode(urls);
    //            //    urls = str.Substring(str.IndexOf("&quot;") + 6, str.LastIndexOf("&quot;") - 10);
    //            //    urls = urls.Substring(0, urls.IndexOf("."));
    //            //    if (!lstUrl.Contains(urls) && urls != string.Empty)
    //            //        lstUrl.Add(urls);
    //            //}
    //        }

    //        //string url = string.Empty;
    //        //XmlDocument doc = new XmlDocument();
    //        //doc.Load(backupUrl);
    //        //XmlNodeList product = doc.GetElementsByTagName("Topic");
            

    //        //foreach (XmlNode price in product)
    //        //{
    //        //    if (price.SelectSingleNode("Title").InnerText != null)
    //        //    {
    //        //        if (price.SelectSingleNode("Title").InnerText.IndexOf(text, StringComparison.OrdinalIgnoreCase) > 0)
    //        //        {
    //        //            url = price.ParentNode.Attributes["xy:guid"].Value;
    //        //        }
    //        //    }
                
    //        //    if (price.SelectSingleNode("ParaBlock") != null)
    //        //    {
    //        //        if (price.SelectSingleNode("ParaBlock").InnerText.Contains(text))
    //        //        {
    //        //            url = price.Attributes["xy:guid"].Value;
    //        //        }
    //        //    }
    //        //    if (!lstUrl.Contains(url) && url != string.Empty)
    //        //        lstUrl.Add(url);
    //        //}
            
    //    }
    //    catch (Exception)
    //    {


    //    }
    //    return lstUrl;
    //}
    public DataTable GetTitleUrl(List<string> foundUrlList,string manifestUrl)
    {
        try
        {
            dtUrlTitle.Columns.Add("pageno",typeof(int));
            dtUrlTitle.Columns.Add("url", typeof(string));
            dtUrlTitle.Columns.Add("title", typeof(string));
            Boolean bflag = false;
            string url = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(manifestUrl);
            XmlNodeList product = doc.GetElementsByTagName("item");
            foreach (string urlStr in foundUrlList)
            {
                bflag = false;
                foreach (XmlNode price in product)
                {
                    string identifierRef = "R_" + urlStr;
                    for (int i = 1; i < price.ChildNodes.Count; i++)
                    {
                        if (price.ChildNodes[i].Attributes["identifierref"] != null)
                        {
                            if (price.ChildNodes[i].Attributes["identifierref"].Value == identifierRef)
                            {
                                string pno =price.ChildNodes[i].Attributes["identifier"].Value;
                                //pno = pno.Substring(pno.IndexOf("item") + 5);
                                int pageNumber =Convert.ToInt16(GetPageNumber(pno));
                                dtUrlTitle.Rows.Add(pageNumber, hdnObjectUrl.Value + "#" + price.ChildNodes[i].Attributes["identifier"].Value, price.ChildNodes[i].ChildNodes[0].InnerXml);
                                bflag = true;
                                break;
                            }
                        }
                    }
                    if (bflag)
                        break;
                }
            }
        }
        catch (Exception)
        {


        }
        DataView dv = dtUrlTitle.DefaultView;
        dv.Sort = "pageno asc";
        DataTable sortedDT = dv.ToTable();
        return sortedDT;
    }

    public string GetPageNumber(string pno)
    {
        string prmItemNo = string.Empty;
        string connString = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        SqlConnection con = new SqlConnection(connString);
        string uid = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty;
        string cid = !string.IsNullOrEmpty(Request.QueryString["cid"]) ? Request.QueryString["cid"].ToString() : string.Empty;
        string jid = !string.IsNullOrEmpty(Request.QueryString["jid"]) ? Request.QueryString["jid"].ToString() : string.Empty; 
        string coursecode = cid;
        string userid = uid;
        coursecode = coursecode.Substring(0, coursecode.IndexOf("-"));
        string juliandate = jid;
        string pagenumber = pno;
        try
        {
            if (coursecode != string.Empty || juliandate != string.Empty || pagenumber != string.Empty)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "SearchPageNumber";
                cmd.Parameters.AddWithValue("@classid", Convert.ToString(coursecode));
                cmd.Parameters.AddWithValue("@juliandate", Convert.ToString(juliandate));                
                cmd.Parameters.AddWithValue("@userid", Convert.ToString(userid));
                cmd.Parameters.AddWithValue("@pageTag", pagenumber);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();



                da.Fill(dt);
                //hdnPageNumber.Value = dt.Rows[0].ItemArray[0].ToString();
                prmItemNo = dt.Rows[0].ItemArray[0].ToString();
                con.Close();
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), Guid.NewGuid().ToString(), "GoSpecificPage();", true);
            }
        }

        catch (Exception ex)
        {
            return "";
        }
        return prmItemNo;

    }
    protected void btnViewFav_Click(object sender, EventArgs e)
    {
        BindFavRepeaterData();
		ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "showFavorites();", true);
        //ClientScriptManager cs = Page.ClientScript;

        //cs.RegisterStartupScript(this.GetType(), "ShowFavourite", "ShowFavoriteDialog();", true);
       
    }

    //protected void globalNotesBtn_Click(object sender, EventArgs e)
    //{
    //    BindRepeaterData();
    //}
    protected void searchBtn_Click(object sender, EventArgs e)
    {
        string searchText = searchBox.Text;
        if (searchText != string.Empty && searchText!="Search")
        {
        string originalUrl = hdnObjectUrl.Value;
        originalUrl = originalUrl.Substring(0, originalUrl.LastIndexOf("/") + 1);
        backupUrl = originalUrl + "resources/fileCache.js";
        manifestUrl = originalUrl + "manifest.xml";

        List<string> foundUrlList = SearchText(searchText, backupUrl);
        DataTable dtTitleURL = GetTitleUrl(foundUrlList,manifestUrl);

        rptSearch.DataSource = dtTitleURL;
        rptSearch.DataBind();
        }
    }
    


    [System.Web.Services.WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetCurrentTime()
    {
        return "Hello ";
    }

    [System.Web.Services.WebMethod]
    public static string Bookmark(string userid, string classid, string CurrentPage, string juliandate)
    {
        SqlCommand comm;
        string constr = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "Bookmark";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@ClassId", Convert.ToString(classid));
            // comm.Parameters.AddWithValue("@currentPage", Convert.ToString(CurrentPage));
            comm.Parameters.AddWithValue("@PageName", Convert.ToString(CurrentPage));
            comm.Parameters.AddWithValue("@JulienDate", Convert.ToDateTime(juliandate));
            try
            {
                comm.ExecuteNonQuery();
                return "Record Saved";
            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }
    }

    [System.Web.Services.WebMethod]
    public static string GotoSpecificpage(string cid,string jid,string pageNo,string uid)
    {

        string prmItemNo = string.Empty;
        string connString = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        SqlConnection con = new SqlConnection(connString);
        string coursecode = cid;
        string userid = uid;
        coursecode = coursecode.Substring(0, coursecode.IndexOf("-"));
        string juliandate = jid;
        string pagenumber = pageNo;
        try
        {
            if (coursecode != string.Empty || juliandate != string.Empty || pagenumber != string.Empty)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "GetGoToPageNumber";
                cmd.Parameters.AddWithValue("@coursecode", Convert.ToString(coursecode));                
                cmd.Parameters.AddWithValue("@juliandate", Convert.ToString(juliandate));
                cmd.Parameters.AddWithValue("@pagenumber", Convert.ToInt16(pagenumber));
                cmd.Parameters.AddWithValue("@userid", Convert.ToString(userid));

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();



                da.Fill(dt);
                //hdnPageNumber.Value = dt.Rows[0].ItemArray[0].ToString();
                prmItemNo = dt.Rows[0].ItemArray[0].ToString();
                con.Close();
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), Guid.NewGuid().ToString(), "GoSpecificPage();", true);
            }
        }

        catch (Exception ex)
        {
            return "";
        }
        return prmItemNo;
    }
   
   
    protected void rptSearch_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (rptSearch.Items.Count < 1)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblFooter = (Label)e.Item.FindControl("lblEmptyData");
                lblFooter.Visible = true;
            }
        }
    }
    protected void globalNotesBtn_Click(object sender, EventArgs e)
    {
        BindRepeaterData();
		ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "showGlobalNotes();", true);
        //ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "GlobalButtonClick();", true);
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "GlobalButtonClick()", true);
    }

    protected string Encrypt(string plainText)
    {
        string PasswordHash = "P##Sw@rd";
        string SaltKey = "S#LT%KEY";
        string VIKey = "#1B2c3D4e5F6g7H8";

        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();
                cryptoStream.Close();
            }
            memoryStream.Close();
        }
        return Convert.ToBase64String(cipherTextBytes);
    }
    protected void btnChngPwd1_Click(object sender, EventArgs e)
    {
        string uid = !string.IsNullOrEmpty(Request.QueryString["uid"]) ? Request.QueryString["uid"].ToString() : string.Empty;
        string conn = ConfigurationManager.AppSettings["viewerConnection"].ToString();
        SqlConnection con = new SqlConnection(conn);

        con.Open();
        string Encrypttext = Encrypt(oldpwd.Value);

        string str1 = "select * from aspnetusers where passwordhash ='" + Encrypttext + "' and id ='" + uid + "'";

        SqlCommand cmd = new SqlCommand(str1, con);

        SqlDataReader dr = cmd.ExecuteReader();

        if (dr.Read())
        {

            SqlConnection con1 = new SqlConnection(conn);

            con1.Open();
            if (string.IsNullOrEmpty(newpwd.Value))
            {

                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", "alert('New password can't be blank..')", true);

                //ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('New Password can't be blank..');", true);
                return;
            }
            if (string.IsNullOrEmpty(confirmPwd.Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Confirm password can't be blank..')", true);
                return;
            }
            if (Convert.ToString(newpwd.Value) == Convert.ToString(confirmPwd.Value))
            {
                SqlCommand cmd1 = new SqlCommand();
                cmd1.Connection = con1;
                cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                cmd1.CommandText = "aspnetusers_insert";
                cmd1.Parameters.AddWithValue("@oldpassword", Encrypt(oldpwd.Value));
                cmd1.Parameters.AddWithValue("@newpassword", Encrypt(newpwd.Value));
                cmd1.Parameters.AddWithValue("@updateby", uid);
                // string str = "update aspnetusers set passwordhash='" + Encrypt(newPwdTxt.Value) + "' ,updateddate='" + DateTime.Now + "',updatedby='" + Session["UserId"] + "' where passwordhash ='" + Encrypt(oldPwdTxt.Value) + "'";

                // SqlCommand cmd1 = new SqlCommand(str, con1);

                cmd1.ExecuteNonQuery();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Password has been updated successfully..')", true);
                //lblMessage.Text = "Password has been updated successfully";
            }
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('New password and confirm password are not same.')", true);
            //    return;
            //}
            // Label1.Text = "Your Password has been changed successfully ";

            con1.Close();

            con.Close();

        }

        else
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Old Password is incorrect')", true);
            return;


        }

    }
    
}