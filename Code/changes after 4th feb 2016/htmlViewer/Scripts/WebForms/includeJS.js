﻿/// <reference path="../script1/jquery.xml2json.pack.js" />
/// <reference path="../../SearchHighlight/hilitor.js" />
/// <reference path="../jquery-1.10.2.min.js" />
//var head = document.getElementsByTagName('head')[0];
$(document).keydown(function (eventObject) {
  
    
     if (eventObject.keyCode == 9) {  //tab pressed
        eventObject.preventDefault(); // stops its action
    }

});


$(window).load(function () {

    setTimeout(function () {
        var htmldata = $('#PageBody').html();
        if (htmldata.indexOf('TabsComponent Horizontal') > -1) {

            $('.annotator').val('');
            $('#btnAnnotator').addClass('activeAnnotator');
            $('#btnAnnotator').attr('disabled', 'disabled');
            $('#btnhighlighter').attr('disabled', 'disabled');
            $('#remove-highlights-btn').attr('disabled', 'disabled');
            //Code to disable the selection of text
            $('#PageBody').attr('unselectable', 'on')
            $('#PageBody').css('user-select', 'none')
            $('#PageBody').css('-moz-user-select', 'none')
            $('#PageBody').css('-khtml-user-select', 'none')
            $('#PageBody').css('-webkit-user-select', 'none')
            $('#PageBody').on('selectstart', false)
            $('#PageBody').on('contextmenu', false)
            $('#PageBody').on('keydown', false)
            $('#PageBody').on('mousedown', false);
        }
        else {

            //alert("Inside Else");
            $('#btnAnnotator').removeAttr('disabled');
            $('#btnhighlighter').removeAttr('disabled');
            $('#remove-highlights-btn').removeAttr('disabled');
            $('.annotator').val('AnnotateME');
            $('#btnAnnotator').removeClass('activeAnnotator');
            //Code to enable the selection of text
            $('#PageBody').attr('unselectable', '')
            $('#PageBody').css('user-select', '')
            $('#PageBody').css('-moz-user-select', '')
            $('#PageBody').css('-khtml-user-select', '')
            $('#PageBody').css('-webkit-user-select', '')
            $('#PageBody').off('selectstart', false)
            $('#PageBody').off('contextmenu', false)
            $('#PageBody').off('keydown', false)
            $('#PageBody').off('mousedown', false);
        }
    }, 1500);


    $(document).delegate('.Resource', 'click', function (event) {
        var pageHeight = $('.PopupWrapper').width();
        if (pageHeight >= 480) {
            $('.PopupWrapper').addClass('adjustPopupWrapper')
        }
    });
    $('#sideTabContainer').on('click', function () {
        $('.annotator-adder').css('display', 'none');
    });
    $('#footNoteWrapper').on('click', function () {
        $('.annotator-adder').css('display', 'none');
    });
    $('.InnerHeader').on('click', function () {
        $('.annotator-adder').css('display', 'none');
    });

    $('.annotator-outer.annotator-viewer ').addClass('annotator_hide_custom');
    (function ($) {
        $.fn.tagRemover = function () {
            return this.each(function () {
                var $this = $(this);
                var text = $this.text();
                $this.replaceWith(text);
            });
        }
    })(jQuery);
    //Comment Popup Edit and Delete start here
    //<div class='annotator-widget'></div> 
    var action = 0;
    var targetId;
    var selectedSpan = '';
    var selectedSpanText = '';
    var highlightedID = '';
    var deleteTargetId = '';
    $("#sandbox").append("<div id='popup' style='border:1px solid #808080; display:none;z-index: 999999999; position:absolute; width:217px; min-height:100px; height:auto; background: #f8f8f8 none repeat scroll 0 0; box-shadow: 0 0 15px 0 rgba(128, 128, 128, 0.75); border-radius:10px; padding:10px;'><div id='close' style='float:right; width:66px; height:30px;'> <a id='editPopup' href='#' title='Edit comment'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> <a id='deletePopup' href='#' title='Delete comment'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> <a id='closePopup' href='#' title='Close'>&nbsp;&nbsp;&nbsp;&nbsp;</a></div> <TextArea ID='editContainer' style='width:100%; height:66px;overflow-y:auto;cols='40' rows='100' /> <div id='buttonSaveCancel' style='clear:both;width:145px;float:right;'><input href='#' id='btnCancel' value='Close' /><input href='#' id='btnSave' value='Save' /></div>");

    $('#editContainer').attr('readonly', true);

    $('span').each(function () {
        var id = $(this).attr('id');
        if (id > 0) {
            getData(id);
            
        }
    });
    $('#btnCancel').hover(

             function () {
                 $('#btnSave').css({ "background-color": "#90182a" });
             },

             function () {
                 $('#btnSave').css({ "background-color": "#e8a713" });
             }
          );
    $('#buttonSaveCancel').hide();
    //$("#sandbox").on('mouseout', 'span.annotator-hl', function (event) {
    //    $('#popup').hide();
    //    $('#editContainer').prop('disabled', true);
    //    $('#buttonSaveCancel').hideo
    //});
    //$(document).on('mouseout', '#popup', function (event) {
    //    $('#popup').hide();
    //    $('#editContainer').prop('disabled', true);
    //    $('#buttonSaveCancel').hide();
    //});
    $('#buttonSaveCancel').hide();
    $('#deletePopup').click(function (event) {
        event.preventDefault();
        var pageData = '';
        var datas = '';
        var d = '';

        $('span[id^=' + deleteTargetId + ']').tagRemover();
        deleteData(deleteTargetId, pageData);
        //var highlightedIndex=datas.indexOf('highlighted');
        //var annotatorIndex = datas.indexOf('annotator-hl'); 
        //&& (highlightedIndex > 0 || annotatorIndex > 0)
        //$('span').removeClass('annotator-hl annotator-hl-errata')
        datas = $('#PageBody').html();
        if (datas.indexOf('PageResourcesButtonContainer') > 0) {
            datas = $('.Topic').html();
        }
        datas = datas.replace(selectedSpan, selectedSpanText)

        oData = datas;
        if (oData.indexOf('Topic') == 12) {
            $('.Topic').empty();
            $('.Topic').append(datas);
            $('.Topic').bind();
        }
        else {
            $('#PageBody').empty();
            $("#PageBody").append(datas);
            $("#PageBody").bind();
        }

        d = $('.OneImagePage div:first').attr('style');
        if (datas != '' && datas != undefined) {
            //alert(datas)
            datas = datas.replace(/[\[\]]+/g, '');
            //alert(datas)
            var repd = '';
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 && d != undefined) {
                repd = d.replace(/"/g, "&quot;");
                datas = datas.replace(d, repd);
                //datas = datas.replace(/"/g, "&quot;");
                datas = datas.replace(/'/g, "&quot;&quot;");

            }
            else {
                datas = datas.replace(/'/g, '&quot;');
                //alert(datas+'ff')
            }

            var pattern = new RegExp('</?a ?[^>]*>', 'g');
            var clean = datas; //.replace(pattern, '');
            var a = clean.replace("Main Content", "");
            var datta = '';
            if (a.indexOf('Topic') > -1) {

                datta = a;
            }
            else {

                var addbefore = '<div class="Topic">';
                var addafter = '</div>';
                datta = '<div class="Topic">' + a + '</div>'

            }
            var Replacedata = '';
            Replacedata = datas.replace(/(<([^>]+)>)/ig, "");

            var PageName = '';
            PageName = window.location.href;
            //alert(data+PageName+userid+courseid)
            //if (datas != '' && datas != undefined) {

            $.ajax({
                type: "POST",
                url: "CS.aspx/UpdateDataAnnotation",
                data: "{PageName: '" + PageName + "', AnnotationData:'" + datta + "',userid: '" + userid + "',classid: '" + courseid + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {

                },
                failure: function (response) {
                    alert('F');
                }
            });
            //}
        }



        $('#popup').hide();
        //$('#editContainer').attr('disabled', true);
        $('#buttonSaveCancel').hide();
    });
    $('#closePopup').click(function (event) {
        event.preventDefault();
        //$('.annotator-outer').removeClass('annotator_hide_custom');
        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();
    });

    $('#editPopup').click(function (event) {
        event.preventDefault();
        $('#editContainer').attr('readonly', false);
        $('#buttonSaveCancel').show();
        $('.annotator-widget').addClass('annotatorWidget');
    });

    $('#btnCancel').click(function (event) {
        event.preventDefault();
        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();
    });

    $('#btnSave').click(function (event) {
        event.preventDefault();
        updateData(targetId, $('#editContainer').val());
        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();
    });
    // document.oncontextmenu = function () { return false; };
    function updateData(id, description) {
        //if (description == '') {
        //    alert('Can not be empty');
        //    getData(targetId);
        //    $('#popup').css('left', event.pageX);
        //    $('#popup').css('top', event.pageY + 10);
        //    $('#popup').css('display', 'inline');
        //    $('#popup').css('position', 'absolute');

        //}
        //else {
        $.ajax({
            type: "POST",
            url: "CS.aspx/updateAnnotatedComment",
            data: "{'Id': '" + id + "','description':'" + description + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

            },
            failure: function (response) {
                alert(response);
            }
        });
        //}

    }
    function deleteData(id, description) {

        $.ajax({
            type: "POST",
            url: "CS.aspx/deleteAnnotatedDataComment",
            data: "{'Id': '" + id + "','description':'" + description + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

            },
            failure: function (response) {
                alert(response);
            }
        });


    }

    $(document).mouseup(function (event) {
        if (event.which == 1) {
            //selectText();

        }

    });

    function getData(id) {
        $.ajaxSetup({ cache: false });
        var vall = id;
        $.ajax({

            type: "POST",
            url: "CS.aspx/GetAnnotation",
            data: "{id: '" + vall + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (response) {

                if (response.d == '') {
                    $('#' + id).removeClass('span');
                }
                $('#editContainer').val(response.d);
            },
            failure: function (response) {
                alert(response);
            }
        });
    }
    //Comment Popup Edit and Delete end here

    function getIEVersion() {
        var agent = navigator.userAgent;
        var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
        var matches = agent.match(reg);
        if (matches != null) {
            return { major: matches[1], minor: matches[2] };
        }
        return { major: "-1", minor: "-1" };
    }

    var ie_version = getIEVersion();
    var is_ie10 = ie_version.major == 10;
    var versionIE = ie_version.major

    if (versionIE == 9) {
        {
            $('.NavigationTreeItem').addClass('NavigationTreeItemIE9')
            $('.NavigationNode').addClass('NavigationNodeIE9')
        }
    }


    //$('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
    //$('#remove-highlights-btn').removeClass('activeRemoveHighlight');
    //$('#btnhighlighter').removeClass('activeHighlight');
    //$('#btnAnnotator').removeClass('activeAnnotator');


    $('.annotator').val('AnnotateME')
    var highlighter;
    var datas = '';
    var d = '';
    $.ajaxSetup({ cache: false });
    setTimeout(function () {
        //$('#footNoteContent1').html('');
        $.ajaxSetup({ cache: false });
        if ($('#PageBody').children().hasClass('OneImagePage')) {
            d = $('.OneImagePage div:first').attr('style');
        }
        var footerNote = '';
        var n = 1;
        $('.FootnoteContent').each(function () {
            footerNote += n + '.' + $(this).html() + "<br \>";
            n = n + 1;
        });

        if (footerNote != undefined && footerNote != '') {

            $('.footNoteContent').html(footerNote);
            $('#footnotesTab').css('background-color', '#e8a713')

        }
        else {
            $('#footnotesTab').css('background-color', '#931729')
            $('.footNoteContent').html('');
        }
    }, 4000);
    $("#zoom").change(function () {
        var size = $(this).val();
        $("#sandbox").css('font-size', size + 'px');
        return false;
    });
    setTimeout(function () {
        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/jquery.texthighlighter-0.2.1/jquery.texthighlighter-0.2.1/src/jquery.textHighlighter.js';
        var head = document.getElementsByTagName('head')[0];
        var script2 = document.createElement('script');
        script2.type = 'text/javascript';
        script2.src = jqHighlighterFilePath;
        head.appendChild(script2);

        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/annotator_view-master/lib/annotator-full.1.2.9/annotator-full.min.js';

        var script3 = document.createElement('script');
        script3.type = 'text/javascript';
        script3.src = jqHighlighterFilePath;
        head.appendChild(script3);


        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/annotator_view-master/lib/jquery.dateFormat.js';

        var script99 = document.createElement('script');
        script99.type = 'text/javascript';
        script99.src = jqHighlighterFilePath;
        head.appendChild(script99);

        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/annotator_view-master/lib/jquery-i18n-master/jquery.i18n.min.js';

        var script4 = document.createElement('script');
        script4.type = 'text/javascript';
        script4.src = jqHighlighterFilePath;
        head.appendChild(script4);

        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/annotator_view-master/locale/en/annotator.js';

        var script5 = document.createElement('script');
        script5.type = 'text/javascript';
        script5.src = jqHighlighterFilePath;
        head.appendChild(script5);

        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/annotator_view-master/src/view_annotator.js';

        var script6 = document.createElement('script');
        script6.type = 'text/javascript';
        script6.src = jqHighlighterFilePath;
        head.appendChild(script6);

        var jqHighlighterFilePath = 'http://coursematerials.managementconcepts.com:8088/annotator_view-master/src/categories.js';

        var script7 = document.createElement('script');
        script7.type = 'text/javascript';
        script7.src = jqHighlighterFilePath;
        head.appendChild(script7);
    }, 4000);



    setTimeout(function () {
        var a = window.location.href;
        if (a.indexOf('#') === -1) {

        }
        else {
            var someVarName = a;
            localStorage.setItem("someVarName", a);

            // c = a;

        }


    }, 4000);



    //alert('test')
    $('#userContent').html('');
    var userid = '';
    var courseid = '';
    var juliandate = '';
    userid = GetParameterValues('uid');

    //if (courseid.indexOf('#') > 0) {

    var uids = userid.split('#');
    userid = uids[0];

    //}
    courseid = GetParameterValues('cid');
    //if (courseid.indexOf('#') > 0) {
    var cid = courseid.split('#');
    courseid = cid[0];
    //}

    juliandate = GetParameterValues('jid');
    //alert(userid + courseid + juliandate)
    function GetParameterValues(param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    }

    /*FootNote Start here*/

    /*FootNote End here*/

    //setTimeout(function () {

    //    var footerNote = '';
    //    var n = 1;
    //    $('.FootnoteContent').each(function () {
    //        footerNote += n + '.' + $(this).text() + "<br \>";
    //        n = n + 1;

    //    });

    //    if (footerNote != undefined && footerNote != '') {
    //        $('.footNoteContent').html(footerNote);
    //        $('#footnotesTab').css('background-color', '#e8a713')

    //    }
    //    else {
    //        $('#footnotesTab').css('background-color', '#931729')
    //        $('.footNoteContent').html('');

    //    }
    //}, 2000);

    setTimeout(function () {
        //$('#userContent').html('');

        var Getpagename = '';
        Getpagename = window.location.href;
        //alert(userid + ',' + courseid+',' + juliandate + ',' + Getpagename)
        $.ajax({

            type: "POST",
            url: "CS.aspx/GetBooknotes",
            data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                //$('#userContent').val(data.d);
                // alert(data.d)
                $('#userContent').html(data.d);
                if (data.d != null && data.d != '') {
                    $('#notesTab').css('background-color', '#e8a713')
                }
                else {
                    $('#notesTab').css('background-color', '#931729')

                }
                data = '';

            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }, 4000);

    setTimeout(function () {
        var Getpagename = '';
        Getpagename = window.location.href;
        $.ajax({

            type: "POST",
            url: "CS.aspx/GetBooknotes",
            data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                //$('#userContent').val(data.d);
                $('#userContent').html(data.d);
                data = '';

            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }, 4000);

    setTimeout(function () {
        $.ajaxSetup({ cache: false });
        $.ajax({

            type: "POST",
            url: "CS.aspx/GetGlobalnotes",
            data: "{userid: '" + userid + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                $('#footNoteContent1').val(data.d);
                data.d = '';


            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }, 4000);

    $("#footNoteContent1").blur(function () {
        var Getpagename = '';
        Getpagename = window.location.href;

        var footNoteContent = $("#footNoteContent1").text()
        $.ajax({
            type: "POST",
            url: "CS.aspx/InsertGlobalNotes",
            data: "{userid: '" + userid + "',Globalnotes: '" + footNoteContent + "',Getpagename: '" + Getpagename + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {

            },
            failure: function (response) {
                alert(response.d);

            }
        });
        if ($("#footNoteSlide").attr("visibility") == "visible") {

            $("#footNoteSlide").attr("visibility", "hidden");
            $("#footnotesTab").animate({ marginLeft: "260px" }, 500);
            $("#footNoteSlide").animate({ marginLeft: "280px" }, 500);

        }
    });

    $("#userContent").blur(function () {

        var Getpagename = '';
        Getpagename = window.location.href;
        //alert(juliandate)
        var userContent
        if ($("#userContent").text().trim() == '')
            userContent = '';
        else
            userContent = $("#userContent").text();
        userContent = userContent.replace("'", '');


        $.ajax({
            type: "POST",
            url: "CS.aspx/InsertBooknotes",
            data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',BookNotes: '" + userContent + "',Getpagename: '" + Getpagename + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {//alert(userContent)
            },
            failure: function (response) {
                alert(response.d);

            }
        });
        if ($("#userContent").text().trim() != "") {
            $('#notesTab').css('background-color', '#e8a713')
        }
        else {
            $('#notesTab').css('background-color', '#931729')
        }
        if ($("#noteSlide").attr("visibility") == "visible") {

            $("#noteSlide").attr("visibility", "hidden");
            $("#notesTab").animate({ marginLeft: "260px" }, 500);
            $("#noteSlide").animate({ marginLeft: "280px" }, 500);

        }

    });
    $(document).delegate('span.annotator-hl.annotator-hl-errata', 'click', function (event) {
        deleteTargetId = $(this).attr('id');
        if ($('#popup').css('display') == 'none') {
            $('#popup').css('display', 'block');
            $('#popup').css('left', event.pageX - 150);
            $('#popup').css('top', event.pageY + 10);
            //$('#popup').css('display', 'inline');
            $('#popup').css('position', 'absolute');
            var pageHeight = $('#PageBody').height();
            var popupTop = event.pageY;
            var heightDiff = pageHeight - popupTop;
            if (heightDiff < 150) {
                $('#popup').css('top', event.pageY - 160);
            }
            if (event.pageX <= 190) {
                $('#popup').css('left', 19);
            }
            if (event.pageX >= 800) {
                $('#popup').css('left', 645);
            }
            //var id = $(this).attr('id');            
            selectedSpan = $(this)[0].outerHTML;
            selectedSpanText = $(this)[0].innerHTML;

            targetId = event.target.id;


            getData(targetId);
        }
        //else {
        //     $('#popup').hide();
        //     $('#editContainer').prop('readonly', true);
        //     $('#buttonSaveCancel').hide();
        // }
    });
    $("#PageBody").click(function (event) {
        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();
    });
    //$(document).on('mouseover', 'span.annotator-hl.annotator-hl-errata', function (event) {
    //    var isEditContainer = $('#editContainer').attr('readonly')

    //     if (isEditContainer != undefined) {
    //        $('.annotator-widget').removeClass('annotatorWidget');
    //    }
    //     $('.annotator-outer.annotator-viewer ').addClass('annotator_hide_custom');

    //     $('#popup').css('left', event.pageX - 150);
    //     $('#popup').css('top', event.pageY);
    //   $('#popup').css('display', 'inline');
    // $('#popup').css('position', 'absolute');
    // if (event.pageX <= 190) {
    //   $('#popup').css('left', 19);
    //}
    //if (event.pageX >= 872) {
    //  $('#popup').css('left', 645);
    //}

    //selectedSpan = $(this)[0].outerHTML;
    //selectedSpanText = $(this)[0].innerHTML;

    //targetId = event.target.id;


    //        getData(targetId);


    //  });
    //$(document).on('mouseover', 'span.annotator-hl.annotator-hl-errata', function (event) {

    //    $('.annotator-outer.annotator-viewer ').addClass('annotator_hide_custom');

    //        $('#popup').css('left', event.pageX-150);
    //        $('#popup').css('top', event.pageY);
    //        $('#popup').css('display', 'inline');
    //        $('#popup').css('position', 'absolute');
    //        if (event.pageX <= 190) {
    //            $('#popup').css('left', 19);
    //        }
    //        if (event.pageX >= 872) {
    //            $('#popup').css('left', 645);
    //        }
    //    //var id = $(this).attr('id');            
    //        selectedSpan = $(this)[0].outerHTML;
    //        selectedSpanText = $(this)[0].innerHTML;           
    //        targetId = event.target.id;           

    //        getData(targetId);


    //});

    $(document).on('mouseover', 'span.highlighted', function (event) {
        targetId = event.target.id;
    });

    //var siteAddress = location.protocol + '//' + document.location.hostname + ":51089";
    //var jqCoreFilePath = 'http://htmlviewer/jquery.texthighlighter-0.2.1/jquery.texthighlighter-0.2.1/lib/jquery-1.6.1.min.js';
    //var jqCoreFilePath = 'http://htmlviewer/Scripts/jquery-1.10.2.min.js';

    //var jqRangeCoreFilePath = 'http://htmlviewer/jquery.texthighlighter-0.2.1/jquery.texthighlighter-0.2.1/lib/rangy-core.js';
    //var jqHighlighterFilePath = 'http://htmlviewer/jquery.texthighlighter-0.2.1/jquery.texthighlighter-0.2.1/src/jquery.textHighlighter.js';
    //var head = document.getElementsByTagName('head')[0];


    var highData;
    //$('#btnAnnotator').addClass('activeAnnotator');
    $('#remove-highlights-btn').on("click", function () {
        $(this).val('stopHighlightAll');
        $('.annotator').val('');
        $('#remove-highlights-btn').addClass('activeRemoveHighlight');
        $('#btnhighlighter').removeClass('activeHighlight');
        $('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
        $('#btnAnnotator').addClass('activeAnnotator');
        $('#remove-highlights-btn-selected').attr('disabled', 'disabled');
        //$('#remove-highlights-btn-selected').prop("disabled", false);
        var xKeyPressed = false;
        var xKeyCode = 1;

        $(window).mousedown(function (e) {
            xKeyPressed = (e.which == xKeyCode);
        }).mouseup(function (e) {
            xKeyPressed = !(e.which == xKeyCode);

            d = $('.OneImagePage div:first').attr('style');
            if ($('.annotator').val() == '') {
                $('.annotator-adder').css('display', 'none');
            }
        });
        var content = $(document);

        content.delegate(".highlighted", "mouseup", function () {

            if (xKeyPressed) {
                $('#sandbox').textHighlighter({
                    color: '#F6CB96'
                });
                highlighter = $('#sandbox').getHighlighter();

                if ($('#btnAnnotator').val() == '' && $('#btnhighlighter').attr('class') == 'addHighlight') {
                    $('#sandbox').getHighlighter().removeHighlights(this);
                    //$('span[id^=' + targetId + ']').tagRemover();
                    $('span[id^=' + targetId + '].highlighted').tagRemover();
                    $('.annotator-adder').css('display', 'none');
                }

                datas = $('#PageBody').html();
                if (datas.indexOf('PageResourcesButtonContainer') > 0) {
                    datas = $('.Topic').html();
                }
                action = 1;
            }
        });

    });




    // $('#remove-highlights-btn').click(function ()
    //{
    //    $(this).val('stopHighlightAll');
    //    $('.annotator').val('');
    //    $('#remove-highlights-btn').addClass('activeRemoveHighlight');
    //    $('#btnhighlighter').removeClass('activeHighlight');
    //    $('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
    //    $('#btnAnnotator').addClass('activeAnnotator');
    //    //$('#remove-highlights-btn-selected').attr('disabled','disabled');
    //    var xKeyPressed = false;
    //    var xKeyCode = 1;

    //    $(window).mousedown(function (e) {
    //        xKeyPressed = (e.which == xKeyCode);
    //    }).mouseup(function (e) {
    //        xKeyPressed = !(e.which == xKeyCode);
    //        if ($('.annotator').val() == '') {
    //            $('.annotator-adder').css('display', 'none');
    //        }
    //    });
    //    $('#sandbox').textHighlighter({
    //        color: '#F6CB96'
    //    });
    //    highlighter = $('#sandbox').getHighlighter();

    //    $('#sandbox').getHighlighter().removeHighlights();
    //    datas = $('#PageBody').html();
    //    d = $('.OneImagePage div:first').attr('style');
    //    action = 1
    //    //$('#btnhighlighter').unbind('click');
    //}

    //);

    $('#btnhighlighter').on('click', function () {
        $('.annotator').val('');
        $('.undoHighlight').val('');
        $('.removeHighlight').val('');
        $('#btnhighlighter').addClass('activeHighlight');
        $('#remove-highlights-btn').removeClass('activeRemoveHighlight');
        $('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
        $('#btnAnnotator').addClass('activeAnnotator');
        var xKeyPressed = false;
        var xKeyCode = 1;

        $('#sandbox').textHighlighter({
            color: '#F6CB96'
        });
        highlighter = $('#sandbox').getHighlighter();

        highData = highlighter.serializeHighlights();

        $(window).mousedown(function (e) {
            xKeyPressed = (e.which == xKeyCode);
        }).mouseup(function (e) {
            xKeyPressed = !(e.which == xKeyCode);

            $('.annotator-hl.annotator-hl-errata').each(function () {
                var kk = $(this)[0].children.length;

                //alert($(this)[0].children('span').length)
                //var ddd = $(this)[0].children().length;
                //alert(ddd)
                if (kk > 0) {

                    $(this).find('span.highlighted').tagRemover();
                    //$('span[id^=' + targetId + ']').tagRemover();
                    //alert('Not valid selection. Annotated portion can not be highlighted')
                }

            });

            $('span.highlighted').each(function () {
                var kk = $(this)[0].children.length;

                if (kk > 0) {

                    $(this).find('span.highlighted').tagRemover();

                }

            });

            datas = $('#PageBody').html();
            if (datas.indexOf('PageResourcesButtonContainer') > 0) {
                datas = $('.Topic').html();
            }

            d = $('.OneImagePage div:first').attr('style');
            $('#sandbox').textHighlighter({
                color: '#F6CB96'
            });
            highlighter = $('#sandbox').getHighlighter();

            highData = highlighter.serializeHighlights();

            action = 1
        });
    });
    function getSelectedText() {
        if (window.getSelection) {
            return window.getSelection().toString();
        }
        else if (document.getSelection) {
            return document.getSelection();
        }
        else if (document.selection) {
            return document.selection.createRange().text;
        }
    }
    $('.annotator').click(function () {
        $(this).val('AnnotateME');
        $('#btnAnnotator').removeClass('activeAnnotator');
        $('#remove-highlights-btn').removeClass('activeRemoveHighlight');
        $('#btnhighlighter').removeClass('activeHighlight');
        $('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
        //$('.highlighted').unbind('mouseup');

        //$('#sandbox').textHighlighter({
        //    color: '#FFF'
        //});
        //var xKeyPressed = false;
        //var xKeyCode = 1;
        //$(window).mousedown(function (e) {
        //    xKeyPressed = (e.which == xKeyCode);
        //}).mouseup(function (e) {
        //    xKeyPressed = !(e.which == xKeyCode);
        //    if ($('.annotator').val() == '') {
        //        $('.annotator-adder').css('display', 'none');
        //    }
        //});
        //$('.annotator-adder').css('display', 'block');


    });

    var highlighter;



    //Page load[start]
    setTimeout(function () {


        aP = window.location.href;
        if (aP.indexOf('#') === -1) {

        }
        else {

            var someVarNameP = '';
            someVarNameP = aP;
            localStorage.setItem("someVarNameP", aP);
            // alert("current page" + a1);

            // c = a;

        }

        var someVarNameP = '';
        passPageP = '';
        passPageP = aP;
        someVarNameP = localStorage.getItem("someVarNameP");
        localStorage.removeItem('someVarNameP');
        var PageNameP = '';
        PageNameP = someVarNameP;
        var tP = '';
        tP = PageNameP;
        // alert(userid + courseid + juliandate)
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: "POST",
            url: "CS.aspx/GetQueryInfo",
            data: "{PageName: '" + aP.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.d != null) {
                    var highlightedData = data.d;
                    highlightedData = highlightedData.replace(/[\[\]]+/g, '');
                    oData = data.d;
                    if (oData.indexOf('Topic') == 13) {
                        $('.Topic').empty();
                        $('.Topic').append(highlightedData);
                        $('.Topic').bind();
                    }
                    else {
                        $('#PageBody').empty();
                        $("#PageBody").append(highlightedData);
                        $("#PageBody").bind();
                    }
                }
            },
            failure: function (response) {
                alert(response.d);

            }
        });
        $.ajaxSetup({ cache: false });
        $.ajax({
            //type: "POST",
            //url: "http://10.163.105.98:97/JSONP-Service/JSONP-EndPoint.asmx/GetQueryInfo",
            //data: "{Page: '" + Page + "'}",
            //contentType: "application/jsonp;charset=utf-8",
            //dataType: "json",

            type: "POST",
            url: "CS.aspx/GetQueryInfoAnnotation",
            data: "{PageName: '" + aP + "',userid: '" + userid + "',classid: '" + courseid + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {

                if (data.d != null) {
                    oData = data.d;
                    if (oData.indexOf('Topic') == 12) {
                        $('.Topic').empty();
                        $('.Topic').append(data.d);
                        $('.Topic').bind();
                    }
                    else {
                        $('#PageBody').empty();
                        $("#PageBody").append(data.d);
                        $("#PageBody").bind();
                    }

                }
            },
            failure: function (response) {
                alert(response.d);

            }
        });



    }, 4000);
    function OnSuccess(response) {

        alert(response.d);
    }


    $('.NavigationNode > .NavigationTreeItem').click(function () {
        $.ajaxSetup({ cache: false });

        //$('#btnhighlighter').removeClass('activeHighlight');
        //$('#remove-highlights-btn').removeClass('activeRemoveHighlight');
        //$('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
        //$('#btnAnnotator').removeClass('activeAnnotator');

        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();

        setTimeout(function () {
            var footerNote = '';
            var n = 1;
            $('.FootnoteContent').each(function () {
                footerNote += n + '.' + $(this).html() + "<br \>";
                n = n + 1;
            });
            if (footerNote != undefined && footerNote != '') {
                $('.footNoteContent').html(footerNote);
                $('#footnotesTab').css('background-color', '#e8a713')

            }
            else {
                $('#footnotesTab').css('background-color', '#931729')
                $('.footNoteContent').html('');
            }
            $.ajax({
                type: "POST",
                url: "CS.aspx/Bookmark",
                data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + window.location.href + "', juliandate: '" + juliandate + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                }
               ,
                failure: function (response) {
                    alert('F');

                }
            });
        }, 1500);

        $('#userContent').html('');
        setTimeout(function () {
            var Getpagename = '';
            Getpagename = window.location.href;

            $.ajax({

                type: "POST",
                url: "CS.aspx/GetBooknotes",
                data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    //$('#userContent').val(data.d);

                    $('#userContent').html(data.d);
                    if (data.d != null && data.d != '') {
                        $('#notesTab').css('background-color', '#e8a713')
                    }
                    else {
                        $('#notesTab').css('background-color', '#931729')

                    }
                    data = '';

                },
                failure: function (response) {
                    alert(response.d);

                }
            });
        }, 1200);

        $.ajaxSetup({ cache: false });
        var highlightedIndex = datas.indexOf('highlighted');
        var annotatorIndex = datas.indexOf('annotator-hl');


        if (datas != '' && datas != undefined && action == 1) {
            //alert(datas)
            datas = datas.replace(/[\[\]]+/g, '');

            //alert(datas)
            var repd = '';
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 && d != undefined) {
                repd = d.replace(/"/g, "&quot;");
                datas = datas.replace(d, repd);
                //datas = datas.replace(/"/g, "&quot;");
                datas = datas.replace(/'/g, "&quot;&quot;");

            }
            else {
                datas = datas.replace(/'/g, '&quot;');
                //alert(datas+'ff')
            }

            var pattern = new RegExp('</?a ?[^>]*>', 'g');
            var clean = datas; //.replace(pattern, '');

            var a = clean.replace("Main Content", "");
            var datta = '';
            if (a.indexOf('Topic') > -1) {

                datta = a;
            }
            else {

                var addbefore = '<div class="Topic">';
                var addafter = '</div>';
                datta = '<div class="Topic">' + a + '</div>'

            }
            var Replacedata = '';
            Replacedata = datas.replace(/(<([^>]+)>)/ig, "");

            var PageName = '';
            PageName = window.location.href;
            //alert(data+PageName+userid+courseid)
            //if (datas != '' && datas != undefined) {

            $.ajax({
                type: "POST",
                url: "CS.aspx/UpdateDataAnnotation",
                data: "{PageName: '" + PageName + "', AnnotationData:'" + datta + "',userid: '" + userid + "',classid: '" + courseid + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    //alert('Pass')
                },
                failure: function (response) {
                    alert('F');
                }
            });
            //}
        }
        var PageName = window.location.href;

        var dat;
        $('.annotator').val('');

        $.ajaxSetup({ cache: false });



        if (highData != undefined) {

        }


        $.ajaxSetup({ cache: false });



        var a1 = '';
        var passPage = '';


        setTimeout(function () {
            if (!$('#btnAnnotator').hasClass('activeAnnotator')) {
                $('#btnAnnotator').val('AnnotateME')
            }
            a1 = window.location.href;
            if (a1.indexOf('#') === -1) {

            }
            else {

                var someVarName = '';
                someVarName = a1;
                localStorage.setItem("someVarName1", a1);
                // alert("current page" + a1);

                // c = a;

            }



            var someVarName1 = '';
            passPage = '';
            passPage = a1;
            someVarName1 = localStorage.getItem("someVarName1");
            localStorage.removeItem('someVarName1');
            var PageName1 = '';
            PageName1 = someVarName1;
            var t1 = '';
            t1 = PageName1.toString();

            // alert(t1);




            $.ajax({
                type: "POST",
                url: "CS.aspx/GetQueryInfoAnnotation",
                data: "{PageName: '" + t1.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {

                    if (data.d != null) {
                        oData = data.d;
                        if (oData.indexOf('Topic') == 12) {
                            $('.Topic').empty();
                            $('.Topic').append(data.d);
                            $('.Topic').bind();
                        }
                        else {
                            $('#PageBody').empty();
                            $("#PageBody").append(data.d);
                            $("#PageBody").bind();
                        }

                    }
                },
                failure: function (response) {
                    alert(response.d);

                }
            });


            $.ajax({
                type: "POST",
                url: "CS.aspx/GetQueryInfo",
                data: "{PageName: '" + t1.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != null) {
                        var highlightedData = data.d;
                        highlightedData = highlightedData.replace(/[\[\]]+/g, '');
                        oData = data.d;
                        if (oData.indexOf('Topic') == 13) {
                            $('.Topic').empty();
                            $('.Topic').append(highlightedData);
                            $('.Topic').bind();
                        }
                        else {
                            $('#PageBody').empty();
                            $("#PageBody").append(highlightedData);
                            $("#PageBody").bind();
                        }
                    }

                },
                failure: function (response) {
                    alert(response.d);

                }
            });

        }, 800);
    });

    $("#NavigationButtonNext").click(function () {
        $.ajaxSetup({ cache: false });
        $('#NavigationButtonNext').attr('disabled', 'disabled');
        $('#NavigationButtonNext').attr('class', 'NavigationButton Button Enabled Visible sideNext');

        $('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
        $('#remove-highlights-btn').removeClass('activeRemoveHighlight');
        $('#btnhighlighter').removeClass('activeHighlight');
        $('#btnAnnotator').removeClass('activeAnnotator');



        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();

        setTimeout(function () {
            var htmldata = $('#PageBody').html();
            if (htmldata.indexOf('TabsComponent Horizontal') > -1) {
                //alert("Inside IF");
                $('.annotator').val('');
                $('#btnAnnotator').addClass('activeAnnotator');
                $('#btnAnnotator').attr('disabled', 'disabled');
                $('#btnhighlighter').attr('disabled', 'disabled');
                $('#remove-highlights-btn').attr('disabled', 'disabled');
                //Code to disable the selection of text
                $('#PageBody').attr('unselectable', 'on')
                $('#PageBody').css('user-select', 'none')
                $('#PageBody').css('-moz-user-select', 'none')
                $('#PageBody').css('-khtml-user-select', 'none')
                $('#PageBody').css('-webkit-user-select', 'none')
                $('#PageBody').on('selectstart', false)
                $('#PageBody').on('contextmenu', false)
                $('#PageBody').on('keydown', false)
                $('#PageBody').on('mousedown', false);
            }
            else {

                //alert("Inside Else");
                $('#btnAnnotator').removeAttr('disabled');
                $('#btnhighlighter').removeAttr('disabled');
                $('#remove-highlights-btn').removeAttr('disabled');
                $('.annotator').val('AnnotateME');
                $('#btnAnnotator').removeClass('activeAnnotator');
                //Code to enable the selection of text
                $('#PageBody').attr('unselectable', '')
                $('#PageBody').css('user-select', '')
                $('#PageBody').css('-moz-user-select', '')
                $('#PageBody').css('-khtml-user-select', '')
                $('#PageBody').css('-webkit-user-select', '')
                $('#PageBody').off('selectstart', false)
                $('#PageBody').off('contextmenu', false)
                $('#PageBody').off('keydown', false)
                $('#PageBody').off('mousedown', false);
            }
        }, 1500);

        //End 



        setTimeout(function () {
            var footerNote = '';
            var n = 1;
            $('.FootnoteContent').each(function () {
                footerNote += n + '.' + $(this).html() + "<br \>";
                n = n + 1;
            });
            if (footerNote != undefined && footerNote != '') {
                $('.footNoteContent').html(footerNote);
                $('#footnotesTab').css('background-color', '#e8a713')

            }
            else {
                $('#footnotesTab').css('background-color', '#931729')
                $('.footNoteContent').html('');
            }
            $.ajax({
                type: "POST",
                url: "CS.aspx/Bookmark",
                data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + window.location.href + "', juliandate: '" + juliandate + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                }
               ,
                failure: function (response) {
                    alert('F');

                }
            });
        }, 1500);
        SaveDataNextPrevQuestion()
        $('#userContent').html('');
        setTimeout(function () {
            var Getpagename = '';
            Getpagename = window.location.href;

            $.ajax({

                type: "POST",
                url: "CS.aspx/GetBooknotes",
                data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    //$('#userContent').val(data.d);
                    $('#userContent').html(data.d);
                    if (data.d != null && data.d != '') {
                        $('#notesTab').css('background-color', '#e8a713')
                    }
                    else {
                        $('#notesTab').css('background-color', '#931729')

                    }
                    data = '';

                },
                failure: function (response) {
                    alert(response.d);

                }
            });
        }, 1200);

        $.ajaxSetup({ cache: false });
        var highlightedIndex = datas.indexOf('highlighted');
        var annotatorIndex = datas.indexOf('annotator-hl');


        if (datas != '' && datas != undefined && action == 1) {
            //alert(datas)
            datas = datas.replace(/[\[\]]+/g, '');

            //alert(datas)
            var repd = '';
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 && d != undefined) {
                repd = d.replace(/"/g, "&quot;");
                datas = datas.replace(d, repd);
                //datas = datas.replace(/"/g, "&quot;");
                datas = datas.replace(/'/g, "&quot;&quot;");

            }
            else {
                datas = datas.replace(/'/g, '&quot;');
                //alert(datas+'ff')
            }

            var pattern = new RegExp('</?a ?[^>]*>', 'g');
            var clean = datas; //.replace(pattern, '');

            var a = clean.replace("Main Content", "");
            var datta = '';
            if (a.indexOf('Topic') > -1) {

                datta = a;
            }
            else {

                var addbefore = '<div class="Topic">';
                var addafter = '</div>';
                datta = '<div class="Topic">' + a + '</div>'

            }
            var Replacedata = '';
            Replacedata = datas.replace(/(<([^>]+)>)/ig, "");

            var PageName = '';
            PageName = window.location.href;
            //alert(data+PageName+userid+courseid)
            //if (datas != '' && datas != undefined) {
            if ((datta.indexOf("PopupWrapper") == -1) || (datta.indexOf("PopupWrapper ") == -1) || (datta.indexOf("TabsComponent Horizontal") == -1)) {
                if ((datta.indexOf("PopupWrapper") == -1) && (datta.indexOf("TabsComponent Horizontal") == -1)) {
                    $.ajax({
                        type: "POST",
                        url: "CS.aspx/UpdateDataAnnotation",
                        data: "{PageName: '" + PageName + "', AnnotationData:'" + datta + "',userid: '" + userid + "',classid: '" + courseid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            //alert('Pass')
                        },
                        failure: function (response) {
                            alert('F');
                        }
                    });
                }
            }
        }
        var PageName = window.location.href;

        var dat;
        $('.annotator').val('');
        //$('#sandbox').textHighlighter({
        //    color: '#F6CB96'
        //});
        //highlighter = $('#sandbox').getHighlighter();
        //dat = highlighter.serializeHighlights();

        //alert(highData)
        $.ajaxSetup({ cache: false });

        //alert('Cur Page'+PageName);

        if (highData != undefined) {
            //$.ajax({
            //    type: "POST",
            //    url: "CS.aspx/Insertdata",
            //    data: "{PageName: '" + PageName + "', HighlightData:'" + JSON.stringify(highData) + "',userid: '" + userid + "',classid: '" + courseid + "'}",
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    cache: false,
            //    success: function (data) {


            //    }
            //    ,
            //    failure: function (response) {
            //        alert('F');

            //    }
            //});
        }
        //Pageload in Next Button[Start]

        $.ajaxSetup({ cache: false });


        // var someVarName = localStorage.getItem("someVarName");
        // var PageName = window.location.href;
        // var t = PageName.toString();

        var a1 = '';
        var passPage = '';


        setTimeout(function () {
            if (!$('#btnAnnotator').hasClass('activeAnnotator')) {
                $('#btnAnnotator').val('AnnotateME')
            }
            a1 = window.location.href;
            if (a1.indexOf('#') === -1) {

            }
            else {

                var someVarName = '';
                someVarName = a1;
                localStorage.setItem("someVarName1", a1);
                // alert("current page" + a1);

                // c = a;

            }



            var someVarName1 = '';
            passPage = '';
            passPage = a1;
            someVarName1 = localStorage.getItem("someVarName1");
            localStorage.removeItem('someVarName1');
            var PageName1 = '';
            PageName1 = someVarName1;
            var t1 = '';
            t1 = PageName1.toString();

            // alert(t1);


            //alert(window.location.href+','+userid+','+courseid)
            $.ajax({
                type: "POST",
                url: "CS.aspx/GetQueryInfo",
                data: "{PageName: '" + t1.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != null && data.d != '') {
                        var highlightedData = data.d;
                        highlightedData = highlightedData.replace(/[\[\]]+/g, '');
                        oData = data.d;
                        if (oData.indexOf('Topic') == 13) {
                            $('.Topic').empty();
                            $('.Topic').append(highlightedData);
                            $('.Topic').bind();
                        }
                        else {
                            $('#PageBody').empty();
                            $("#PageBody").append(highlightedData);
                            $("#PageBody").bind();
                        }
                    }
                },
                failure: function (response) {
                    alert(response.d);

                }
            });
            $.ajax({
                type: "POST",
                url: "CS.aspx/GetQueryInfoAnnotation",
                data: "{PageName: '" + t1.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {

                    if (data.d != null && data.d != '') {
                        oData = data.d;
                        if (oData.indexOf('Topic') == 12) {
                            $('.Topic').empty();
                            $('.Topic').append(data.d);
                            $('.Topic').bind();
                        }
                        else {
                            $('#PageBody').empty();
                            $("#PageBody").append(data.d);
                            $("#PageBody").bind();
                        }

                    }
                },
                failure: function (response) {
                    alert(response.d);

                }
            });




        }, 1800);
        setTimeout(function () {
            $('#NavigationButtonNext').removeAttr('disabled', 'disabled');
            $('#NavigationButtonNext').removeAttr('class', 'NavigationButton Button Enabled Visible sideNext').attr('class', 'NavigationButton Button Enabled Visible');
        }, 3000);
        //Pageload in Next Button[End]
    });



    //previous [start]
    $("#NavigationButtonPrevious").click(function () {
        $.ajaxSetup({ cache: false });
        $('#NavigationButtonPrevious').attr('disabled', 'disabled');
        $('#NavigationButtonPrevious').attr('class', 'NavigationButton Button Enabled Visible sideNext');

        $('#remove-highlights-btn-selected').removeClass('activeRemoveAllHighlight');
        $('#remove-highlights-btn').removeClass('activeRemoveHighlight');
        $('#btnhighlighter').removeClass('activeHighlight');
        $('#btnAnnotator').removeClass('activeAnnotator');

        $('#popup').hide();
        $('#editContainer').prop('readonly', true);
        $('#buttonSaveCancel').hide();


        setTimeout(function () {
            var htmldata = $('#PageBody').html();
            if (htmldata.indexOf('TabsComponent Horizontal') > -1) {
                //alert("Inside IF");
                $('.annotator').val('');
                $('#btnAnnotator').addClass('activeAnnotator');
                $('#btnAnnotator').attr('disabled', 'disabled');
                $('#btnhighlighter').attr('disabled', 'disabled');
                $('#remove-highlights-btn').attr('disabled', 'disabled');
                //Code to disable the selection of text
                $('#PageBody').attr('unselectable', 'on')
                $('#PageBody').css('user-select', 'none')
                $('#PageBody').css('-moz-user-select', 'none')
                $('#PageBody').css('-khtml-user-select', 'none')
                $('#PageBody').css('-webkit-user-select', 'none')
                $('#PageBody').on('selectstart', false)
                $('#PageBody').on('contextmenu', false)
                $('#PageBody').on('keydown', false)
                $('#PageBody').on('mousedown', false);
            }
            else {

                //alert("Inside Else");
                $('#btnAnnotator').removeAttr('disabled');
                $('#btnhighlighter').removeAttr('disabled');
                $('#remove-highlights-btn').removeAttr('disabled');
                $('.annotator').val('AnnotateME');
                $('#btnAnnotator').removeClass('activeAnnotator');
                //Code to enable the selection of text
                $('#PageBody').attr('unselectable', '')
                $('#PageBody').css('user-select', '')
                $('#PageBody').css('-moz-user-select', '')
                $('#PageBody').css('-khtml-user-select', '')
                $('#PageBody').css('-webkit-user-select', '')
                $('#PageBody').off('selectstart', false)
                $('#PageBody').off('contextmenu', false)
                $('#PageBody').off('keydown', false)
                $('#PageBody').off('mousedown', false);
            }
        }, 1500);

        //End 



        setTimeout(function () {
            var footerNote = '';
            var n = 1;
            $('.FootnoteContent').each(function () {
                footerNote += n + '.' + $(this).html() + "<br \>";
                n = n + 1;
            });
            if (footerNote != undefined && footerNote != '') {
                $('.footNoteContent').html(footerNote);
                $('#footnotesTab').css('background-color', '#e8a713')

            }
            else {
                $('#footnotesTab').css('background-color', '#931729')
                $('.footNoteContent').html('');
            }
            $.ajax({
                type: "POST",
                url: "CS.aspx/Bookmark",
                data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + window.location.href + "', juliandate: '" + juliandate + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                }
          ,
                failure: function (response) {
                    alert('F');

                }
            });
        }, 1500);
        SaveDataNextPrevQuestion()
        $('#userContent').html('');

        setTimeout(function () {
            var Getpagename = '';
            Getpagename = window.location.href;
            $.ajax({

                type: "POST",
                url: "CS.aspx/GetBooknotes",
                data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {

                    //$('#userContent').val(data.d);
                    $('#userContent').html(data.d);

                    if (data.d != null && data.d != '') {
                        $('#notesTab').css('background-color', '#e8a713')
                    }
                    else {
                        $('#notesTab').css('background-color', '#931729')

                    }
                    data = '';

                },
                failure: function (response) {
                    alert(response.d);

                }
            });
        }, 1200);

        var highlightedIndex = datas.indexOf('highlighted');
        var annotatorIndex = datas.indexOf('annotator-hl');


        if (datas != '' && datas != undefined && (highlightedIndex > 0 || annotatorIndex > 0)) {
            datas = datas.replace(/[\[\]]+/g, '');
            var repd = '';
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 && d != undefined) {
                repd = d.replace(/"/g, "&quot;");
                datas = datas.replace(d, repd);
                //datas = datas.replace(/"/g, "&quot;");
                datas = datas.replace(/'/g, "&quot;&quot;");

            }
            else {
                datas = datas.replace(/'/g, '&quot;');
                //alert(datas+'ff')
            }

            var pattern = new RegExp('</?a ?[^>]*>', 'g');
            var clean = datas; //.replace(pattern, '');

            var a = clean.replace("Main Content", "");
            var datta = '';
            if (a.indexOf('Topic') > -1) {

                datta = a;
            }
            else {

                var addbefore = '<div class="Topic">';
                var addafter = '</div>';
                datta = '<div class="Topic">' + a + '</div>'

            }
            var Replacedata = '';
            Replacedata = datas.replace(/(<([^>]+)>)/ig, "");


            var PageName = '';
            PageName = window.location.href;
            //alert(data+PageName+userid+courseid)
            //if (datas != '' && datas != undefined) {

            if ((datta.indexOf("PopupWrapper") == -1) || (datta.indexOf("PopupWrapper ") == -1) || (datta.indexOf("TabsComponent Horizontal") == -1)) {
                if ((datta.indexOf("PopupWrapper") == -1) && (datta.indexOf("TabsComponent Horizontal") == -1)) {
                    $.ajax({
                        type: "POST",
                        url: "CS.aspx/UpdateDataAnnotation",
                        data: "{PageName: '" + PageName + "', AnnotationData:'" + datta + "',userid: '" + userid + "',classid: '" + courseid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            //alert('Pass')
                        },
                        failure: function (response) {
                            alert('F');
                        }
                    });
                }
            }
        }

        var PageName = window.location.href;

        var dat;
        $('.annotator').val('');
        //$('#sandbox').textHighlighter({
        //    color: '#F6CB96'
        //});
        //highlighter = $('#sandbox').getHighlighter();
        //dat = highlighter.serializeHighlights();

        //alert(highData)
        $.ajaxSetup({ cache: false });

        //alert('Cur Page'+PageName);

        if (highData != undefined) {
            //$.ajax({
            //    type: "POST",
            //    url: "CS.aspx/Insertdata",
            //    data: "{PageName: '" + PageName + "', HighlightData:'" + JSON.stringify(highData) + "',userid: '" + userid + "',classid: '" + courseid + "'}",
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    cache: false,
            //    success: function (data) {


            //    }
            //    ,
            //    failure: function (response) {
            //        alert('F');

            //    }
            //});
        }

        var a1 = '';
        var passPage = '';
        setTimeout(function () {
            if (!$('#btnAnnotator').hasClass('activeAnnotator')) {
                $('#btnAnnotator').val('AnnotateME')
            }

            a1 = window.location.href;
            if (a1.indexOf('#') === -1) {

            }
            else {

                var someVarName = '';
                someVarName = a1;
                localStorage.setItem("someVarName1", a1);
                // alert("current page" + a1);

                // c = a;

            }



            var someVarName1 = '';
            passPage = '';
            passPage = a1;
            someVarName1 = localStorage.getItem("someVarName1");
            localStorage.removeItem('someVarName1');
            var PageName1 = '';
            PageName1 = someVarName1;
            var t1 = '';
            t1 = PageName1;

            // alert(t1);
            $.ajax({
                //type: "POST",
                //url: "http://10.163.105.98:97/JSONP-Service/JSONP-EndPoint.asmx/GetQueryInfo",
                //data: "{Page: '" + Page + "'}",
                //contentType: "application/jsonp;charset=utf-8",
                //dataType: "json",

                type: "POST",
                url: "CS.aspx/GetQueryInfo",
                data: "{PageName: '" + t1 + "' ,userid: '" + userid + "',classid: '" + courseid + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != null) {
                        var highlightedData = data.d;
                        highlightedData = highlightedData.replace(/[\[\]]+/g, '');
                        oData = data.d;
                        if (oData.indexOf('Topic') == 13) {
                            $('.Topic').empty();
                            $('.Topic').append(highlightedData);
                            $('.Topic').bind();
                        }
                        else {
                            $('#PageBody').empty();
                            $("#PageBody").append(highlightedData);
                            $("#PageBody").bind();
                        }
                    }
                },
                failure: function (response) {
                    alert(response.d);

                }
            });
            $.ajax({
                //type: "POST",
                //url: "http://10.163.105.98:97/JSONP-Service/JSONP-EndPoint.asmx/GetQueryInfo",
                //data: "{Page: '" + Page + "'}",
                //contentType: "application/jsonp;charset=utf-8",
                //dataType: "json",

                type: "POST",
                url: "CS.aspx/GetQueryInfoAnnotation",
                data: "{PageName: '" + t1 + "' ,userid: '" + userid + "',classid: '" + courseid + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {

                    if (data.d != null) {
                        oData = data.d;
                        if (oData.indexOf('Topic') == 12) {
                            $('.Topic').empty();
                            $('.Topic').append(data.d);
                            $('.Topic').bind();
                        }
                        else {
                            $('#PageBody').empty();
                            $("#PageBody").append(data.d);
                            $("#PageBody").bind();
                        }

                    }

                },
                failure: function (response) {
                    alert(response.d);

                }
            });

        }, 1800);
        setTimeout(function () {
            $('#NavigationButtonPrevious').removeAttr('disabled', 'disabled');
            $('#NavigationButtonPrevious').removeAttr('class', 'NavigationButton Button Enabled Visible sideNext').attr('class', 'NavigationButton Button Enabled Visible');
        }, 3000);
    });
    $(document).delegate('.ConfirmOkButton', 'click', function (event) {


        setTimeout(function () {
            loadHighlightAnnotation();
            loadFootnote();
            loadNote();
        }, 1800);
    });
    function loadFootnote() {
        var footerNote = '';
        var n = 1;
        $('.FootnoteContent').each(function () {
            footerNote += n + '.' + $(this).html() + "<br \>";
            n = n + 1;
        });
        if (footerNote != undefined && footerNote != '') {
            $('.footNoteContent').html(footerNote);
            $('#footnotesTab').css('background-color', '#e8a713')

        }
        else {
            $('#footnotesTab').css('background-color', '#931729')
            $('.footNoteContent').html('');
        }
    }
    function loadNote() {
        $('#userContent').html('');

        var Getpagename = '';
        Getpagename = window.location.href;

        $.ajax({

            type: "POST",
            url: "CS.aspx/GetBooknotes",
            data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                //$('#userContent').val(data.d);
                $('#userContent').html(data.d);
                if (data.d != null && data.d != '') {
                    $('#notesTab').css('background-color', '#e8a713')
                }
                else {
                    $('#notesTab').css('background-color', '#931729')

                }
                data = '';

            },
            failure: function (response) {
                alert(response.d);

            }
        });

    }
    function loadHighlightAnnotation() {
        var a1 = '';
        var passPage = '';
        if (!$('#btnAnnotator').hasClass('activeAnnotator')) {
            $('#btnAnnotator').val('AnnotateME')
        }
        a1 = window.location.href;
        if (a1.indexOf('#') === -1) {

        }
        else {

            var someVarName = '';
            someVarName = a1;
            localStorage.setItem("someVarName1", a1);
            // alert("current page" + a1);

            // c = a;

        }



        var someVarName1 = '';
        passPage = '';
        passPage = a1;
        someVarName1 = localStorage.getItem("someVarName1");
        localStorage.removeItem('someVarName1');
        var PageName1 = '';
        PageName1 = someVarName1;
        var t1 = '';
        t1 = PageName1.toString();

        // alert(t1);


        //alert(window.location.href+','+userid+','+courseid)
        $.ajax({
            type: "POST",
            url: "CS.aspx/GetQueryInfo",
            data: "{PageName: '" + t1.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.d != null && data.d != '') {
                    var highlightedData = data.d;
                    highlightedData = highlightedData.replace(/[\[\]]+/g, '');
                    oData = data.d;
                    if (oData.indexOf('Topic') == 13) {
                        $('.Topic').empty();
                        $('.Topic').append(highlightedData);
                        $('.Topic').bind();
                    }
                    else {
                        $('#PageBody').empty();
                        $("#PageBody").append(highlightedData);
                        $("#PageBody").bind();
                    }
                }
            },
            failure: function (response) {
                alert(response.d);

            }
        });
        $.ajax({
            type: "POST",
            url: "CS.aspx/GetQueryInfoAnnotation",
            data: "{PageName: '" + t1.toString() + "',userid: '" + userid + "',classid: '" + courseid + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {

                if (data.d != null && data.d != '') {
                    oData = data.d;
                    if (oData.indexOf('Topic') == 12) {
                        $('.Topic').empty();
                        $('.Topic').append(data.d);
                        $('.Topic').bind();
                    }
                    else {
                        $('#PageBody').empty();
                        $("#PageBody").append(data.d);
                        $("#PageBody").bind();
                    }

                }




            },
            failure: function (response) {
                alert(response.d);

            }
        });





    }
    $.i18n.load(i18n_dict);
    var annotator = $('body').annotator().annotator().data('annotator');
    var propietary = 'Aptara';
    annotator.addPlugin('Permissions', {
        user: propietary,
        permissions: {
            'read': [propietary],
            'update': [propietary],
            'delete': [propietary],
            'admin': [propietary]
        },
        showViewPermissionsCheckbox: false,
        showEditPermissionsCheckbox: false
    });
    $('body').annotator().annotator('addPlugin', 'AnnotatorViewer');
    $('body').annotator().annotator('addPlugin', 'Categories', {
        errata: 'annotator-hl-errata',
        destacat: 'annotator-hl-destacat',
        subratllat: 'annotator-hl-subratllat'
    }
       );

    //previous [end]



    //$("#NavigationButtonNext").click(function () {
    //    //var HighlightData = highlighter.serializeHighlights();
    //    var PageName = window.location.href;


    //    $.ajax({
    //        type: "POST",
    //        url: "CS.aspx/Insertdata",
    //        data: "{ PageName: '" + PageName + "', HighlightData: '" + highlighter.serializeHighlights() + "'}",
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (r) {
    //            alert(r.d);
    //        },
    //        error: function (r) {
    //            alert(r.responseText);
    //        },
    //        failure: function (r) {
    //            alert(r.responseText);
    //        }
    //    });
    //    return false;
    //});

    //function OnSuccess(response) {
    //    alert(response.d);
    //}

    //$('#sandbox').textHighlighter();
    //highlighter = $('#sandbox').getHighlighter();
    //$('#NavigationButtonNext').click(function () {


    //    // var jsonStr = highlighter.serializeHighlights();
    //    // var url = window.location.href;
    //    // var parameters = "{'PageName':'" + JSON.stringify(url) + "','HighlightData':'" + JSON.stringify(jsonStr) + "'}";
    //    // alert(parameters);
    //    //$.ajax({
    //    //    type: "POST",
    //    //    contentType: "application/json; charset=utf-8",
    //    //    url: "WebService2.asmx/Insertdata",
    //    //    data: {},

    //    //    dataType: "json",
    //    //    success: function (response) {
    //    //        alert("User has been added successfully.");
    //    //        window.location.reload();
    //    //        error: OnGetMemberError
    //    //    }
    //    //});
    //    //    $.ajax({
    //    //        type: "POST",
    //    //        contentType: "application/json; charset=utf-8",
    //    //        url: 'Service.svc/Hello',
    //    //        data: "{}",            
    //    //        dataType: "json",
    //    //        success: OnSuccess,
    //    //        error: OnError
    //    //    });
    //    //});
    //    //function OnSuccess(data, status) {
    //    //    alert(status);
    //    //}

    //    //function OnError(request, status, error) {
    //    //    alert(request);
    //    //}

    //    var jsonStr = highlighter.serializeHighlights();
    //    alert(JSON.stringify(jsonStr));

    //    // $('textarea').val(jsonStr);
    //    //highlighter.removeHighlights();

    ////    $.ajax({
    ////        type: "POST",
    ////        url: "http://publishservice/ServiceCS.asmx/GetCurrentTime.xml",
    ////        // data: "{ name: '" + name + "', age: " + age + "}",
    ////        data: '{ }',

    ////        dataType: "xml",
    ////      success: function(data) {
    ////          var xml= $.parseXML(data);
    ////          {
    ////              var obj = $.xml2json(xml);
    ////          }
    ////},
    ////        error: function (response) { alert(response.statusText); }
    ////    });
    //    return false;
    //    //alert(JSON.stringify(jsonStr));
    //});
    //function blah(data) {
    //    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
    //    $('#summary').html('<p>All new content. <em>You bet!</em></p>');
    //    $('#summary').html(result);
    //}


    //$('#deserialize-btn').click(function () {


    //    var jsonStr = $('textarea').val();

    //    highlighter.deserializeHighlights(jsonStr);


    //});
    //$('.highlighted').live('mouseup', function (e) {

    //    if (xKeyPressed) {
    //        $('#sandbox').getHighlighter().removeHighlights(this);
    //    }
    //});

    $('#btnSearch').click(function () {

        var txtsearchdata = $('#txtSearch').val();
        var myHilitor = new Hilitor(txtsearchdata);
        myHilitor.apply(txtsearchdata);

    });
    $('#btnAnnotator').click(function () {

        //$('.annotator-adder').css('display','none')
    });

    function SaveDataNextPrevQuestion() {
        setTimeout(function () {

            $.ajax({
                type: "POST",
                url: "CS.aspx/Bookmark",
                data: "{userid: '" + userid + "',classid: '" + courseid + "' , CurrentPage: '" + window.location.href + "', juliandate: '" + juliandate + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                }
               ,
                failure: function (response) {
                    alert('F');

                }
            });
            $('#userContent').html('');
            var Getpagename = '';
            Getpagename = window.location.href;
            $.ajax({

                type: "POST",
                url: "CS.aspx/GetBooknotes",
                data: "{userid: '" + userid + "',courseid: '" + courseid + "',juliandate: '" + juliandate + "',Getpagename: '" + Getpagename + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    //$('#userContent').val(data.d);
                    $('#userContent').html(data.d);
                    if (data.d != null && data.d != '') {
                        $('#notesTab').css('background-color', '#e8a713')
                    }
                    else {
                        $('#notesTab').css('background-color', '#931729')

                    }
                    data = '';

                },
                failure: function (response) {
                    alert(response.d);

                }
            });
        }, 3200);

    }


    function getVirtualDirectory() {
        var vDir = document.location.pathname.split('/');
        return '/' + vDir[1] + '/';
    }

});
   

    
    

    




