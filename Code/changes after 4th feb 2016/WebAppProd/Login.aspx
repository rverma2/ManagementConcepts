﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApp.Login1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="html">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <title>Home</title>
    <!-- CSS -->
    <link href="Style/bootstrap.css" rel="stylesheet" />
    <link href="Style/foo.css" rel="stylesheet" />
    <link href="Style/footer.css" rel="stylesheet" />
    <link href="Style/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/site_global1.css?3983832653" />
    <link rel="stylesheet" type="text/css" href="css/index1.css?89448219" id="pagesheet" />

     <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300italic,300,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

    <!-- Other scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript">
        document.documentElement.className += ' js';

        $(window).load(function () {
            $("#btnCloseChngPwd").click(function (e) {
                HideDialog();
                window.location.reload();
                e.preventDefault();
            });
            function HideDialog() {
                $("#overlay").hide();
                $("#dialog").fadeOut(600);
            }
           
            $('#linkForgotPassword').click(function () {
                $("#overlay").show();
                $("#dialog").fadeIn(600);
            });
            
        });
    </script>

    
    <style>
        .html {
            background-color: transparent;
        }
        .footer_top {
            background: #3c3c3c;
        }

    .skin_width {
    width: 100%;
    max-width: 1140px;
    min-width: 767px;
    margin: 0 auto;
    position: relative;
}


    .copyright_bar {
    border-top: 1px solid #222;
    color: #666;
}



    .glyphicon {
    display: inline-block;
    font-family: "Glyphicons Halflings";
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    position: relative;
    top: 1px;
}


    #footer p {
        font-family: "Droid Sans",Arial,sans-serif;
        line-height: 17px;
        margin: 0 0 6px;
    }
    #footer{
        font-family: "Droid Sans",Arial,sans-serif;
    }
    .c_title_footer {
        font-family: "Titillium Web",Arial,Tahoma,sans-serif;
        font-size: 20px !important;
        font-weight: 600;
    }
    .Footer {
        font-family: "Droid Sans",Arial,sans-serif !important;
    }
    </style>
</head>
<body>

    <div class="clearfix" id="page">
        <!-- column -->
        <div class="position_content" id="page_position_content">
            <div class="clearfix colelem" id="u69">
                <!-- group -->
                <div class="clip_frame grpelem" id="u71">
                    <!-- image -->
                    <img class="block" id="u71_img" src="images/logo.jpg" style="padding:0 25px;line-height:90px" alt="" width="190" height="90" />
                </div>
            </div>
            <div class="clearfix colelem" id="pu83">
                <!-- group -->
                <div class="grpelem" id="u83">
                    <!-- simple frame -->
                </div>
                <div class="grpelem" id="u84">
                    <!-- simple frame -->
                </div>
                <div class="clip_frame grpelem" id="u87">
                    <!-- image -->
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <script type="text/javascript" language="javascript">
                            Sys.Application.add_load(jScript);
                        </script>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" ImageUrl="~Images/loadinfo.net.gif" Width="48px"
                                    Height="48px" AlternateText="Processing" runat="server" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="UpdateProgress1"
                            PopupControlID="UpdateProgress1" BackgroundCssClass="modalBackground1">
                        </cc1:ModalPopupExtender>
                        <asp:UpdatePanel ID="updtPnl" runat="server">
                            <ContentTemplate>
                                <div id="main_container" class="main_container" style="width: 381px; height: 251px">
                                    <div class="lock" style="background-color: black; height: 210px">
                                        <div style="text-align: center;">
                                            <%-- <img src="Images/mconceptLogo.png" />--%>
                                        </div>
                                        <br />
                                        <div style="width: 380px; height: 1px; margin-bottom: 30px;">
                                        </div>
                                        <div style="width: 380px; height: 10px; margin-bottom: 30px;">
                                            <table>
                                                <tr>
                                                    <td width="30px"></td>
                                                    <td>

                                                        <asp:Label runat="server" Text="Username" ForeColor="White" Font-Bold="true" ID="lblname" />

                                                        <asp:TextBox ID="txtboxid" runat="server" ClientIDMode="Static" BorderStyle="None" Width="200px" Height="30px" CssClass="Loginad"></asp:TextBox>
                                                        <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtboxid" WatermarkCssClass="textboxLoginWatermark" WatermarkText="User Name">
        </cc1:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="width: 380px; height: 10px; margin-bottom: 30px;">
                                            <table>
                                                <tr>
                                                    <td width="30px"></td>
                                                    <td>

                                                        <asp:Label runat="server" Text="Password" ForeColor="White" Font-Bold="true" ID="Label1" />



                                                        <asp:TextBox ID="txtboxpwd" runat="server" ClientIDMode="Static" style="margin-left:2px;" BorderStyle="None" Width="204px" Height="30px" TextMode="Password" CssClass="Loginad"></asp:TextBox>
                                                        <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtboxpwd" WatermarkCssClass="textboxLoginWatermark" WatermarkText="Password">
        </cc1:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div style="width: 380px; height: 10px; margin-bottom: 50px; float: right; clear: both;">
                                            <table>
                                                <tr>
                                                    <td width="22px"></td>
                                                    <td width="220px" style="font-size: 12px; padding-left: 80px;">
                                                        <%--<asp:LinkButton runat="server" OnClientClick="ShowGenericDialog()" Text="Forgot your Password?" ForeColor="White" Font-Bold="true" />--%>
                                                        <a style="color:#fff;" id="linkForgotPassword" href="#">Forgot Password?</a>
                                                        <%--   <asp:Label runat="server" Text="Forget your Password?" ForeColor="White" Font-Bold="true" ID="Label2"  />--%>

                
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="Button5" ForeColor="Black" runat="server" Text="Log In" OnClientClick="return validate();" OnClick="Button5_Click" BorderStyle="None" Width="61px" Height="35px" CssClass="btnAdminLogin" />
                                                    </td>
                                                    <td width="50px"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="text-align:center;">
                                        <asp:Label ID="lblstatus" runat="server" style="text-align: center; font-weight: bold; bottom: 10px; color: rgb(246, 206, 206);" Text="Label"
                                            Visible="False"></asp:Label></div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div id="overlay" class="web_dialog_overlay"></div>
                        <div visibility="hidden" class="zoomCovePageDialog web_dialog" id="dialog" style="margin-top: -220px; display: none;">
                            <div class="zoom_title">
                                <h5>Forget Password</h5>
                            </div>
                            <div class="inner_space" id="changePwdContainer" style="height: 300px;">



                                <div class="password_box">

                                    <%--<form class="form-horizontal" role="form" method="post">--%>
                                        <asp:UpdatePanel ID="upForgetPanel" runat="server">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnChngPwd1" EventName="Click" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="inputEmail3">
                                                        Enter your email address</label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtEmailAddress" placeholder="Email address" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <%--<input type="text" autofocus="" placeholder="Email address" id="txtEmailAddress" name="txtEmailAddress" class="form-control" runat="server">--%>
                                                    </div>
                                                </div>

                                                <div class="form-group" runat="server" id="divPassword" visible="false">
                                                    <label class="col-sm-3 control-label" for="inputPassword3">
                                                        </label>
                                                    <div class="col-sm-9" >
                                                        <asp:Label ID="txtNewPassword" Enabled="false" CssClass="form-control removeborder" runat="server"></asp:Label>                                                       
                                                    </div>
                                                </div>


                                                </ContentTemplate>
                                        </asp:UpdatePanel>

                                                <div class="form-group col-sm-12 forgotPassword">
                                                    <button class="btn btn-default pass_cancel pull-right" id="btnCloseChngPwd" type="button" style="display: block;" runat="server">Close</button>
                                                    <%--<button class="btn btn-default pull-right pass_submit" runat="server" id="btnChngPwd" type="button" onclick="btnChngPwd_Click">Submit</button>--%>
                                                    <asp:Button ID="btnChngPwd1" runat="server" CssClass="btn btn-default pass_cancel pull-right" Text="Get Password" OnClick="btnChngPwd1_Click" />

                                                </div>
                                            
                                        <div class="clear"></div>
                                    <%--</form>--%>
                                </div>



                            </div>
                        </div>
                    </form>

                </div>
            </div>

<%--            <div id="footer" class="footer">
                <div class="pageNumbersContainer" id="pageNumbersContainer" style="display: none;">
                    <span class="pageNumberView" id="pageNumberingView"></span>
                </div>
                <div class="clearafter">

                    <div class="footer_container" id="footer_container">

                        <div id="footerpane" class="footerpane_style" style="display: block;">
                            <div class="col-xs-12 col-sm-4">
                                <h2>CONTACT US</h2>
                                <span class="contact_us">Management Concepts is the
							nation's premier provider of training and professinal development
							solutions serving the public and private sectors.</span> <span class="contact_us">8230 LeesburgPike, Tysons Corner, VA
							22182</span> <span class="contact_us">888.545.8571</span> <a href="mailto:info@ManagementConcepts.com" target="_blank"><span>Info@ManagementConcepts.com</span></a>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <h2>TRAINING BY TOPIC</h2>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Acquisition%20-%20Contracting" target="_blank">
                                        <span>FederalAcquisition &amp; Contracting</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Financial%20Management" target="_blank">
                                        <span>Federal Financial Management</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Leadership%20-%20Management" target="_blank">
                                        <span>Leadership &amp; Management</span>
                                    </a>/
					
                                </p>
                                <p>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Grants%20-%20Assistance" target="_blank">
                                        <span>Federal Grants &amp; Assistance</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Project%20-%20Program%20Management" target="_blank">
                                        <span>Federal Project &amp; Program Management</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Domain/id/Federal%20Human%20Capital%20and%20Human%20Resources" target="_blank">
                                        <span>Federal Human Capital &amp; Human Resources</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/CertificatePrograms" target="_blank">
                                        <span>Certificate Programs</span>
                                    </a>
                                </p>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                <h2>POPULAR LINKS</h2>
                                <p>
                                    <a href="http://www.managementconcepts.com/FormsCollection/RequestaCatalog" target="_blank">
                                        <span>Request a Catalog</span>
                                    </a>/
					
                                </p>
                                <p>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/How-to-Buy-from-Us" target="_blank">
                                        <span>How to Buy from Us</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Forms-Collection/Grants-ENewsletter-Signup" target="_blank">
                                        <span>Grants E-Newsletter</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Resources/Download-Free-White-Papers" target="_blank">
                                        <span>Request a Whitepaper</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://ecampus.managementconcepts.com" target="_blank"><span>eCampus</span></a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/About-Us/Careers" target="_blank"><span>Careers</span></a>/
					
                                </p>
                                <p>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Contact-Us/Classroom-Rental-Facilities" target="_blank">
                                        <span>Classroom Rental</span>
                                    </a>
                                </p>
                                <p>
                                    <a href="http://www.managementconcepts.com/Contact-Us/Frequently-Asked-Questions" target="_blank">
                                        <span>FAQ</span>
                                    </a>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="footerTop_border">
                            <div class="footer_left copyright_bar">
                                <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="Footer">Copyright
							&copy; 2015 Management Concepts</span> | <a href="http://www.managementconcepts.com/Contact-Us/Terms-and-Conditions" class="Footer" target="_blank">Terms
							and Conditions </a>| <a href="http://www.managementconcepts.com/Privacy-Policy" class="Footer" target="_blank">Privacy
							Policy</a>
                            </div>


                            <div class="socialpane_style">
                                <div class="Social-Links">
                                    <a class="iconFacebook" title="Facebook" href="http://www.facebook.com/managementconcepts" target="_blank"></a><a class="iconTwitter" title="Twitter" href="http://twitter.com/#!/mgmt_concepts" target="_blank"></a><a class="iconLinkedIn" title="LinkedIn" href="http://www.linkedin.com/company/management-concepts" target="_blank"></a><a class="iconYouTube" title="YouTube" href="http://www.youtube.com/mgmtconcepts" target="_blank"></a>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <img style="display: none;" id="ImgforCanvas" />
                <canvas style="display: none;" width="662" height="857" id="imageCanvas"></canvas>




            </div>--%>


                <div id="footer" class="footer">
<div class="footer_top">
    <div class="skin_width">
		<div style="border-bottom:2px groove;padding-top:20px;text-align:center;" class="dnnpane tagline">
          <img src="images/footer-tagline.png">
        </div>
		
      <div class="footerpane_style skin_main_padding">
        <div class="row dnnpane">
          <div id="dnn_FooterGrid3A" class="footer_grid3a col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-1423 DnnVersionableControl"><a name="1423"></a>

<div class="Footer01_style" style="width: 248px;">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr1423_dnnTITLE_titleLabel" class="c_title_footer">CONTACT US</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr1423_ContentPane" class="Footer01_content"><!-- Start_Module_1423 --><div id="dnn_ctr1423_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr1423_HtmlModule_lblContent"><span style="font-size: 11px; color: #ffffff;">
<p>Management Concepts is the nation’s premier provider of training and professional development solutions serving the
public and private sectors.</p>
<p><i style="font-size: 13px; color: #dea310; margin-right:6px" aria-hidden="true" class="fa fa-map-marker"></i>&nbsp;8230 Leesburg Pike, Tysons Corner, VA 22182&nbsp;</p>
<p><i style="font-size: 16px; color: #dea310; margin-right:9px"  aria-hidden="true" class="fa fa-mobile-phone"></i>&nbsp;888.545.8571 &nbsp;</p>
<p><span style="font-size: 14px; color: #dea310;" class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<a href="mailto:studentsupport@managementconcepts.com?subject=Electronic%20Materials%20Support"><span style="font-size: 11px; color: #ffffff;">studentsupport@managementconcepts.com</span></a> </p>
</span></div>




</div><!-- End_Module_1423 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
          <div id="dnn_FooterGrid3B" class="footer_grid3b col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-429 DnnVersionableControl"><a name="429"></a>

<div class="Footer01_style">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr429_dnnTITLE_titleLabel" class="c_title_footer">TRAINING BY TOPIC</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr429_ContentPane" class="Footer01_content"><!-- Start_Module_429 --><div id="dnn_ctr429_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr429_HtmlModule_lblContent"><a target="_blank" href="http://managementconcepts.com/Domain/id/Federal%20Acquisition%20-%20Contracting"><span style="font-size: 11px;">Acquisition &amp; Contracting</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Analytics"><span style="font-size: 11px;">Analytics</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/tabid/326/id/Federal%20Financial%20Management/Default.aspx"><span style="font-size: 11px;">Financial Management</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Federal%20Grants%20-%20Assistance"><span style="font-size: 11px;">Grants &amp; Assistance</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Federal%20Human%20Capital%20and%20Human%20Resources"><span style="font-size: 11px;">Human Capital &amp; Human Resources</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Leadership%20-%20Management"><span style="font-size: 11px;">Leadership &amp; Management</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Project%20-%20Program%20Management"><span style="font-size: 11px;">Project &amp; Program Management</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Training/CertificatePrograms.aspx"><span style="font-size: 11px;">Certificate Programs</span></a><br></div>




</div><!-- End_Module_429 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
          <div id="dnn_FooterGrid3C" class="footer_grid3c col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-4327 DnnVersionableControl"><a name="4327"></a>

<div class="Footer01_style">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr4327_dnnTITLE_titleLabel" class="c_title_footer">CONSULTING AREAS</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr4327_ContentPane" class="Footer01_content"><!-- Start_Module_4327 --><div id="dnn_ctr4327_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr4327_HtmlModule_lblContent"><a target="_blank" href="http://managementconcepts.com/Consulting/Organizational-Culture-Alignment"><span style="font-size: 11px;">Organizational Culture Alignment</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Consulting/Organizational-Change-Management"><span style="font-size: 11px;">Organizational Change Management<br>
</span></a>
<a target="_blank" href="http://managementconcepts.com/Consulting/Human-Capital-Management"><span style="font-size: 11px;">Human Capital Management</span><br>
</a><a target="_blank" href="http://managementconcepts.com/Consulting/Performance-Improvement"><span style="font-size: 11px;">Performance Improvement</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Consulting/Learning-and-Development"><span style="font-size: 11px;">Learning &amp; Development</span></a><br></div>




</div><!-- End_Module_4327 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
          <div id="dnn_FooterGrid3D" class="footer_grid3d col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-4331 DnnVersionableControl"><a name="4331"></a>

<div class="Footer01_style" style="width: 150px;">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr4331_dnnTITLE_titleLabel" class="c_title_footer">POPULAR LINKS</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr4331_ContentPane" class="Footer01_content"><!-- Start_Module_4331 --><div id="dnn_ctr4331_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr4331_HtmlModule_lblContent"><a target="_blank" href="http://managementconcepts.com/FormsCollection/RequestaCatalog"><span style="font-size: 11px;">Request a Catalog</span></a><br>
<a target="_blank"  href="http://managementconcepts.com/HowtoBuyfromUs.aspx"><span style="font-size: 11px;">How to Buy from Us<br>
</span></a>
<a target="_blank" href="http://managementconcepts.com/Resources/DownloadFreeWhitePapers.aspx"><span style="font-size: 11px;">Free Resources</span></a><br>
<a target="_blank" href="https://managementconcepts.csod.com"><span style="font-size: 11px;">Student Central</span></a><br>
<a target="_blank" href="http://managementconcepts.com/AboutUs/Careers.aspx"><span style="font-size: 11px;">Careers</span></a><br>
<a target="_blank" href="http://managementconcepts.com/ContactUs/ClassroomRentalFacilities.aspx"><span style="font-size: 11px;">Classroom Rental</span></a><br>
<a target="_blank" href="http://www.managementconcepts.com/electroniccoursematerials#FAQ"><span style="font-size: 11px;">FAQ </span></a></div>




</div><!-- End_Module_4331 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
	 </div>
        <div class="row dnnpane">
          <div id="dnn_FooterPane" class="footerpane col-md-12 DNNEmptyPane"></div>
        </div>
      </div>



<div class="copyright_bar clearafter clearfix" style="border-top: 2px groove">
        <div class="footer_left">
          <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="Footer">Copyright © 2016 Management Concepts </span>

          |
  
	 <a target="_blank" href="http://managementconcepts.com/Contact-Us/Terms-and-Conditions" class="Footer">Terms and Conditions </a>
          |
	 <a target="_blank" href="http://managementconcepts.com/Privacy-Policy" class="Footer">Privacy Policy</a>
        
        </div>
        <div class="socialpane_style">
          <div id="dnn_Socialpane" class="socialpane">
	  
		<div class="Social-Links"> 
		     <a class="iconFacebook" title="Facebook" href="http://www.facebook.com/managementconcepts"></a>
		     <a class="iconTwitter" title="Twitter" href="http://twitter.com/#!/mgmt_concepts"></a>
		     <a class="iconLinkedIn" title="LinkedIn" href="http://www.linkedin.com/company/management-concepts"></a>
		     <a class="iconYouTube" title="YouTube" href="http://www.youtube.com/mgmtconcepts"></a>
		</div> 
	  
	  </div>
        </div>
       </div>    </div>
  </div>

            </div>
</body>
</html>
