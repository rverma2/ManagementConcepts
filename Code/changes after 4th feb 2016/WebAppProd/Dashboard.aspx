﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="WebApp.Dashboard" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="html">
<head runat="server">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
    <title>Home</title>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/site_global.css?3983832653" />
    <link rel="stylesheet" type="text/css" href="css/index.css?316465036" id="pagesheet" />
    <link href="Style/bootstrap.css" rel="stylesheet" />
    <link href="Style/foo.css" rel="stylesheet" />
    <link href="Style/font-awesome.css" rel="stylesheet" />
    <link href="css/poposlides.css" rel="stylesheet" />

     <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300italic,300,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        #divImage img {
            /*padding: 6px;
            font-weight:bolder;
            border:1px;*/
            height:30px;
            float:right;
            cursor:pointer;
        }

        .modal {
            display: none;
            position: absolute;
            top: 0px;
            left: 0px;
            background-color: black;
            z-index: 100;
            opacity: 0.8;
            filter: alpha(opacity=60);
            -moz-opacity: 0.8;
            min-height: 100%;
        }

        #divImage {
            display: none;
            z-index: 1000;
            position: fixed;
            top: 0;
            left: 0;
            background-color: White;
            height: 550px;
            width: 600px;
            padding: 3px;
            border: solid 1px black;
        }
    </style>

    <style>
        .ZoomICon {
            background-image: url("Images/icon/zoom-icon.gif");
            background-repeat: no-repeat;
            height: 24px;
            margin-left: 40px;
            margin-top: 10px;
            position: absolute;
            width: 24px;
            cursor: pointer;
        }
    </style>

    <style>
        .html {
            background-color: transparent;
        }
        .footer_top {
            background: #3c3c3c;
        }

    .skin_width {
    width: 100%;
    max-width: 1140px;
    min-width: 767px;
    margin: 0 auto;
    position: relative;
}


    .copyright_bar {
    border-top: 1px solid #222;
    color: #666;
}

    #footer p {
        font-family: "Droid Sans",Arial,sans-serif;
        line-height: 17px;
        margin: 0 0 6px;
    }
    #footer{
        font-family: "Droid Sans",Arial,sans-serif;
    }
    .c_title_footer {
        font-family: "Titillium Web",Arial,Tahoma,sans-serif;
        font-size: 20px !important;
        font-weight: 600;
    }
    .Footer {
        font-family: "Droid Sans",Arial,sans-serif !important;
    }
    </style>

    <script type="text/javascript">

        function LoadDiv(url) {
            var img = new Image();
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            var imgLoader = document.getElementById("imgLoader");
            imgLoader.style.display = "block";
            img.onload = function () {
                imgFull.src = img.src;
                imgFull.style.display = "block";
                imgLoader.style.display = "none";
            };
            img.src = url;
            var width = document.body.clientWidth;
            if (document.body.clientHeight > document.body.scrollHeight) {
                bcgDiv.style.height = document.body.clientHeight + "px";
            }
            else {
                bcgDiv.style.height = document.body.scrollHeight + "px";
            }
            imgDiv.style.left = (width - 650) / 2 + "px";
            imgDiv.style.top = "20px";
            bcgDiv.style.width = "100%";

            bcgDiv.style.display = "block";
            imgDiv.style.display = "block";
            return false;
        }
        function HideDiv() {
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            if (bcgDiv != null) {
                bcgDiv.style.display = "none";
                imgDiv.style.display = "none";
                imgFull.style.display = "none";
            }
        }

        $(document).ready(function () {
            $(".ZoomICon").click(function () {
                var imgUrl = $(this).siblings().find('div a img').attr('src');
                LoadDiv(imgUrl);
                //alert($(this).siblings().find('div a img').attr('src'));
            });
            $("#lnkbtnCourseName > img").hover(function (e) {
                //alert($(this).parent().parent().html());
                //LoadDiv($(this).attr('src'));
                //$(this).toggleClass("EnlargeImage");
                //e.stopPropagation();
            });
            $("#btnUseMe").click(function (e) {
                ShowUseMeDialog();
                e.preventDefault();
            });
            $("#btnUseMeDialogClose").click(function (e) {
                $("#popupVideoPlayer")[0].pause();
                HideUseMeDialog();
                e.preventDefault();
               
            });
            $("#btnCloseChngPwd").click(function (e) {
                HideDialog();
                e.preventDefault();
            });
            $("#brands").attr("checked", false);
            $("#btnAccount").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });
            $("#btnHelp").click(function (e) {
                ShowGenericDialog(true);
                e.preventDefault();
            });
            $("#btnClose").click(function (e) {
                HideDialog();
                e.preventDefault();
            });
            $("#btnDialogClose").click(function (e) {
                HideGenericDialog();
                e.preventDefault();
            });

            $("#btnSubmit").click(function (e) {
                if ($("#brands").find("input:checked").length > 0) {
                    var brand = $("#brands input:radio:checked").val();
                    $("#output").html("<b>Your Country Is : </b>" + brand);
                    alert(" Your Country Is " + brand);
                    HideDialog();
                    e.preventDefault();
                }
                else {
                    alert("Please select Radio Button")
                    ShowDialog(true);
                    e.preventDefault();
                }
            });
            $("#btnChngPwd1").click(function (e) {
                if ($('#newPwdTxt').val() == '') {
                    alert('Enter the New password')
                    ShowDialog(true);
                    e.preventDefault();

                }
                if ($('#confirmPwdTxt').val() == '') {
                    alert('Enter the Confirm password')
                    ShowDialog(true);
                    e.preventDefault();

                }
                if ($('#newPwdTxt').val() != $('#confirmPwdTxt').val()) {
                    alert('Passwords are not matched.')
                    ShowDialog(true);
                    e.preventDefault();

                }

            });
        });
        function ShowDialog(modal) {
            $("#overlay").show();
            $("#dialog").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }

        function HideDialog() {
            $("#overlay").hide();
            $("#dialog").fadeOut(300);
        }
        function ShowGenericDialog(modal) {
            $("#overlay").show();
            $("#genericDialog").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideGenericDialog() {
            $("#overlay").hide();
            $("#genericDialog").fadeOut(300);
        }

        function ShowUseMeDialog(modal) {
            $("#overlay").show();
            $("#useMeDialog").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }

        function HideUseMeDialog() {
            $("#overlay").hide();
            $("#useMeDialog").fadeOut(300);
        }

    </script>

    <!-- Other scripts -->
    <script type="text/javascript">
        document.documentElement.className += ' js';
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <%-- <asp:Button Text="Modal Dialog" runat="server" ID="btnShowModal" />  
    <asp:Button Text="Sample Dialog" runat="server"  ID="btnShowSimple" Visible="false"/>  
    <br />  
    <br />  --%>
        <div id="output"></div>
       






        <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://cdn.jsdelivr.net/jcarousel/0.2.8/jquery.jcarousel.min.js"></script>
        <link href="http://cdn.jsdelivr.net/jcarousel/0.2.8/skins/tango/skin.css" rel="Stylesheet" />  --%>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.js"></script>
        <script src="js/poposlides.js"></script>
        <%-- <script type="text/javascript" src="https://raw.githubusercontent.com/gilbitron/Nivo-Slider/master/jquery.nivo.slider.pack.js"></script>--%>
        <script type="text/javascript">
            $(document).ready(function () {
                //$('#slider').nivoSlider({
                //    pauseTime: 2000,
                //    effect: 'fold', //Specify sets like: 'fold,fade,sliceDown'
                //    slices: 1
                //});
                $(".slides").poposlides();
            });
        </script>
        <div class="clearfix" id="page">
            <!-- column -->
            <div class="position_content" id="page_position_content">
                <div class="gradient clearfix colelem" id="u69">
                    <!-- group -->
                    <%-- <div class="clip_frame grpelem" id="u71"><!-- image -->    
      <img class="block" id="u71_img" src="images/logo.jpg" alt="" width="251" height="83"/>
     </div>--%>
                    <%--<div class="clip_frame grpelem" id="u182"><!-- image -->
           <table style="width: 100%">
            <tr>
              
                <td style="float: right; width:100%">
                    <div style="float: right;">
                        <asp:Label ID="lblUserName" ForeColor="Blue" runat="server"></asp:Label>                        | 
                      <asp:Label ID="lblaccount" Text="Account" ForeColor="Gray" runat="server"></asp:Label>|
                       <asp:LinkButton ID="lnkbtnLogOut" runat="server" Text="Logout" ForeColor="Gray" OnClick="lnkbtnLogOut_Click"></asp:LinkButton>
                    </div>
                </td>
            </tr>
           </table>

        <div style="width: 100%; height: 36px; text-align: right; border-bottom: 1px solid #cecece; background-color: #f8f8f8; padding-top: 13px;">
            <asp:LinkButton ID="lbfeedback" runat="server" Text="Feedback" ForeColor="Maroon" />
            <asp:LinkButton ID="lbhow" runat="server" Text="How to" ForeColor="Maroon" />
            <asp:LinkButton ID="lbHelp" runat="server" Text="Help" ForeColor="Maroon" />
        </div>
     
     </div>--%>
                    <div role="navigation" class="header navbar navbar-default" id="mainHeader" style="width:auto; padding-right:10px;">
                    
                        <div class="navbar-header">
                            <div class="white_patch" id="white_patch" style="display: none;"></div>
                            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                            </button>
                            <a dontchange="true" href="#" class="navbar-brand">
                                <img src="images/logo.jpg" id="logo"  style="top: 18px;"></a>
                        </div>

                        <div class="nav_menu navbar-collapse collapse" id="menuBar">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="welcome_text"><span><a id="welcomeUserTxt" target="_blank">
                                    <asp:Label ID="lblUserName" ForeColor="#00688f" runat="server"></asp:Label>
                                </a></span>
                                    <span><a class="onlineIndicator online" id="onlineIndicator" target="_blank"></a></span></li>
                                <li><a id="btnAccount" target="_blank">Account</a></li>
                                <li><a id="btnLogout" href="Login.aspx" target="_parent">Logout</a></li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->

                       
                        <div class="navSub_menu navbar-collapse collapse" id="subMenuBar" style="clear:both">
                            <ul class="nav navbar-nav navbar-right">
<%--                                <li><a target="_blank" href="http://www.managementconcepts.com/Contact-Us/Customer-Service-Center">Feedback</a></li>
                                <li><a id="btnUseMe" target="_blank">How to</a></li>
                                <li><a id="btnHelp" target="_blank">Help</a></li>--%>

                                <li>  
                                <%-- <a style="text-decoration:none;" href="mailto:info@ManagementConcepts.com">Contact Us</a>--%>

                                 <a style="text-decoration:none;" href="mailto:studentsupport@managementconcepts.com?subject=Electronic%20Materials%20Support">Contact Us</a>
                                 </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
                <div class="clearfix colelem" id="u131-4">
                    <!-- content -->
                    <p id="u131-2">Course Materials</p>
                </div>
                <div class="clearfix colelem" id="pu132-4">
                    <!-- group -->
                    <div class="clearfix grpelem" id="u132-4">
                        <!-- content -->
                        <p id="u132-2">Course Materials</p>
                    </div>

                    <div class="clip_frame grpelem" id="u133">
                        <!-- image -->
                        <div class="s1">
                            <div id="slider" class="slides-box">
                                <ul class="slides">
                                    <asp:Repeater ID="rptSlider" runat="server">
                                        <ItemTemplate>
                                            <li>
                                               <%-- <asp:Image ID="imgUrl" ImageUrl='<%# Eval("ImageUrl") %>' runat="server" />--%>


                                               <img class="block" alt="" src="images/eBookBanner.jpg" />
                                                
                                                </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>

                    </div>
                   <div class="clip_frame grpelem" id="u138">
                        <a id="registerURL" target="_blank" href="http://www.managementconcepts.com/electroniccoursematerials#FAQ">
                            <img class="block" alt="" src="images/side-butt1.jpg" />
                        </a>
                    </div>
                    <div class="clip_frame grpelem" id="u143">
                        <a id="publicationURL" target="_blank" href="http://www.managementconcepts.com">
                            <img class="block" alt="" src="images/side-butt2.jpg" />
                        </a>
                    </div>
                    <div class="clip_frame grpelem" id="u148">
                        
                        <a id="btnUseMe" target="_blank" href="">
                            <img class="block" alt="" src="images/side-butt3.jpg" />
                        </a>
                    </div>
                </div>
                <%-- <div class="clip_frame colelem" id="u167">
                    <!-- image -->
                    <img class="block" id="u167_img" src="images/bar1.jpg" alt="" width="1140" height="62" />
                </div>--%>
                <div class="lib_head">
                    <h3 class="pull-left">My Library</h3>
                    <div class="lib_rt">
                        <div class="sort pull-left">
                            <span class="custom-dropdown">
                                <select class="searchTypeDropDown" id="searchTypeDropDown">
                                    <option id="titleType" selected="selected">Search by Title</option>
                                    <option id="authorType">Search by Author</option>
                                </select>
                            </span>
                        </div>
                        <div class="search pull-left">
                            <div id="tfheader">
                                <div id="tfnewsearch">
                                    <input type="text" placeholder="Search" maxlength="120" size="21" name="q" class="tftextinput4" id="searchText">

                                    <input type="submit" class="tfbutton4" value=" " id="btnSearch">

                                    <div class="btnResetShelfSerarch" id="btnResetShelfSerarch">Reset</div>
                                    <div class="tfclear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clip_frame colelem" id="u172">
                    <!-- image -->

                    <div style="clear: both; background: url(../Images/book_bg.jpg); height: auto; padding-top: 5px;">
                        <div style="clear: both; text-align: center;">
                            <asp:DataList ID="ddlDashboard" ClientIDMode="Static" runat="server" RepeatColumns="5" OnItemDataBound="ddlDashboard_ItemDataBound">

                                <ItemTemplate>
                                    <div class="ZoomICon"></div>
                                    <div style="width: auto;">
                                        <div style="clear: both; width: auto">

                                            <asp:HyperLink ID="lnkbtnCourseName" runat="server" Text="View" ImageUrl='<%#Bind("bookimage") %>' Target="_parent"></asp:HyperLink>
                                        </div>
                                        <div style="width: 200px; height: auto; clear: both; margin-top: 10px; margin-left: 10px; text-align: center;">
                                            <asp:Label ID="lblClassId" runat="server" Visible="false" Text='<%#Bind("ClassId") %>'></asp:Label>
                                            <asp:Label ID="lblJulianDate" runat="server" Visible="false" Text='<%#Bind("JulianDate") %>'></asp:Label>
                                            <asp:Label ID="lblCourseName" runat="server" ForeColor="#000"></asp:Label>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div class="ZoomICon"></div>
                                    <div style="width: auto;">
                                        <div style="clear: both; width: auto;">
                                            <asp:HyperLink ID="lnkbtnCourseName" runat="server" Text="View" ImageUrl='<%#Bind("bookimage") %>' Target="_parent"></asp:HyperLink>
                                        </div>
                                        <div style="width: 200px; height: auto; clear: both; margin-top: 10px; margin-left: 10px; text-align: center;">
                                            <asp:Label ID="lblClassId" runat="server" Visible="false" Text='<%#Bind("ClassId") %>'></asp:Label>
                                            <asp:Label ID="lblJulianDate" runat="server" Visible="false" Text='<%#Bind("JulianDate") %>'></asp:Label>
                                            <asp:Label ID="lblCourseName" runat="server" ForeColor="#000"></asp:Label>
                                        </div>
                                    </div>

                                </AlternatingItemTemplate>
                                <%--<SeparatorTemplate><div style="width:100%; border-bottom:1px solid #cecece;"></div></SeparatorTemplate>--%>
                                <%--  <div style="border-bottom:1px solid #cecece; width:100%; height:auto;"></div>--%>
                            </asp:DataList>
                        </div>
                    </div>
                    <%-- <img class="block" id="u172_img" src="images/bar2.jpg" alt="" width="1140" height="191"/>--%>
                </div>
<%--                <div id="footer" class="footer">
                    <div class="pageNumbersContainer" id="pageNumbersContainer" style="display: none;">
                        <span class="pageNumberView" id="pageNumberingView"></span>
                    </div>
                    <div class="clearafter">

                        <div class="footer_container" id="footer_container">

                            <div id="footerpane" class="footerpane_style" style="display: block;">
                                <div class="col-xs-12 col-sm-4">
                                    <h2>CONTACT US</h2>
                                    <span class="contact_us">Management Concepts is the
							nation's premier provider of training and professinal development
							solutions serving the public and private sectors.</span> <span class="contact_us">8230 LeesburgPike, Tysons Corner, VA
							22182</span> <span class="contact_us">888.545.8571</span> <a href="mailto:info@ManagementConcepts.com" target="_blank"><span>Info@ManagementConcepts.com</span></a>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <h2>TRAINING BY TOPIC</h2>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Domain/id/Federal%20Acquisition%20-%20Contracting" target="_blank">
                                            <span>FederalAcquisition &amp; Contracting</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Domain/id/Federal%20Financial%20Management" target="_blank">
                                            <span>Federal Financial Management</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Domain/id/Leadership%20-%20Management" target="_blank">
                                            <span>Leadership &amp; Management</span>
                                        </a>/
					
                                    </p>
                                    <p>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Domain/id/Federal%20Grants%20-%20Assistance" target="_blank">
                                            <span>Federal Grants &amp; Assistance</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Domain/id/Federal%20Project%20-%20Program%20Management" target="_blank">
                                            <span>Federal Project &amp; Program Management</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Domain/id/Federal%20Human%20Capital%20and%20Human%20Resources" target="_blank">
                                            <span>Federal Human Capital &amp; Human Resources</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/CertificatePrograms" target="_blank">
                                            <span>Certificate Programs</span>
                                        </a>
                                    </p>
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                    <h2>POPULAR LINKS</h2>
                                    <p>
                                        <a href="http://www.managementconcepts.com/FormsCollection/RequestaCatalog" target="_blank">
                                            <span>Request a Catalog</span>
                                        </a>/
					
                                    </p>
                                    <p>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/How-to-Buy-from-Us" target="_blank">
                                            <span>How to Buy from Us</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Forms-Collection/Grants-ENewsletter-Signup" target="_blank">
                                            <span>Grants E-Newsletter</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Resources/Download-Free-White-Papers" target="_blank">
                                            <span>Request a Whitepaper</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://ecampus.managementconcepts.com" target="_blank"><span>eCampus</span></a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/About-Us/Careers" target="_blank"><span>Careers</span></a>/
					
                                    </p>
                                    <p>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Contact-Us/Classroom-Rental-Facilities" target="_blank">
                                            <span>Classroom Rental</span>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="http://www.managementconcepts.com/Contact-Us/Frequently-Asked-Questions" target="_blank">
                                            <span>FAQ</span>
                                        </a>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="footerTop_border">
                                <div class="footer_left copyright_bar">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="Footer">Copyright
							&copy; 2015 Management Concepts</span> | <a href="http://www.managementconcepts.com/Contact-Us/Terms-and-Conditions" class="Footer" target="_blank">Terms
							and Conditions </a>| <a href="http://www.managementconcepts.com/Privacy-Policy" class="Footer" target="_blank">Privacy
							Policy</a>
                                </div>


                                <div class="socialpane_style">
                                    <div class="Social-Links">
                                        <a class="iconFacebook" title="Facebook" href="http://www.facebook.com/managementconcepts" target="_blank"></a><a class="iconTwitter" title="Twitter" href="http://twitter.com/#!/mgmt_concepts" target="_blank"></a><a class="iconLinkedIn" title="LinkedIn" href="http://www.linkedin.com/company/management-concepts" target="_blank"></a><a class="iconYouTube" title="YouTube" href="http://www.youtube.com/mgmtconcepts" target="_blank"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <img style="display: none;" id="ImgforCanvas" />
                    <canvas style="display: none;" width="662" height="857" id="imageCanvas"></canvas>




                </div>--%>


                     <div id="footer" class="footer">
<div class="footer_top">
    <div class="skin_width">
		<div style="border-bottom:2px groove;padding-top:20px;text-align:center;" class="dnnpane tagline">
          <img src="images/footer-tagline.png">
        </div>
		
      <div class="footerpane_style skin_main_padding">
        <div class="row dnnpane">
          <div id="dnn_FooterGrid3A" class="footer_grid3a col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-1423 DnnVersionableControl"><a name="1423"></a>

<div class="Footer01_style" style="width: 248px;">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr1423_dnnTITLE_titleLabel" class="c_title_footer">CONTACT US</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr1423_ContentPane" class="Footer01_content"><!-- Start_Module_1423 --><div id="dnn_ctr1423_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr1423_HtmlModule_lblContent"><span style="font-size: 11px; color: #ffffff;">
<p>Management Concepts is the nation’s premier provider of training and professional development solutions serving the
public and private sectors.</p>
<p><i style="font-size: 13px; color: #dea310; margin-right:6px" aria-hidden="true" class="fa fa-map-marker"></i>&nbsp;8230 Leesburg Pike, Tysons Corner, VA 22182&nbsp;</p>
<p><i style="font-size: 16px; color: #dea310; margin-right:9px"  aria-hidden="true" class="fa fa-mobile-phone"></i>&nbsp;888.545.8571 &nbsp;</p>
<p><span style="font-size: 14px; color: #dea310;" class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<a href="mailto:studentsupport@managementconcepts.com?subject=Electronic%20Materials%20Support"><span style="font-size: 11px; color: #ffffff;">studentsupport@managementconcepts.com</span></a> </p>
</span></div>




</div><!-- End_Module_1423 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
          <div id="dnn_FooterGrid3B" class="footer_grid3b col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-429 DnnVersionableControl"><a name="429"></a>

<div class="Footer01_style">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr429_dnnTITLE_titleLabel" class="c_title_footer">TRAINING BY TOPIC</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr429_ContentPane" class="Footer01_content"><!-- Start_Module_429 --><div id="dnn_ctr429_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr429_HtmlModule_lblContent"><a target="_blank" href="http://managementconcepts.com/Domain/id/Federal%20Acquisition%20-%20Contracting"><span style="font-size: 11px;">Acquisition &amp; Contracting</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Analytics"><span style="font-size: 11px;">Analytics</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/tabid/326/id/Federal%20Financial%20Management/Default.aspx"><span style="font-size: 11px;">Financial Management</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Federal%20Grants%20-%20Assistance"><span style="font-size: 11px;">Grants &amp; Assistance</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Federal%20Human%20Capital%20and%20Human%20Resources"><span style="font-size: 11px;">Human Capital &amp; Human Resources</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Leadership%20-%20Management"><span style="font-size: 11px;">Leadership &amp; Management</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Domain/id/Project%20-%20Program%20Management"><span style="font-size: 11px;">Project &amp; Program Management</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Training/CertificatePrograms.aspx"><span style="font-size: 11px;">Certificate Programs</span></a><br></div>




</div><!-- End_Module_429 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
          <div id="dnn_FooterGrid3C" class="footer_grid3c col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-4327 DnnVersionableControl"><a name="4327"></a>

<div class="Footer01_style">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr4327_dnnTITLE_titleLabel" class="c_title_footer">CONSULTING AREAS</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr4327_ContentPane" class="Footer01_content"><!-- Start_Module_4327 --><div id="dnn_ctr4327_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr4327_HtmlModule_lblContent"><a target="_blank" href="http://managementconcepts.com/Consulting/Organizational-Culture-Alignment"><span style="font-size: 11px;">Organizational Culture Alignment</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Consulting/Organizational-Change-Management"><span style="font-size: 11px;">Organizational Change Management<br>
</span></a>
<a target="_blank" href="http://managementconcepts.com/Consulting/Human-Capital-Management"><span style="font-size: 11px;">Human Capital Management</span><br>
</a><a target="_blank" href="http://managementconcepts.com/Consulting/Performance-Improvement"><span style="font-size: 11px;">Performance Improvement</span></a><br>
<a target="_blank" href="http://managementconcepts.com/Consulting/Learning-and-Development"><span style="font-size: 11px;">Learning &amp; Development</span></a><br></div>




</div><!-- End_Module_4327 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
          <div id="dnn_FooterGrid3D" class="footer_grid3d col-sm-3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-4331 DnnVersionableControl"><a name="4331"></a>

<div class="Footer01_style" style="width: 150px;">
  <div class="Footer01_top_bg">
    <div class="c_icon">
      
	</div>
    <div class="c_title">
      <h2 class="c_titles"><span id="dnn_ctr4331_dnnTITLE_titleLabel" class="c_title_footer">POPULAR LINKS</span>


</h2>
	</div>
    <div class="Footer01_c_help">
	  
	</div>
	<div class="clear_float"></div>
  </div>
  <div class="c_content_style">
    <div id="dnn_ctr4331_ContentPane" class="Footer01_content"><!-- Start_Module_4331 --><div id="dnn_ctr4331_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr4331_HtmlModule_lblContent"><a target="_blank" href="http://managementconcepts.com/FormsCollection/RequestaCatalog"><span style="font-size: 11px;">Request a Catalog</span></a><br>
<a target="_blank"  href="http://managementconcepts.com/HowtoBuyfromUs.aspx"><span style="font-size: 11px;">How to Buy from Us<br>
</span></a>
<a target="_blank" href="http://managementconcepts.com/Resources/DownloadFreeWhitePapers.aspx"><span style="font-size: 11px;">Free Resources</span></a><br>
<a target="_blank" href="https://managementconcepts.csod.com"><span style="font-size: 11px;">Student Central</span></a><br>
<a target="_blank" href="http://managementconcepts.com/AboutUs/Careers.aspx"><span style="font-size: 11px;">Careers</span></a><br>
<a target="_blank" href="http://managementconcepts.com/ContactUs/ClassroomRentalFacilities.aspx"><span style="font-size: 11px;">Classroom Rental</span></a><br>
<a target="_blank" href="http://www.managementconcepts.com/electroniccoursematerials#FAQ"><span style="font-size: 11px;">FAQ </span></a></div>




</div><!-- End_Module_4331 --></div>
  </div>
  <div class="c_footer">
    <div class="c_footer_l">
      
    </div>
    <div class="c_footer_r">
      
      
      
    </div>
	<div class="clear_float"></div>
  </div>
  <div class="Footer01_footer"></div>
</div></div></div>
	 </div>
        <div class="row dnnpane">
          <div id="dnn_FooterPane" class="footerpane col-md-12 DNNEmptyPane"></div>
        </div>
      </div>



<div class="copyright_bar clearafter clearfix" style="border-top: 2px groove">
        <div class="footer_left">
          <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="Footer">Copyright © 2016 Management Concepts </span>

          |
  
	 <a target="_blank" href="http://managementconcepts.com/Contact-Us/Terms-and-Conditions" class="Footer">Terms and Conditions </a>
          |
	 <a target="_blank" href="http://managementconcepts.com/Privacy-Policy" class="Footer">Privacy Policy</a>
        
        </div>
        <div class="socialpane_style">
          <div id="dnn_Socialpane" class="socialpane">
	  
		<div class="Social-Links"> 
		     <a class="iconFacebook" title="Facebook" href="http://www.facebook.com/managementconcepts"></a>
		     <a class="iconTwitter" title="Twitter" href="http://twitter.com/#!/mgmt_concepts"></a>
		     <a class="iconLinkedIn" title="LinkedIn" href="http://www.linkedin.com/company/management-concepts"></a>
		     <a class="iconYouTube" title="YouTube" href="http://www.youtube.com/mgmtconcepts"></a>
		</div> 
	  
	  </div>
        </div>
       </div>    </div>
  </div>

            </div>

                <div id="divBackground" class="modal">
                </div>
                <div id="divImage">
                    <table style="height: 100%; width: 100%">
                        <tr>
                            <td align="center" valign="bottom">
                                <%--<input id="btnClose" type="button" value="Close" onclick="HideDiv()" />--%>
                                <img id="btnClose" onclick="HideDiv()" src="Images/icon/button-cross.png"
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center">
                                <img id="imgLoader" alt="" src="images/loader.gif" />
                                <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                            </td>
                        </tr>

                    </table>
                </div>

                 <div id="overlay" class="web_dialog_overlay"></div>

        <div visibility="hidden" class="zoomCovePageDialog web_dialog" id="dialog" style="margin-top: -220px; display: none;">
            <div class="zoom_title">
                <h5>Change Password</h5>
            </div>
            <div class="inner_space" id="changePwdContainer" style="height: 300px;">



                <div class="password_box">

                    <form class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">
                                Old
							Password</label>
                            <div class="col-sm-9">
                                <input type="password" autofocus="" required="" placeholder="Old Password" id="oldPwdTxt" name="oldPwdTxt" class="form-control" runat="server">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputPassword3">
                                New
							Password</label>
                            <div class="col-sm-9">
                                <input type="password" placeholder="New Password" id="newPwdTxt" name="newPwdTxt" class="form-control" runat="server">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputPassword3">
                                Confirm
							Password</label>
                            <div class="col-sm-9">
                                <input type="password" placeholder="Confirm Password" id="confirmPwdTxt" name="confirmPwdTxt" class="form-control" runat="server">
                            </div>
                        </div>

                        <div class="form-group col-sm-12 forgotPassword">
                            <button class="btn btn-default pass_cancel pull-right" id="btnCloseChngPwd" type="button" style="display: block;" runat="server" onclick="btnCloseChngPwd_Click">Close</button>
                            <%--<button class="btn btn-default pull-right pass_submit" runat="server" id="btnChngPwd" type="button" onclick="btnChngPwd_Click">Submit</button>--%>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnChngPwd1" EventName="Click" />
                                            </Triggers>
                                            <ContentTemplate>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                            <asp:Button ID="btnChngPwd1" runat="server" Text="Submit" CssClass="btn btn-default pass_cancel pull-right" OnClick="btnChngPwd1_Click" />

                        </div>

                        <div class="clear"></div>
                    </form>
                </div>



            </div>
        </div>

        <div visibility="hidden" class="zoomCovePageDialog" id="genericDialog" style="display: none; margin-top: -220px;">
            <div class="zoom_title">
                <h5 id="titleOfDialog">Help</h5>
                <div id="btnDialogClose" class="zoom_close">
                    x
			
                <!-- <img src="assets/images/helpPopupCloseBtn.png"> -->
                </div>
            </div>
            <div class="inner_space" id="dialogMainContainer" style="text-align: left; height: 300px;">
                <!--?xml version="1.0" encoding="UTF-8"?-->



                <title>Help</title>
                <meta content="width=device-width, initial-scale=1.0" name="viewport">

                <style>
                    p.extract {
                        font-size: 1.1em;
                        font-family: arial;
                        text-align: left;
                        text-indent: 0em;
                        margin-bottom: 0em;
                        margin-top: 1em;
                    }

                    span.gray {
                        color: #444444;
                        font-family: arial;
                    }

                    p.hanging {
                        font-size: 1.1em;
                        text-indent: -1.8em;
                        margin-left: 3em;
                        margin-top: 0.3em;
                        margin-bottom: 0em;
                        text-align: left;
                    }

                    p.hanging1 {
                        font-size: 1.1em;
                        text-indent: -1.5em;
                        margin-left: 6em;
                        margin-top: 0.4em;
                        margin-bottom: 0em;
                        text-align: left;
                    }

                    span.big {
                        font-size: 2.2em;
                    }

                    p.center {
                        margin-top: 1em;
                        text-align: center;
                        margin-bottom: 1em;
                    }
                </style>




                <p class="extract">
                    <strong>What are electronic course materials?</strong><br>
                    <br>
                    Electronic course materials are digitally-enhanced versions of traditional print course materials that can be downloaded on a computer, desktop, laptop, or tablet.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Do I need to have a technical background to take a course with electronic course materials?</strong><br>
                    <br>
                    No. Your materials will be accessed through an electronic bookshelf that’s designed to be intuitive and easy to operate. Additionally, you have access to a video demonstration that walks you through the basic functionsaswell as the Help section, both located at  <a href="http://coursematerials.managementconcepts.com" target="_blank">http://coursematerials.managementconcepts.com</a>.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Do I still have to come to the class?</strong><br>
                    <br>
                    Yes. The bookshelf provides access to electronic course materials used in class for instructional purposes. It is not a replacement for attendance in the course itself.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>When and how will I gain access to the electronic course materials?</strong><br>
                    <br/>
                    You will receive a welcome emailfrom studentsupport@managementconcepts.comwith login credentials one week prior to the class start date. (Please check your Spam folder if you do not see the email at this time). The electronic course materials will be waiting on your bookshelf when you log infor the first time.
                    <br />
                    To log in, follow the instructions below:<br />
                    <ul>
                    <li>•	Access the course materials on your bookshelf at <a href="http://coursematerials.managementconcepts.com" target="_blank">http://coursematerials.managementconcepts.com</a> or by clicking the “Log On” link in thewelcome email (sent one week prior to class from <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a>. </li>
                    <li>•	Enter your email address into the User Name field and use the password provided in the welcome email</li>
                    <li>•	Agree to the Terms of Use</li>
                    <li>•	Create a new password</li>
                    </ul>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What should I do if no books are available on my bookshelf?</strong><br>
                    <br>
                    If there are no books on your bookshelf please contact Student Support toll-free at <b>844.876.7476</b> or email Student Support at <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a> for assistance.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What device do I bring to class to access my electronic course materials?</strong><br>
                    <br>
                    Management Concepts will supply you with a laptop to use during your class, so you will not need to bring a device to access your electronic course materials. 
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What do I need to do to prepare for my class?</strong><br>
                    <br>
                    If you will be using a laptop or desktop at home to access your electronic course materials outside of class, you will need to verify your browser by clicking <a href="http://www.managementconcepts.com/Portals/0/Files/BrowserCheck.pdf" target="_blank">here</a>. You also need to test your operating system as well as the access to your bookshelf by following the instructions below. 
                    <br />
                    For optimum performance of electronic course materials, please use the most current version of either of these browsers on your laptop: 
                    <br />
                    <ul>
                    <li>o	Windows: Internet Explorer 9, Google Chrome 26.0+, and Mozilla Firefox 20.0+</li>
                    <li>o	Mac: Safari 5.1+</li>
                    </ul>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What features are currently available on the electronic course materials bookshelf?</strong><br>
                    <br>
                    On your bookshelf you can search by title, author, or topic. You can view the bookshelf like a traditional bookshelf, or switch to a list view. Within the materials, you can annotate, take notes, bookmark pages for quick access, highlight, and search by keyword. Notes and bookmarks can be accessed from any device with an internet connection and a browser.
                </p>
                <br>
                 <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What should I do if no books are available on my bookshelf?</strong><br>
                    <br>
                    If there are no books on your bookshelf please contact Student Support toll-free at <b>844.876.7476</b> or email Student Support at <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a> for assistance.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What if I open my bookshelf and the wrong book is there?</strong><br>
                    <br>
                    Please contact Student Support toll-free at <b>844.876.7476</b> or email Student Support at student <a href="mailto:support@managementconcepts.com" target="_blank">support@managementconcepts.com</a> for help with your bookshelf titles.                    
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>I’m in the middle of reading a book for a class, how do I get back to my bookshelf quickly?</strong><br>
                    <br>
                    Hit the home button at the top of your toolbar.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>I’m reading a book and clicked on a hyperlink and now my book is no longer on the screen.</strong><br>
                    <br>
                    Any hyperlinks clicked while reading your book will open in a different tab of your browser. To navigate back to your book, close the tab that the hyperlink opened in.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>I clicked on the table of contents but now I can only see half of my book on the screen. What should I do?</strong><br>
                    <br>
                    In order to collapse the Table on Contents, click the white back arrow at the top of the Table of Contents pane once you navigate to the section you would like to read.
                </p>
                <br>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>The text on my screen is too small or too large, how can I make adjustments?</strong><br>
                    <br>
                    Zoom Control Instructions for Google Chrome (two options):<br />
                    To adjust the size of everything on the webpages you visit, including text, images, and videos:

                </p>
                <br>

                <!--
<p class="extract"><strong>When I open my book I choose not to go to my last bookmarked page and now I can not remember what chapter I was on, can I easily navigate back there?</strong></p>
<p class="extract"><strong>** we will have to ask Aptara what the best answer for this one is, what do they suggest?</strong></p>
-->
                  <p class="hanging">
                    1.&nbsp;&nbsp;&nbsp;&nbsp;Click the Chrome menu
                <img width="30px" height="30px" alt="" src="assets/images/help/menu.jpg">
                    on the browser toolbar
                </p>
                <p class="hanging">2.&nbsp;&nbsp;&nbsp;&nbsp;Select <strong>Settings</strong>.</p>
                <p class="hanging">3.&nbsp;&nbsp;&nbsp;&nbsp;Click <strong>Show advanced settings</strong>.</p>
                <p class="hanging">4.&nbsp;&nbsp;&nbsp;&nbsp;In the “Web Content” section, use the "Page zoom" drop-down menu to adjust the zoom.</p>
                <br>
                <p class="extract"><strong>To adjust the zoom on your current page: </strong></p>
                <p class="extract">
                    Use the zoom options in the Chrome menu to make everything on a webpage larger or smaller.<br>
                    <br>
                </p>

                <p class="hanging">
                    1.&nbsp;&nbsp;&nbsp;&nbsp;Click the Chrome menu
                <img width="30px" height="30px" alt="" src="assets/images/help/menu.jpg">
                    on the browser toolbar.
                </p>
                <p class="hanging">2.&nbsp;&nbsp;&nbsp;&nbsp;Find the “Zoom” section in the menu and choose one of the following options:</p>
                <p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Click
                <img width="30px" height="30px" alt="" src="assets/images/help/p.jpg">
                    to make everything on the page larger. You can also use the keyboard shortcuts <strong>Ctrl</strong> and <strong>+</strong> (Windows, Linux, and Chrome OS) and
                <img width="30px" height="30px" alt="" src="assets/images/help/q.jpg">
                    and <strong>+</strong> (Mac).
                </p>
                <p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Click
                <img width="30px" height="30px" alt="" src="assets/images/help/minus.jpg">
                    to make everything smaller. You can also use the keyboard shortcuts <strong>Ctrl</strong> and - (Windows, Linux, and Chrome OS) and
                <img width="30px" height="30px" alt="" src="assets/images/help/q.jpg">
                    and - (Mac).
                </p>
                <p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;To go into full-screen mode, click 
                <img width="30px" height="30px" alt="" src="assets/images/help/minus.jpg">
                    You can also use the keyboard shortcuts <strong>F11</strong> (Windows and Linux) and
                <img width="30px" height="30px" alt="" src="assets/images/help/fullscreen.png">
                    Shift-F (Mac). If you’re using Chrome OS, you can also press 
                    <img width="30px" height="30px" alt="" src="assets/images/help/OS.png">
                    at the top of your keyboard.
                </p>
                <%--<p class="hanging1">
                    <span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;To go into full-screen mode, click
                <img width="30px" height="30px" alt="" src="assets/images/help/1.jpg">. You can also use the keyboard shortcuts <strong>F11</strong> (Windows and Linux) and<img alt="" src="assets/images/help/q.jpg" class="inline"><strong>-Shift-F</strong> (Mac). If you’re using Chrome OS, you can also press
                <img width="50px" height="80px" alt="" src="assets/images/help/2.jpg">
                    at the top of your keyboard.
                </p>--%>

                <hr width="100%" height="1%">

                <p class="hanging">Zoom Control Instructions for Firefox:</p>
                <br>
                

                
                <p class="hanging1"><span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Click the menu button on the right. The customization menu will open and you will see the zoom controls at the top.</p>
                <p class="center">
                    <img alt="" src="assets/images/help/3.jpg">
                </p>
                <p class="hanging"><span class="small">o</span>&nbsp;&nbsp;&nbsp;&nbsp;Use the + button to zoom in, and the - button to zoom out. The number in the middle is the current zoom level - click it to reset the zoom to 100%.</p>

                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>If I don't have internet access, can I still access electronic course materials?</strong><br>
                    <br>
                    An internet connection is only required when downloading the materials for the first time. Each browser has different capabilities; however, if it has already been downloaded, you will be able to accesselectronic materials for at least one course without a connection.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I read electronic course materials on my bookshelf from home?</strong><br>
                    <br>
                    You can read your electronic course materials from home or anywhere else with an internet connection. If it has already been downloaded, you will be able access electronic course materials for at least one course without an internet connection from home.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What internet browsers can I use to access and read electronic course materials?</strong><br>
                    <br>
                    <strong>Recommended:</strong><br />
                    o	Windows: Internet Explorer 9, Google Chrome 26.0+, Mozilla Firefox 20.0+<br />
                    o	Mac: Safari 5.1+<br />
                    <strong>Not recommended:</strong><br />
                    •	Windows: Internet Explorer V9 and below<br />
                    •	Mac: Safari 5.1 and below
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I access and read electronic course materials at home and pick up where I left off in class?</strong><br>
                    <br>
                    Yes and your notes, bookmarks, highlights, and favorites are available as well. When you login and open your course materials from home, you will be asked if you would like to return to your last page.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>How do I bookmark a page so I can follow along in class?</strong><br>
                    <br>
                    Use the "Favorites" button in the top navigation bar to bookmark a specific page and easily access it at a later date. If you are unsure which icon represents "Favorites," click the "Question Mark" for an icon key.
                </p>
                <br>
                 <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I cut and paste from the text to take notes and send them to my personal email?</strong><br>
                    <br>
                    Yes, you can copy and paste text into any email of your choosing.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What do I get to take home after the class?</strong><br>
                    <br>
                    You will have unlimited, continuous access to the current version of the electronic course materials for three years. Management Concepts-provided laptops are for classroom use only and must be returned at the end of class.
                </p>
                <br>
                 <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I access and read electronic course materials on my Kindle?</strong><br>
                    <br>
                    Electronic course materials are not supported on Kindle devices at this time. 
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I access and read electronic course materials on my Nook?</strong><br>
                    <br>
                    Electronic course materials are not supported on Nook devices at this time.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>How do I register for a course?</strong><br>
                    <br>
                    The registration process for a class using electronic course materials is the same as any other Management Concepts class – either online at <a href="http://www.ManagementConcepts.com" target="_blank">http://www.ManagementConcepts.com</a> on the selected course page or via phone at <strong>888.545.8571</strong>. For a complete list of courses, please visit <a href="http://www.ManagementConcepts.com/ElectronicCourseMaterials" target="_blank">http://www.ManagementConcepts.com/ElectronicCourseMaterials</a>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>How much does it cost to register for a class using electronic course materials?</strong><br>
                    <br>
                    There is no difference in the price of a class that uses electronic course materials.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Is technical assistance available throughout for the class?</strong><br>
                    <br>
                    Yes. If you need help getting into the portal, or resolving other technical issues, contact our technical support team, toll-free, at <strong>844.876.7476</strong> or send an email to <a href="mailto:studentsupport@managementconcepts.com" target="_blank">studentsupport@managementconcepts.com</a>
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>What if I forget my password?</strong><br>
                    <br>
                    If you forgot your password, go to the login screen at <a href="http://coursematerials.managementconcepts.com" target="_blank">http://coursematerials.managementconcepts.com</a> and click “Forgot Password”. Enter your email address and a new password will be sent to you.
                </p>
                <br>
                <hr width="100%" height="1%">
                <p class="extract">
                    <strong>Can I print the course content?</strong><br>
                    <br>
                    Yes, electronic course materials can be printed but not until the class has been completed.
                </p>
                <br>

            </div>
        </div>

        <div class="zoomCovePageDialog" id="useMeDialog" style="margin-top: -220px;margin-left:180px;display: none;">
            <div class="zoom_title">
                <h5 id="titleOfVideoDialog">How to</h5>
                <div id="btnUseMeDialogClose" class="zoom_close">
                    x
			
                    <!--<img src="assets/images/helpPopupCloseBtn.png"> -->
                </div>
            </div>
            <div class="inner_space" style="height: 300px;">
                <video controls="" src="http://coursematerials.managementconcepts.com:8088/video/eBooksVid_160204.mp4" id="popupVideoPlayer"></video>
            </div>
        </div>
    </form>

</body>
</html>
