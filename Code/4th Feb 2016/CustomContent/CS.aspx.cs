﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    public static string GetCurrentTime()
    {
        return "Hello " + DateTime.Now.ToString();
    }

    [System.Web.Services.WebMethod]
    public static string Insertdata(string PageName, string HighlightData, string userid, string classid)
    {
        //return "Hello " + PageName + HighlightData+ DateTime.Now.ToString();
        SqlCommand comm;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
  
        //string si = HighlightData.Replace(@"highlighted", @"\""highlighted\");
        
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "UserAction_Insert";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@DeviceId", 0);
            comm.Parameters.AddWithValue("@ClassId", Convert.ToString(classid));
            comm.Parameters.AddWithValue("@ContentId", 0);
            comm.Parameters.AddWithValue("@PageNo", 0);
            comm.Parameters.AddWithValue("@TotalPage", 0);
            comm.Parameters.AddWithValue("@StartDate", DateTime.Now);
            comm.Parameters.AddWithValue("@EndDate", DateTime.Now);
            comm.Parameters.AddWithValue("@SynchId", 0);
            comm.Parameters.AddWithValue("@IsActive", 1);
            comm.Parameters.AddWithValue("@PageName", PageName);
            comm.Parameters.AddWithValue("@HighlightData",  HighlightData.Replace("\\\"","#")  );
            try
            {
                comm.ExecuteNonQuery();
                return "Record Saved";
            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }
        

    }

    [System.Web.Services.WebMethod]
    public static string GetAnnotation(string id)
    {//return "Hello ajay " +DateTime.Now.ToString();

        SqlDataAdapter da = new SqlDataAdapter();
        String daresult = null;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("GetAnotation", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id", id);

                using (da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();


                    da.Fill(dt);
                    ds.Tables.Add(dt);
                    if (dt.Rows.Count > 0)
                        daresult = dt.Rows[0]["comment"].ToString();
                    //daresult =  dt.Rows[0][0].ToString() ;

                    return daresult;
                }
            }
        }

    }

    [System.Web.Services.WebMethod]
    public static string InsertdataAnnotation(string PageName, string AnnotationData, string Parentclass, string Parentid, string Replacedata, string comment, string AnnotationID, string userid, string classid)
    {
       //return "Hello " + PageName + AnnotationData + DateTime.Now.ToString();
        SqlCommand comm;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;

        //string si = HighlightData.Replace(@"highlighted", @"\""highlighted\");

        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "UserAction_InsertAnnotationData";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@DeviceId", 0);
            comm.Parameters.AddWithValue("@ClassId", Convert.ToString(classid));
            comm.Parameters.AddWithValue("@ContentId", 0);
            comm.Parameters.AddWithValue("@PageNo", 0);
            comm.Parameters.AddWithValue("@TotalPage", 0);
            comm.Parameters.AddWithValue("@StartDate", DateTime.Now);
            comm.Parameters.AddWithValue("@EndDate", DateTime.Now);
            comm.Parameters.AddWithValue("@SynchId", 0);
            comm.Parameters.AddWithValue("@IsActive", 1);
            comm.Parameters.AddWithValue("@PageName", PageName);
            comm.Parameters.AddWithValue("@AnnotationData", AnnotationData);
            comm.Parameters.AddWithValue("@Parentclass", Parentclass);
            comm.Parameters.AddWithValue("@Parentid", Parentid);
            comm.Parameters.AddWithValue("@Replacedata", Replacedata);
            comm.Parameters.AddWithValue("@comment", comment);
            comm.Parameters.AddWithValue("@AnnotationID", AnnotationID);
            try
            {
                comm.ExecuteNonQuery();
                return "Record Saved";
            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }


    }
	
	[System.Web.Services.WebMethod]
    public static string UpdateDataAnnotation(string PageName, string AnnotationData, string userid, string classid)
    {
       //return "Hello " + PageName + AnnotationData + DateTime.Now.ToString();
        SqlCommand comm;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;

        //string si = HighlightData.Replace(@"highlighted", @"\""highlighted\");

        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "UserAction_UpdateAnnotationData";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@DeviceId", 0);
            comm.Parameters.AddWithValue("@ClassId", Convert.ToString(classid));
            comm.Parameters.AddWithValue("@ContentId", 0);
            comm.Parameters.AddWithValue("@PageNo", 0);
            comm.Parameters.AddWithValue("@TotalPage", 0);
            comm.Parameters.AddWithValue("@StartDate", DateTime.Now);
            comm.Parameters.AddWithValue("@EndDate", DateTime.Now);
            comm.Parameters.AddWithValue("@SynchId", 0);
            comm.Parameters.AddWithValue("@IsActive", 1);
            comm.Parameters.AddWithValue("@PageName", PageName);
            comm.Parameters.AddWithValue("@AnnotationData", AnnotationData);
            try
            {
                comm.ExecuteNonQuery();
                return "Record Saved";
            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }


    }
	
    [System.Web.Services.WebMethod]
    public static string InsertBooknotes(string userid, string courseid, string juliandate, string BookNotes, string Getpagename)
    {
      
        //return "Hello " + PageName + AnnotationData + DateTime.Now.ToString();
        SqlCommand comm;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;

        //string si = HighlightData.Replace(@"highlighted", @"\""highlighted\");

        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "InsertBookNotes";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@courseid",  Convert.ToString(courseid));
            comm.Parameters.AddWithValue("@juliandate", juliandate);
            comm.Parameters.AddWithValue("@BookMarkContents", BookNotes);
            comm.Parameters.AddWithValue("@NoteContents", string.Empty);
            comm.Parameters.AddWithValue("@Type", '1');
            comm.Parameters.AddWithValue("@CreatedBy", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
            comm.Parameters.AddWithValue("@UpdatedBy", 0);
            comm.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
            comm.Parameters.AddWithValue("@Active", 1);
			comm.Parameters.AddWithValue("@pagename", Getpagename);
    
            try
            {
                
                    comm.ExecuteNonQuery();
                   

                
                return "Record Saved";


            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }

    }
    [System.Web.Services.WebMethod]
    public static string GetBooknotes(string userid, string courseid, string juliandate, string Getpagename)
    {
        //return "Hello ajay " +DateTime.Now.ToString();

        SqlDataAdapter da = new SqlDataAdapter();
        String daresult = null;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("GetBooknotes", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userid", Convert.ToString(userid));
                cmd.Parameters.Add("@courseid", Convert.ToString(courseid));
                cmd.Parameters.Add("@juliandate", juliandate);
				cmd.Parameters.Add("@pagename", Getpagename);              

                using (da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();


                    da.Fill(dt);
                    ds.Tables.Add(dt);
                    if (dt.Rows.Count > 0)
                        daresult =  dt.Rows[0][0].ToString() ;
                    //daresult =  dt.Rows[0][0].ToString() ;

                    return daresult;
                }
            }
        }


    }

    [System.Web.Services.WebMethod]
    public static string InsertGlobalNotes(string userid,string Globalnotes, string Getpagename)
    {

        //return "Hello " + PageName + AnnotationData + DateTime.Now.ToString();
        SqlCommand comm;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;

        //string si = HighlightData.Replace(@"highlighted", @"\""highlighted\");

        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "InsertGlobalNotes";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@courseid", string.Empty);
            comm.Parameters.AddWithValue("@juliandate", string.Empty);
            comm.Parameters.AddWithValue("@BookMarkContents", string.Empty);
            comm.Parameters.AddWithValue("@NoteContents", Globalnotes);
            comm.Parameters.AddWithValue("@Type", '2');
            comm.Parameters.AddWithValue("@CreatedBy", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
            comm.Parameters.AddWithValue("@UpdatedBy", 0);
            comm.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
            comm.Parameters.AddWithValue("@Active", 1);
			comm.Parameters.AddWithValue("@pagename", Getpagename);

            try
            {
                
                    comm.ExecuteNonQuery();


               
                return "Record Saved";


            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }

    }
    [System.Web.Services.WebMethod]
    public static string GetGlobalnotes(string userid)
    {
        //return "Hello ajay " +DateTime.Now.ToString();

        SqlDataAdapter da = new SqlDataAdapter();
        String daresult = null;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("GetGlobalnotes", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userid", Convert.ToString(userid));
             


                using (da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();


                    da.Fill(dt);
                    ds.Tables.Add(dt);
                    if (dt.Rows.Count > 0)
                        daresult = dt.Rows[0][0].ToString();
                    //daresult =  dt.Rows[0][0].ToString() ;

                    return daresult;
                }
            }
        }


    }
    
    [System.Web.Services.WebMethod]
    public static string GetQueryInfo(string PageName,string userid, string classid)
     {
        //return "Hello ajay " +DateTime.Now.ToString();

         SqlDataAdapter da = new SqlDataAdapter();
         String daresult = null;
         string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
         using (SqlConnection con = new SqlConnection(constr))
         {
             con.Open();

             using (SqlCommand cmd = new SqlCommand("UserAction_SelectPage", con))
             {
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.Parameters.Add("@Page", PageName);
                 cmd.Parameters.Add("@userid", Convert.ToString(userid));
                 cmd.Parameters.Add("@classid", Convert.ToString(classid));

                 using (da = new SqlDataAdapter(cmd))
                 {
                     DataTable dt = new DataTable();
                     DataSet ds = new DataSet();


                     da.Fill(dt);
                     ds.Tables.Add(dt);
                     if(dt.Rows.Count >0)
                     daresult = "[" + dt.Rows[0][0].ToString() + "]";
                     //daresult =  dt.Rows[0][0].ToString() ;

                     return daresult;
                 }
             }
         }


    }
    [System.Web.Services.WebMethod]
    public static string GetQueryInfoAnnotation(string PageName, string userid, string classid)
    {
        //return "Hello ajay " +DateTime.Now.ToString();

        SqlDataAdapter da = new SqlDataAdapter();
        String daresult = null;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();

            using (SqlCommand cmd = new SqlCommand("UserAction_SelectPageAnnotation", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Page", PageName);
                cmd.Parameters.Add("@userid",Convert.ToString(userid));
                cmd.Parameters.Add("@classid", Convert.ToString(classid));
                using (da = new SqlDataAdapter(cmd))
                {
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();


                    da.Fill(dt);
                    ds.Tables.Add(dt);
                    if(dt.Rows.Count>0)
                    daresult = dt.Rows[0][0].ToString();
                    //daresult =  dt.Rows[0][0].ToString() ;

                    return daresult;
                }
            }
        }


    }
    public static string DataSetToJSON(DataSet ds)
    {

        Dictionary<string, object> dict = new Dictionary<string, object>();
        foreach (DataTable dt in ds.Tables)
        {
            object[] arr = new object[dt.Rows.Count + 1];

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
               
                arr[i] = dt.Rows[i].ItemArray;
            }

            dict.Add(dt.TableName, arr);
        }

        JavaScriptSerializer json = new JavaScriptSerializer();
        return json.Serialize(dict);
    }
	  [System.Web.Services.WebMethod]
    public static string Bookmark(string userid, string classid, string CurrentPage, string juliandate)
    {
        SqlCommand comm;
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            con.Open();
            comm = new SqlCommand();
            comm.Connection = con;
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.CommandText = "Bookmark";

            comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
            comm.Parameters.AddWithValue("@ClassId", Convert.ToString(classid));
            // comm.Parameters.AddWithValue("@currentPage", Convert.ToString(CurrentPage));
            comm.Parameters.AddWithValue("@PageName", Convert.ToString(CurrentPage));
            comm.Parameters.AddWithValue("@JulienDate", Convert.ToDateTime(juliandate));
            try
            {
                comm.ExecuteNonQuery();
                return "Record Saved";
            }
            catch (Exception ex)
            {
                return "Not Saved";
            }
            finally
            {
                con.Close();
            }
        }
    }
	//[System.Web.Services.WebMethod]
   // public static string getHighlighterData(string Id)
    //{
	//	SqlCommand comm;
	//	string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
    //    string description = String.Empty;      
        
	//	using (SqlConnection con = new SqlConnection(constr))
    //    {
     //       con.Open();
     //       comm = new SqlCommand();
     //       comm.Connection = con;
     //       comm.CommandType = System.Data.CommandType.StoredProcedure;
     //       comm.CommandText = "GetAnnotationComment";

     //       comm.Parameters.AddWithValue("@UserId", Convert.ToString(userid));
    //        comm.Parameters.AddWithValue("@ClassId", Convert.ToString(classid));
    //        comm.Parameters.AddWithValue("@PageName", Convert.ToString(CurrentPage));
    //        comm.Parameters.AddWithValue("@JulienDate", Convert.ToDateTime(juliandate));
	//		cmd.Parameters.AddWithValue("@AnnotationId", Convert.ToInt32(Id));
    //        SqlDataAdapter adp = new SqlDataAdapter(cmd);
	//		DataTable dt = new DataTable();
	//		adp.Fill(dt);
	//		if (dt.Rows.Count > 0)
	//		{
	//			description = dt.Rows[0][0].ToString();
	//		}
	//		return description;
	//		}
   // }
 	
	[System.Web.Services.WebMethod]
    public static void deleteAnnotatedDataComment(string Id, string description)
    {
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand("deleteAnnotatedDataComment", con);
		cmd.CommandType = System.Data.CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@description", description);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        cmd.ExecuteNonQuery();
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }

    }

    [System.Web.Services.WebMethod]
    public static void updateAnnotatedComment(string Id, string description)
    {
        string constr = ConfigurationManager.ConnectionStrings["WebAdmin"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand("updateAnnotatedComment", con);
		cmd.CommandType = System.Data.CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Id", Id);
        cmd.Parameters.AddWithValue("@description", description);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        cmd.ExecuteNonQuery();
        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }

    }
}
